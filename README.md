# PRIVATE: SHARED FOR DEVELPOMENT PURPOSES ONLY WITH UIKB TEAM AT PISTOIA

# gastby-theme-laboperator-docs
A Gatsby.js theme for use in Laboperator static documentation pages.

## Structure

The repository is structured using yarn workspaces. The theme itself is in [gatsby-theme-laboperator-docs](https://github.com/labforward/gatsby-theme-laboperator-docs/tree/develop/gatsby-theme-laboperator-docs), under [site](https://github.com/labforward/gatsby-theme-laboperator-docs/tree/develop/site) is a gastby website using the theme that can be used for testing during development.

## Development

Check out the repository and install dependencies using:
```
$ lerna bootstrap
```

To run the development site:
```
$ cd packages/site
packages/site:$ yarn develop
```

[Some changes](https://www.gatsbyjs.com/docs/reference/gatsby-cli/#clean) require you to clean the local `.cache` and `public` directories for `gatsby` to pick up changes:
```
packages/site:$ yarn clean
```

## Access

Since these packages are privately published using GitHub Packages, we will need to tell npm where to find them.

Please follow these [steps](https://docs.github.com/en/free-pro-team@latest/github/authenticating-to-github/creating-a-personal-access-token) to create a personal access token for GitHub. The access token must have the following scope: `repo`, `write:packages`, `read:packages`, and `delete:packages`.

You can authenticate via any of the following options:

### Option 1 (global authentication)

Run the following command to authenticate with the private registry:

```bash
npm login --registry=https://npm.pkg.github.com --scope=@labforward
```

You'll be asked for your username (GitHub username), password (personal access token), and e-mail (GitHub e-mail).

### Option 2

Create a `.npmrc` file at the root your project with the following contents:

```
@labforward:registry=https://npm.pkg.github.com/
//npm.pkg.github.com/:_authToken=YOUR_PRIVATE_TOKEN_HERE
```

Then replace `YOUR_PRIVATE_TOKEN_HERE` with the token previously generated. This option is useful for CI environments.
#   G a t s b y - T h e m e s  
 
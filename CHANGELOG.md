# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.0.5](https://github.com/labforward/gatsby-theme-laboperator-docs/compare/v0.0.4...v0.0.5) (2021-01-18)

**Note:** Version bump only for package gatsby-theme-laboperator-docs





## [0.0.4](https://github.com/labforward/gatsby-theme-laboperator-docs/compare/v0.0.3...v0.0.4) (2021-01-12)

**Note:** Version bump only for package gatsby-theme-laboperator-docs





## [0.0.3](https://github.com/labforward/gatsby-theme-laboperator-docs/compare/v0.0.2...v0.0.3) (2021-01-12)

**Note:** Version bump only for package gatsby-theme-laboperator-docs





## [0.0.2](https://github.com/labforward/gatsby-theme-laboperator-docs/compare/v0.0.1...v0.0.2) (2021-01-08)

**Note:** Version bump only for package gatsby-theme-laboperator-docs

import { FC } from 'react';
import { Box } from '@material-ui/core';

import logo from '../assets/logo.svg';
import Link from './Link';

// eslint-disable-next-line import/prefer-default-export
export const Logo: FC = () => {
  const config = useConfig();

  return (
    <Box>
      <img src={logo} alt="Laboperator Logo" />
      <Link to="/">{config.title}</Link>
    </Box>
  );
};

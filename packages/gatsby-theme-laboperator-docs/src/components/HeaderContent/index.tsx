import React, { FC } from 'react';
import { get, has } from 'lodash';
import GitHubIcon from '@material-ui/icons/GitHub';
import { graphql, useStaticQuery } from 'gatsby';
import IconButton from '@material-ui/core/IconButton';
import { makeStyles } from '@material-ui/core/styles';

import Link from '../Link';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
}));

const HeaderContent: FC = () => {
  const classes = useStyles();
  const data = useStaticQuery<GatsbyTypes.SiteTitleQuery>(graphql`
    query SiteTitle {
      site {
        siteMetadata {
          title
          githubUrl
        }
      }
    }
  `);

  return (
    <>
      <Link to="/" variant="h6" color="textPrimary" className={classes.title}>
        {get(data, 'site.siteMetadata.title', '')}
      </Link>
      {has(data, 'site.siteMetadata.githubUrl') ? (
        <IconButton
          href={get(data, 'site.siteMetadata.githubUrl')}
          target="_blank"
          color="inherit"
        >
          <GitHubIcon />
        </IconButton>
      ) : null}
    </>
  );
};

export default HeaderContent;

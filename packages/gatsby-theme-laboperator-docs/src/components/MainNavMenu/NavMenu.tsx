import React, { FC } from 'react';
import List from '@material-ui/core/List';

import { MenuItem as MenuItemInt } from '../../hooks/useMenus';
import MenuItem from './MenuItem';

interface NavMenuProps {
  items: MenuItemInt[];
  location: typeof window.location;
  depth?: number;
}

const NavMenu: FC<NavMenuProps> = ({ items, location, depth = 0 }) => (
  <List disablePadding={depth > 0} dense>
    {items.map((item) => (
      <MenuItem key={item.path} {...item} location={location} />
    ))}
  </List>
);

export default NavMenu;

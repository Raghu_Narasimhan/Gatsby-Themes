import React, { FC } from 'react';

import useMenus from '../../hooks/useMenus';
import NavMenu from './NavMenu';
import { PageProps } from 'gatsby';

interface MainNavMenuProps {
  location: typeof window.location;
}

const MainNavMenu: FC<MainNavMenuProps> = ({ location }) => {
  const menu = useMenus();

  return <NavMenu items={menu} location={location} />;
};

export default MainNavMenu;

import React, { FC, useState } from 'react';
import Collapse from '@material-ui/core/Collapse';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import IconButton from '@material-ui/core/IconButton';
import ListItem from '@material-ui/core/ListItem';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import ListSubheader from '@material-ui/core/ListSubheader';
import { makeStyles } from '@material-ui/core/styles';

import { MenuItem as MenuItemInt } from '../../hooks/useMenus';
import Link from '../GatsbyLink';
import NavMenu from './NavMenu';

interface MenuItemProps extends MenuItemInt {
  location: typeof window.location;
}

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
  },
  nested: {
    paddingLeft: (depth: number): number => theme.spacing((depth + 1) * 2),
  },
}));

const MenuItem: FC<MenuItemProps> = ({
  path,
  title,
  subheader,
  location,
  items,
  folders,
}) => {
  const depth = folders.length;
  const classes = useStyles(depth);
  const [open, setOpen] = useState(location.pathname.includes(path));

  return (
    <React.Fragment>
      {subheader ? (
        <ListSubheader className={classes.nested} >{subheader}</ListSubheader>
      ) : null}
      <ListItem
        button
        component={Link}
        to={path}
        className={classes.nested}
        selected={path === location.pathname.replace(/\/?$/, '/')}
      >
        <ListItemText primary={title} />
        {items ? (
          <ListItemSecondaryAction>
            <IconButton
              onClick={(): void => setOpen(!open)}
              edge="end"
              aria-label="toggle"
            >
              {open ? <ExpandLess /> : <ExpandMore />}
            </IconButton>
          </ListItemSecondaryAction>
        ) : null}
      </ListItem>
      {items ? (
        <Collapse in={open} timeout="auto" unmountOnExit>
          <NavMenu items={items} location={location} depth={depth} />
        </Collapse>
      ) : null}
    </React.Fragment>
  );
};

export default MenuItem;

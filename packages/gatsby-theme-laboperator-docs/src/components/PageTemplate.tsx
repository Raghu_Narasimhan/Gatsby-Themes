import React, { FC } from 'react';
import { Box } from '@material-ui/core';
import { getInsetContainer, getInsetSidebar } from '@mui-treasury/layout';
import { graphql } from 'gatsby';
import { makeStyles } from '@material-ui/core/styles';
import { MDXRenderer } from 'gatsby-plugin-mdx';
import { PageProps } from 'gatsby';
import styled from 'styled-components';
import { last } from 'lodash';

import Layout from './Layout';
import Sidebar from './Sidebar';

const InsetContainer = getInsetContainer(styled);
const InsetSidebar = getInsetSidebar(styled);

const useStyles = makeStyles(() => ({
  sidebar: {
    backgroundColor: '#fff',
  },
}));

const PageTemplate: FC<
  PageProps<GatsbyTypes.PageTemplateQuery, GatsbyTypes.SitePageContext>
> = (props) => {
  const {
    data: { mdx },
  } = props;
  const styles = useStyles();

  const embeddedImagesByKey = (mdx?.frontmatter?.embeddedImages || []).reduce(
    (images: any, image, index) => {
      const name = last(image!.childImageSharp!.fluid!.src.split('/')) as string;

      images[name] = images[name] || {
        ...image!.childImageSharp!,
      };

      return images;
    },
    {}
  );

  return (
    <Layout pageContext={props.pageContext} location={props.location}>
      <InsetContainer
        maxWidth="lg"
        rightSidebar={
          <InsetSidebar
            sidebarId="secondarySidebar"
            classes={{ paper: styles.sidebar }}
          >
            <Sidebar toc={mdx!.tableOfContents!} />
          </InsetSidebar>
        }
      >
        <Box m={4}>
          {mdx && mdx.body ? (
            <MDXRenderer
              embeddedImages={embeddedImagesByKey}
            >
              {mdx.body}
            </MDXRenderer>
          ) : null}
        </Box>
      </InsetContainer>
    </Layout>
  );
};

export const query = graphql`
  query PageTemplate($slug: String!) {
    mdx(fields: { slug: { eq: $slug } }) {
      frontmatter {
        title
        embeddedImages {
          childImageSharp {
            fluid(quality: 90) {
              ...GatsbyImageSharpFluid
            }
          }
        }
      }
      body
      tableOfContents
      timeToRead
      fields {
        slug
      }
    }
  }
`;

export default PageTemplate;

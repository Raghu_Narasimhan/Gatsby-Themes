import React, { FC, memo } from 'react';
import Box from '@material-ui/core/Box';
import Checkbox from '@material-ui/core/Checkbox';
import Divider from '@material-ui/core/Divider';
import { makeStyles } from '@material-ui/core/styles';
import { MDXProvider } from '@mdx-js/react';
import Grid from '@material-ui/core/Grid';
import MuiTable from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';
import Img from 'gatsby-image';
import Link from '@material-ui/core/Link';

const useStyles = makeStyles((_theme) => ({
  heading: {
    '& a': {
      float: 'left',
      paddingRight: 4,
      marginLeft: -20,
    },
    '& svg': {
      visibility: 'hidden',
    },
    '&:hover a, &:hover svg': {
      visibility: 'visible',
    },
  },
}));

// import CodeBlock from './CodeBlock';

const components = {
  Img,
  Box,
  Grid,
  p: (() => {
    const Paragraph = (props) => <Typography {...props} paragraph />;

    return memo(Paragraph);
  })(),
  a: Link,
  h1: (() => {
    const H1 = (props) => {
      const classes = useStyles();

      return (
        <Typography
          {...props}
          component="h1"
          variant="h1"
          className={classes.heading}
          paragraph
        />
      );
    };

    return memo(H1);
  })(),
  h2: (() => {
    const H2 = (props) => {
      const classes = useStyles();

      return (
        <Typography
          {...props}
          component="h2"
          variant="h2"
          className={classes.heading}
          paragraph
        />
      );
    };

    return memo(H2);
  })(),
  h3: (() => {
    const H3 = (props) => {
      const classes = useStyles();

      return (
        <Typography
          {...props}
          component="h3"
          variant="h3"
          className={classes.heading}
          paragraph
        />
      );
    };

    return memo(H3);
  })(),
  h4: (() => {
    const H4 = (props) => {
      const classes = useStyles();

      return (
        <Typography
          {...props}
          component="h4"
          variant="h4"
          className={classes.heading}
          paragraph
        />
      );
    };

    return memo(H4);
  })(),
  h5: (() => {
    const H5 = (props) => {
      const classes = useStyles();

      return (
        <Typography
          {...props}
          component="h5"
          variant="h5"
          className={classes.heading}
          paragraph
        />
      );
    };

    return memo(H5);
  })(),
  h6: (() => {
    const H6 = (props) => {
      const classes = useStyles();

      return (
        <Typography
          {...props}
          component="h6"
          variant="h6"
          className={classes.heading}
          paragraph
        />
      );
    };

    return memo(H6);
  })(),
  blockquote: (() => {
    const Blockquote = (props) => (
      <Box
        style={{
          borderLeft: '4px solid lightgrey',
          padding: 8,
          borderRadius: '4px 0 0 4px',
        }}
        mb={2}
        {...props}
      />
    );

    return memo(Blockquote);
  })(),
  ul: (() => {
    const Ul = (props) => <Typography {...props} component="ul" paragraph />;

    return memo(Ul);
  })(),
  ol: (() => {
    const Ol = (props) => <Typography {...props} component="ol" paragraph />;

    return memo(Ol);
  })(),
  li: (() => {
    const Li = (props) => <Typography {...props} component="li" />;

    return memo(Li);
  })(),
  table: (() => {
    const Table = (props) => <MuiTable {...props} />;

    return memo(Table);
  })(),
  tr: (() => {
    const Tr = (props) => <TableRow {...props} />;

    return memo(Tr);
  })(),
  td: (() => {
    const Td = ({ align, ...props }) => (
      <TableCell align={align || undefined} {...props} />
    );

    return memo(Td);
  })(),
  tbody: (() => {
    const TBody = (props) => <TableBody {...props} />;

    return memo(TBody);
  })(),
  th: (() => {
    const Th = ({ align, ...props }) => (
      <TableCell align={align || undefined} {...props} />
    );

    return memo(Th);
  })(),
  thead: (() => {
    const THead = (props) => <TableHead {...props} />;

    return memo(THead);
  })(),
  hr: Divider,
  input: (() => {
    const Input = (props) => {
      const { type } = props;

      if (type === 'checkbox') {
        return <Checkbox {...props} disabled={false} readOnly={true} />;
      }

      return <input {...props} />;
    };

    return memo(Input);
  })(),
  wrapper: (() => {
    const Wrapper = (props) => <div {...props} className="markdown-body" />;

    return memo(Wrapper);
  })(),
};

const CustomMDXProvider: FC = ({ children }) => (
  <MDXProvider components={components}>{children}</MDXProvider>
);

export default CustomMDXProvider;

import React, { FC, ReactNode } from 'react';
import { AppBar, Toolbar } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { omit } from 'lodash';
import {
  Root,
  getCollapseBtn,
  getContent,
  getDrawerSidebar,
  getHeader,
  getInsetContainer,
  getInsetFooter,
  getInsetSidebar,
  getSidebarContent,
  getSidebarTrigger,
} from '@mui-treasury/layout';
import styled from 'styled-components';

import layoutScheme from '../layoutScheme';
import FooterContent from './FooterContent';
import HeaderContent from './HeaderContent';
import MainNavMenu from './MainNavMenu';
import Wrapper from './Wrapper';

const Header = getHeader(styled);
const DrawerSidebar = getDrawerSidebar(styled);
const SidebarTrigger = getSidebarTrigger(styled);
const SidebarContent = getSidebarContent(styled);
const CollapseBtn = getCollapseBtn(styled);
const Content = getContent(styled);
const InsetContainer = getInsetContainer(styled);
const InsetSidebar = getInsetSidebar(styled);
const InsetFooter = getInsetFooter(styled);

const useStyles = makeStyles(() => ({
  header: {
    backgroundColor: '#fff',
  },
}));

interface LayoutProps {
  children: ReactNode;
  location: typeof window.location;
  pageContext: GatsbyTypes.SitePageContext;
}

const App: FC<LayoutProps> = (props) => {
  const { children, location, pageContext } = props;
  const styles = useStyles();

  return (
    <Wrapper pageContext={pageContext}>
      <Root scheme={layoutScheme} themeProviderOmitted>
        {() => (
          <React.Fragment>
            <Header className={styles.header}>
              <AppBar>
                <Toolbar>
                  <SidebarTrigger sidebarId="primarySidebar" />
                  <HeaderContent />
                </Toolbar>
              </AppBar>
            </Header>
            <DrawerSidebar sidebarId="primarySidebar">
              <SidebarContent>
                <MainNavMenu location={location} />
              </SidebarContent>
              <CollapseBtn />
            </DrawerSidebar>
            <Content>
              {children}
            </Content>
            <InsetFooter ContainerProps={{ maxWidth: 'lg' }}>
              <FooterContent />
            </InsetFooter>
          </React.Fragment>
        )}
      </Root>
    </Wrapper>
  );
};

export default App;

import React, { FC } from 'react';
import { CssBaseline, ThemeProvider } from '@material-ui/core';
import { get, has } from 'lodash';
import { graphql, useStaticQuery } from 'gatsby';
import { Helmet } from 'react-helmet';

import theme from '../theme';
import CustomMDXProvider from './CustomMDXProvider';

function getTitle(
  data: GatsbyTypes.SiteMetaDataQuery,
  pageContext: GatsbyTypes.SitePageContext
): string {
  let title = get(data, 'site.siteMetadata.title', '');

  if (has(pageContext, 'frontmatter.title')) {
    title = `${get(pageContext, 'frontmatter.title', '')} - ${title}`;
  }

  return title;
}

interface WrapperProps {
  pageContext: GatsbyTypes.SitePageContext;
}

// The doc prop contains some metadata about the page being rendered that you can use.
const Wrapper: FC<WrapperProps> = ({ children, pageContext }) => {
  const data = useStaticQuery<GatsbyTypes.SiteMetaDataQuery>(graphql`
    query SiteMetaData {
      site {
        siteMetadata {
          title
          robots
          description
        }
      }
    }
  `);

  return (
    <>
      <Helmet>
        <meta charSet="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>{getTitle(data, pageContext)}</title>
        <meta
          name="description"
          content={get(data, 'site.siteMetadata.description', '')}
        />
        {!get(data, 'site.siteMetadata.robots', false) ? (
          <meta name="robots" content="noindex, nofollow" />
        ) : null}
      </Helmet>
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <CustomMDXProvider>{children}</CustomMDXProvider>
      </ThemeProvider>
    </>
  );
};

export default Wrapper;

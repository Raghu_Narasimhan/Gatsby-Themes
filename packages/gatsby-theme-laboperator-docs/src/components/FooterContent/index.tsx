import React, { FC } from 'react';
import Box from '@material-ui/core/Box';
import FacebookIcon from '@material-ui/icons/Facebook';
import IconButton from '@material-ui/core/IconButton';
import LinkedInIcon from '@material-ui/icons/LinkedIn';
import TwitterIcon from '@material-ui/icons/Twitter';
import Typography from '@material-ui/core/Typography';

import Link from '../Link';

const Footer: FC = () => (
  <>
    <Box textAlign="center" m={2} mt={4}>
      <IconButton href="https://twitter.com/Laboperator_/">
        <TwitterIcon />
      </IconButton>
      <IconButton href="https://www.facebook.com/labforward/">
        <FacebookIcon />
      </IconButton>
      <IconButton href="https://www.linkedin.com/company/laboperator">
        <LinkedInIcon />
      </IconButton>
    </Box>
    <Box textAlign="center" m={2}>
      <Typography variant="body2" color="textSecondary">
        Part of&nbsp;
        <Link href="https://www.laboperator.com/" target="_blank">
          Laboperator
        </Link>
        &nbsp;made by&nbsp;
        <Link href="https://www.labforward.io/" target="_blank">
          Labforward
        </Link>
        .
      </Typography>
    </Box>
    <Box textAlign="center" m={2}>
      <Typography variant="body2" color="textSecondary">
        2020 Labforward GmbH ©
      </Typography>
    </Box>
  </>
);

export default Footer;

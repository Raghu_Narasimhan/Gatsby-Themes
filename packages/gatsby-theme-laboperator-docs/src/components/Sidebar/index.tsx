import React, { FC } from 'react';
import Box from '@material-ui/core/Box';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';

import Link from '../Link';

const useStyles = makeStyles((theme) => ({
  sidebar: {
    maxHeight: 'calc(100vh - 150px)',
    overflow: 'scroll',
  },
  list: {
    width: '100%',
    maxWidth: 360,
    padding: 0,
    listStyle: 'none',
  },
  link: {
    '&:hover': {
      textDecoration: 'none',
    },
  },
  inset: {
    paddingLeft: theme.spacing(2),
  },
}));

interface TocItem {
  items?: TocItem[];
  title: string;
  url: string;
}

interface Toc {
  items?: TocItem[];
}

interface SidebarInterface {
  toc: Toc;
}

const Sidebar: FC<SidebarInterface> = ({ toc }) => {
  const classes = useStyles();

  return (
    <Box ml={6} mt={4} className={classes.sidebar}>
      {toc.items !== undefined && toc.items[0].items ? (
        <>
          <Typography variant="overline" color="textSecondary">
            Content
          </Typography>
          <Typography component="div" color="textSecondary">
            {toc.items[0].items.map((h2) => (
              <ul className={classes.list} key={h2.url}>
                <li>
                  <Link href={h2.url} color="inherit" className={classes.link}>
                    {h2.title}
                  </Link>
                </li>
                {h2.items
                  ? h2.items.map((h3) => (
                      <li key={h3.url} className={classes.inset}>
                        <Link
                          href={h3.url}
                          color="inherit"
                          className={classes.link}
                        >
                          {h3.title}
                        </Link>
                      </li>
                    ))
                  : null}
              </ul>
            ))}
          </Typography>
        </>
      ) : null}
    </Box>
  );
};

export default Sidebar;

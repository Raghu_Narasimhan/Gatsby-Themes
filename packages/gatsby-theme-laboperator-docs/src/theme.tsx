import chroma from 'chroma-js';
import { common, grey } from '@material-ui/core/colors';
import { createMuiTheme, responsiveFontSizes } from '@material-ui/core';

const primaryMain = '#ffad00';
const primaryLight = chroma(primaryMain).brighten(1).css();
const primaryDark = chroma(primaryMain).darken(1).css();

const secondaryMain = '#002b56';
const secondaryLight = chroma(secondaryMain).brighten(1).css();
const secondaryDark = chroma(secondaryMain).darken(1).css();

const greyLight = grey[100];
const error = '#a63d40';
const warning = '#fbd237';
const info = '#1446a0';
const success = '#78ba21';

// A custom theme for this app
const theme = responsiveFontSizes(
  createMuiTheme({
    palette: {
      primary: {
        light: primaryLight,
        main: primaryMain,
        dark: primaryDark,
      },
      secondary: {
        light: secondaryLight,
        main: secondaryMain,
        dark: secondaryDark,
      },
      error: {
        main: error,
      },
      warning: {
        main: warning,
      },
      info: {
        main: info,
      },
      success: {
        main: success,
      },
    },
    overrides: {
      MuiCssBaseline: {
        '@global': {
          body: {
            backgroundColor: '#fff',
          },
          html: {
            scrollBehavior: 'smooth',
            WebkitFontSmoothing: 'auto',
          },
        },
      },
      MuiAppBar: {
        root: {
          '& .MuiChip-colorPrimary': {
            color: secondaryMain,
          },
          '& .MuiChip-deleteIcon': {
            color: chroma(secondaryMain).alpha(0.5).hex(),
            '&:hover': {
              color: secondaryMain,
            },
          },
        },
      },
      MuiAvatar: {
        colorDefault: {
          backgroundColor: secondaryLight,
        },
      },
      MuiButton: {
        text: {
          color: secondaryMain,
          '&:hover': {
            backgroundColor: chroma(secondaryMain).alpha(0.08).hex(),
          },
        },
        textPrimary: {
          color: secondaryMain,
          '&:hover': {
            backgroundColor: chroma(secondaryMain).alpha(0.08).hex(),
          },
        },
        root: {
          '&:hover': {
            backgroundColor: chroma(secondaryMain).alpha(0.08).hex(),
          },
          /* Bootstrap currently adds a reduced opacity to buttons on top of the alpha channel value that MUI applies to the disabled state. Once Bootstrap is removed, this rule should be safe to delete.*/
          '&:disabled': {
            opacity: 1,
          },
          '&&:visited': {
            color: secondaryMain,
          },
        },
      },
      MuiDrawer: {
        paperAnchorDockedLeft: {
          backgroundColor: `${greyLight} !important`,
        },
      },
      MuiFormLabel: {
        root: {
          '&$focused': {
            color: secondaryLight,
          },
        },
      },
      MuiFab: {
        label: {
          color: secondaryMain,
        },
      },
      MuiLink: {
        root: {
          color: secondaryMain,
        }
      },
      MuiSvgIcon: {
        root: {
          color: secondaryMain,
          '.MuiAlert-icon &, [class^=SnackbarItem-root] &, .MuiCheckbox-colorPrimary &, .Mui-disabled &': {
            color: 'inherit',
          },
          '.MuiAppBar-root .Mui-disabled &': {
            color: chroma(secondaryMain).alpha(0.5).hex(),
          },
        },
      },
      MuiTab: {
        root: {
          backgroundColor: common.white,
        },
      },
      MuiTabs: {
        indicator: {
          backgroundColor: primaryMain,
        },
      },
      MuiTooltip: {
        tooltip: {
          backgroundColor: grey[600],
        },
      },
      MuiTypography: {
        h1: {
          color: secondaryMain,
        },
        h2: {
          color: secondaryMain,
        },
        h3: {
          color: secondaryMain,
        },
        h4: {
          color: secondaryMain,
        },
        h5: {
          color: secondaryMain,
        },
        h6: {
          color: secondaryMain,
        },
      },
    },
  })
);

export default theme;

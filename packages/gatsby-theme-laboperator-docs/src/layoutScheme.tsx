import Layout from '@mui-treasury/layout';

// eslint-disable-next-line new-cap
const scheme = Layout();

scheme.configureHeader((builder) => {
  builder
    .create('appHeader')
    .registerConfig('xs', {
      position: 'sticky',
      initialHeight: 56,
    })
    .registerConfig('md', {
      position: 'sticky',
      initialHeight: 64,
      clipped: { primarySidebar: true },
    });
});

scheme.configureEdgeSidebar((builder) => {
  builder
    .create('primarySidebar', { anchor: 'left' })
    .registerTemporaryConfig('xs', {
      width: 256,
    })
    .registerPersistentConfig('md', {
      width: 200,
      collapsible: true,
      persistentBehavior: {
        appHeader: 'fit',
        content: 'fit',
        footer: 'fit',
      },
    })
    .registerPermanentConfig('lg', {
      width: 256,
      collapsible: false,
    });
});

scheme.configureInsetSidebar((builder) => {
  builder
    .create('secondarySidebar', { anchor: 'right' })
    .registerStickyConfig('md', {
      width: 256,
      top: 105,
    });
});

export default scheme;

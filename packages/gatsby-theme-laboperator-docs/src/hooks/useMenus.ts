import { compact, sortBy } from 'lodash';
import { graphql, useStaticQuery } from 'gatsby';

interface Node {
  fields: {
    slug: string;
    sourceInstanceName: string;
  };
  frontmatter: {
    title: string;
    order: number;
    subheader: string;
  };
  slug: string;
}

interface EnhancedNode extends Node {
  folders: string[];
  parent: string;
  splitPath: string[];
  title: string;
  subheader: string;
  path: string;
}

export interface MenuItem extends EnhancedNode {
  items?: MenuItem[];
}

function addPage(
  node: EnhancedNode,
  folders: string[],
  menu: MenuItem[] = []
): MenuItem[] {
  const nextMenu = [...menu];

  // No folders to look for, just add the page
  if (folders.length === 0) {
    nextMenu.push(node);
  } else {
    const folder = folders[0];
    const regex = RegExp(`${folder}/$`);
    const i = nextMenu.findIndex(({ fields: { slug } }) => regex.test(slug));

    if (i !== -1) {
      // index page exists, we add the page to it's items
      nextMenu[i] = {
        ...nextMenu[i],
        items: addPage(node, folders.slice(1), nextMenu[i].items),
      };
    }
  }

  return nextMenu;
}

function sortItems(menu: MenuItem[]): MenuItem[] {
  return sortBy(
    menu.map((item) => {
      const sortedItem = { ...item };

      if (sortedItem.items) {
        sortedItem.items = sortItems(sortedItem.items);
      }

      return sortedItem;
    }),
    'frontmatter.order'
  );
}

function buildMenu(nodes: Node[]): MenuItem[] {
  let menu: MenuItem[] = [];

  const enhancedNodes = nodes.map((node) => {
    const splitPath = compact(node.fields.slug.split('/'));
    const folders = splitPath.slice(0, -1);
    const parent = splitPath[splitPath.length - 2];
    const enhancedNode = {
      ...node,
      folders,
      parent,
      path: node.fields.slug,
      splitPath,
      subheader: node.frontmatter?.subheader,
      title: node.frontmatter?.title,
    };

    return enhancedNode;
  });

  sortBy(enhancedNodes, 'splitPath.length').forEach((node) => {
    menu = addPage(node, node.folders, menu);
  });

  return sortItems(menu);
}

const useMenus = (): MenuItem[] => {
  const data = useStaticQuery(graphql`
    {
      allMdx(
        filter: { fields: { sourceInstanceName: { eq: "pages" } } }
      ) {
        nodes {
          fields {
            sourceInstanceName
            slug
          }
          frontmatter {
            title
            subheader
            order
          }
        }
      }
    }
  `);

  return buildMenu(data.allMdx.nodes);
};

export default useMenus;

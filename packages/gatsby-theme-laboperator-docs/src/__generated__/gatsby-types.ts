/* eslint-disable */

declare namespace GatsbyTypes {
type Maybe<T> = T | undefined;
type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
/** All built-in and custom scalars, mapped to their actual values */
type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  Date: string;
  JSON: never;
};











type BooleanQueryOperatorInput = {
  readonly eq: Maybe<Scalars['Boolean']>;
  readonly ne: Maybe<Scalars['Boolean']>;
  readonly in: Maybe<ReadonlyArray<Maybe<Scalars['Boolean']>>>;
  readonly nin: Maybe<ReadonlyArray<Maybe<Scalars['Boolean']>>>;
};


type DateQueryOperatorInput = {
  readonly eq: Maybe<Scalars['Date']>;
  readonly ne: Maybe<Scalars['Date']>;
  readonly gt: Maybe<Scalars['Date']>;
  readonly gte: Maybe<Scalars['Date']>;
  readonly lt: Maybe<Scalars['Date']>;
  readonly lte: Maybe<Scalars['Date']>;
  readonly in: Maybe<ReadonlyArray<Maybe<Scalars['Date']>>>;
  readonly nin: Maybe<ReadonlyArray<Maybe<Scalars['Date']>>>;
};

type Directory = Node & {
  readonly sourceInstanceName: Scalars['String'];
  readonly absolutePath: Scalars['String'];
  readonly relativePath: Scalars['String'];
  readonly extension: Scalars['String'];
  readonly size: Scalars['Int'];
  readonly prettySize: Scalars['String'];
  readonly modifiedTime: Scalars['Date'];
  readonly accessTime: Scalars['Date'];
  readonly changeTime: Scalars['Date'];
  readonly birthTime: Scalars['Date'];
  readonly root: Scalars['String'];
  readonly dir: Scalars['String'];
  readonly base: Scalars['String'];
  readonly ext: Scalars['String'];
  readonly name: Scalars['String'];
  readonly relativeDirectory: Scalars['String'];
  readonly dev: Scalars['Int'];
  readonly mode: Scalars['Int'];
  readonly nlink: Scalars['Int'];
  readonly uid: Scalars['Int'];
  readonly gid: Scalars['Int'];
  readonly rdev: Scalars['Int'];
  readonly ino: Scalars['Float'];
  readonly atimeMs: Scalars['Float'];
  readonly mtimeMs: Scalars['Float'];
  readonly ctimeMs: Scalars['Float'];
  readonly atime: Scalars['Date'];
  readonly mtime: Scalars['Date'];
  readonly ctime: Scalars['Date'];
  /** @deprecated Use `birthTime` instead */
  readonly birthtime: Maybe<Scalars['Date']>;
  /** @deprecated Use `birthTime` instead */
  readonly birthtimeMs: Maybe<Scalars['Float']>;
  readonly blksize: Maybe<Scalars['Int']>;
  readonly blocks: Maybe<Scalars['Int']>;
  readonly id: Scalars['ID'];
  readonly parent: Maybe<Node>;
  readonly children: ReadonlyArray<Node>;
  readonly internal: Internal;
};


type Directory_modifiedTimeArgs = {
  formatString: Maybe<Scalars['String']>;
  fromNow: Maybe<Scalars['Boolean']>;
  difference: Maybe<Scalars['String']>;
  locale: Maybe<Scalars['String']>;
};


type Directory_accessTimeArgs = {
  formatString: Maybe<Scalars['String']>;
  fromNow: Maybe<Scalars['Boolean']>;
  difference: Maybe<Scalars['String']>;
  locale: Maybe<Scalars['String']>;
};


type Directory_changeTimeArgs = {
  formatString: Maybe<Scalars['String']>;
  fromNow: Maybe<Scalars['Boolean']>;
  difference: Maybe<Scalars['String']>;
  locale: Maybe<Scalars['String']>;
};


type Directory_birthTimeArgs = {
  formatString: Maybe<Scalars['String']>;
  fromNow: Maybe<Scalars['Boolean']>;
  difference: Maybe<Scalars['String']>;
  locale: Maybe<Scalars['String']>;
};


type Directory_atimeArgs = {
  formatString: Maybe<Scalars['String']>;
  fromNow: Maybe<Scalars['Boolean']>;
  difference: Maybe<Scalars['String']>;
  locale: Maybe<Scalars['String']>;
};


type Directory_mtimeArgs = {
  formatString: Maybe<Scalars['String']>;
  fromNow: Maybe<Scalars['Boolean']>;
  difference: Maybe<Scalars['String']>;
  locale: Maybe<Scalars['String']>;
};


type Directory_ctimeArgs = {
  formatString: Maybe<Scalars['String']>;
  fromNow: Maybe<Scalars['Boolean']>;
  difference: Maybe<Scalars['String']>;
  locale: Maybe<Scalars['String']>;
};

type DirectoryConnection = {
  readonly totalCount: Scalars['Int'];
  readonly edges: ReadonlyArray<DirectoryEdge>;
  readonly nodes: ReadonlyArray<Directory>;
  readonly pageInfo: PageInfo;
  readonly distinct: ReadonlyArray<Scalars['String']>;
  readonly group: ReadonlyArray<DirectoryGroupConnection>;
};


type DirectoryConnection_distinctArgs = {
  field: DirectoryFieldsEnum;
};


type DirectoryConnection_groupArgs = {
  skip: Maybe<Scalars['Int']>;
  limit: Maybe<Scalars['Int']>;
  field: DirectoryFieldsEnum;
};

type DirectoryEdge = {
  readonly next: Maybe<Directory>;
  readonly node: Directory;
  readonly previous: Maybe<Directory>;
};

enum DirectoryFieldsEnum {
  sourceInstanceName = 'sourceInstanceName',
  absolutePath = 'absolutePath',
  relativePath = 'relativePath',
  extension = 'extension',
  size = 'size',
  prettySize = 'prettySize',
  modifiedTime = 'modifiedTime',
  accessTime = 'accessTime',
  changeTime = 'changeTime',
  birthTime = 'birthTime',
  root = 'root',
  dir = 'dir',
  base = 'base',
  ext = 'ext',
  name = 'name',
  relativeDirectory = 'relativeDirectory',
  dev = 'dev',
  mode = 'mode',
  nlink = 'nlink',
  uid = 'uid',
  gid = 'gid',
  rdev = 'rdev',
  ino = 'ino',
  atimeMs = 'atimeMs',
  mtimeMs = 'mtimeMs',
  ctimeMs = 'ctimeMs',
  atime = 'atime',
  mtime = 'mtime',
  ctime = 'ctime',
  birthtime = 'birthtime',
  birthtimeMs = 'birthtimeMs',
  blksize = 'blksize',
  blocks = 'blocks',
  id = 'id',
  parent___id = 'parent.id',
  parent___parent___id = 'parent.parent.id',
  parent___parent___parent___id = 'parent.parent.parent.id',
  parent___parent___parent___children = 'parent.parent.parent.children',
  parent___parent___children = 'parent.parent.children',
  parent___parent___children___id = 'parent.parent.children.id',
  parent___parent___children___children = 'parent.parent.children.children',
  parent___parent___internal___content = 'parent.parent.internal.content',
  parent___parent___internal___contentDigest = 'parent.parent.internal.contentDigest',
  parent___parent___internal___description = 'parent.parent.internal.description',
  parent___parent___internal___fieldOwners = 'parent.parent.internal.fieldOwners',
  parent___parent___internal___ignoreType = 'parent.parent.internal.ignoreType',
  parent___parent___internal___mediaType = 'parent.parent.internal.mediaType',
  parent___parent___internal___owner = 'parent.parent.internal.owner',
  parent___parent___internal___type = 'parent.parent.internal.type',
  parent___children = 'parent.children',
  parent___children___id = 'parent.children.id',
  parent___children___parent___id = 'parent.children.parent.id',
  parent___children___parent___children = 'parent.children.parent.children',
  parent___children___children = 'parent.children.children',
  parent___children___children___id = 'parent.children.children.id',
  parent___children___children___children = 'parent.children.children.children',
  parent___children___internal___content = 'parent.children.internal.content',
  parent___children___internal___contentDigest = 'parent.children.internal.contentDigest',
  parent___children___internal___description = 'parent.children.internal.description',
  parent___children___internal___fieldOwners = 'parent.children.internal.fieldOwners',
  parent___children___internal___ignoreType = 'parent.children.internal.ignoreType',
  parent___children___internal___mediaType = 'parent.children.internal.mediaType',
  parent___children___internal___owner = 'parent.children.internal.owner',
  parent___children___internal___type = 'parent.children.internal.type',
  parent___internal___content = 'parent.internal.content',
  parent___internal___contentDigest = 'parent.internal.contentDigest',
  parent___internal___description = 'parent.internal.description',
  parent___internal___fieldOwners = 'parent.internal.fieldOwners',
  parent___internal___ignoreType = 'parent.internal.ignoreType',
  parent___internal___mediaType = 'parent.internal.mediaType',
  parent___internal___owner = 'parent.internal.owner',
  parent___internal___type = 'parent.internal.type',
  children = 'children',
  children___id = 'children.id',
  children___parent___id = 'children.parent.id',
  children___parent___parent___id = 'children.parent.parent.id',
  children___parent___parent___children = 'children.parent.parent.children',
  children___parent___children = 'children.parent.children',
  children___parent___children___id = 'children.parent.children.id',
  children___parent___children___children = 'children.parent.children.children',
  children___parent___internal___content = 'children.parent.internal.content',
  children___parent___internal___contentDigest = 'children.parent.internal.contentDigest',
  children___parent___internal___description = 'children.parent.internal.description',
  children___parent___internal___fieldOwners = 'children.parent.internal.fieldOwners',
  children___parent___internal___ignoreType = 'children.parent.internal.ignoreType',
  children___parent___internal___mediaType = 'children.parent.internal.mediaType',
  children___parent___internal___owner = 'children.parent.internal.owner',
  children___parent___internal___type = 'children.parent.internal.type',
  children___children = 'children.children',
  children___children___id = 'children.children.id',
  children___children___parent___id = 'children.children.parent.id',
  children___children___parent___children = 'children.children.parent.children',
  children___children___children = 'children.children.children',
  children___children___children___id = 'children.children.children.id',
  children___children___children___children = 'children.children.children.children',
  children___children___internal___content = 'children.children.internal.content',
  children___children___internal___contentDigest = 'children.children.internal.contentDigest',
  children___children___internal___description = 'children.children.internal.description',
  children___children___internal___fieldOwners = 'children.children.internal.fieldOwners',
  children___children___internal___ignoreType = 'children.children.internal.ignoreType',
  children___children___internal___mediaType = 'children.children.internal.mediaType',
  children___children___internal___owner = 'children.children.internal.owner',
  children___children___internal___type = 'children.children.internal.type',
  children___internal___content = 'children.internal.content',
  children___internal___contentDigest = 'children.internal.contentDigest',
  children___internal___description = 'children.internal.description',
  children___internal___fieldOwners = 'children.internal.fieldOwners',
  children___internal___ignoreType = 'children.internal.ignoreType',
  children___internal___mediaType = 'children.internal.mediaType',
  children___internal___owner = 'children.internal.owner',
  children___internal___type = 'children.internal.type',
  internal___content = 'internal.content',
  internal___contentDigest = 'internal.contentDigest',
  internal___description = 'internal.description',
  internal___fieldOwners = 'internal.fieldOwners',
  internal___ignoreType = 'internal.ignoreType',
  internal___mediaType = 'internal.mediaType',
  internal___owner = 'internal.owner',
  internal___type = 'internal.type'
}

type DirectoryFilterInput = {
  readonly sourceInstanceName: Maybe<StringQueryOperatorInput>;
  readonly absolutePath: Maybe<StringQueryOperatorInput>;
  readonly relativePath: Maybe<StringQueryOperatorInput>;
  readonly extension: Maybe<StringQueryOperatorInput>;
  readonly size: Maybe<IntQueryOperatorInput>;
  readonly prettySize: Maybe<StringQueryOperatorInput>;
  readonly modifiedTime: Maybe<DateQueryOperatorInput>;
  readonly accessTime: Maybe<DateQueryOperatorInput>;
  readonly changeTime: Maybe<DateQueryOperatorInput>;
  readonly birthTime: Maybe<DateQueryOperatorInput>;
  readonly root: Maybe<StringQueryOperatorInput>;
  readonly dir: Maybe<StringQueryOperatorInput>;
  readonly base: Maybe<StringQueryOperatorInput>;
  readonly ext: Maybe<StringQueryOperatorInput>;
  readonly name: Maybe<StringQueryOperatorInput>;
  readonly relativeDirectory: Maybe<StringQueryOperatorInput>;
  readonly dev: Maybe<IntQueryOperatorInput>;
  readonly mode: Maybe<IntQueryOperatorInput>;
  readonly nlink: Maybe<IntQueryOperatorInput>;
  readonly uid: Maybe<IntQueryOperatorInput>;
  readonly gid: Maybe<IntQueryOperatorInput>;
  readonly rdev: Maybe<IntQueryOperatorInput>;
  readonly ino: Maybe<FloatQueryOperatorInput>;
  readonly atimeMs: Maybe<FloatQueryOperatorInput>;
  readonly mtimeMs: Maybe<FloatQueryOperatorInput>;
  readonly ctimeMs: Maybe<FloatQueryOperatorInput>;
  readonly atime: Maybe<DateQueryOperatorInput>;
  readonly mtime: Maybe<DateQueryOperatorInput>;
  readonly ctime: Maybe<DateQueryOperatorInput>;
  readonly birthtime: Maybe<DateQueryOperatorInput>;
  readonly birthtimeMs: Maybe<FloatQueryOperatorInput>;
  readonly blksize: Maybe<IntQueryOperatorInput>;
  readonly blocks: Maybe<IntQueryOperatorInput>;
  readonly id: Maybe<StringQueryOperatorInput>;
  readonly parent: Maybe<NodeFilterInput>;
  readonly children: Maybe<NodeFilterListInput>;
  readonly internal: Maybe<InternalFilterInput>;
};

type DirectoryGroupConnection = {
  readonly totalCount: Scalars['Int'];
  readonly edges: ReadonlyArray<DirectoryEdge>;
  readonly nodes: ReadonlyArray<Directory>;
  readonly pageInfo: PageInfo;
  readonly field: Scalars['String'];
  readonly fieldValue: Maybe<Scalars['String']>;
};

type DirectorySortInput = {
  readonly fields: Maybe<ReadonlyArray<Maybe<DirectoryFieldsEnum>>>;
  readonly order: Maybe<ReadonlyArray<Maybe<SortOrderEnum>>>;
};

type DriverMtPhGitJson = Node & {
  readonly id: Scalars['ID'];
  readonly parent: Maybe<Node>;
  readonly children: ReadonlyArray<Node>;
  readonly internal: Internal;
  readonly _schema: Maybe<Scalars['String']>;
  readonly title: Maybe<Scalars['String']>;
  readonly type: Maybe<Scalars['String']>;
  readonly required: Maybe<ReadonlyArray<Maybe<Scalars['String']>>>;
  readonly properties: Maybe<DriverMtPhGitJsonProperties>;
};

type DriverMtPhGitJsonConnection = {
  readonly totalCount: Scalars['Int'];
  readonly edges: ReadonlyArray<DriverMtPhGitJsonEdge>;
  readonly nodes: ReadonlyArray<DriverMtPhGitJson>;
  readonly pageInfo: PageInfo;
  readonly distinct: ReadonlyArray<Scalars['String']>;
  readonly group: ReadonlyArray<DriverMtPhGitJsonGroupConnection>;
};


type DriverMtPhGitJsonConnection_distinctArgs = {
  field: DriverMtPhGitJsonFieldsEnum;
};


type DriverMtPhGitJsonConnection_groupArgs = {
  skip: Maybe<Scalars['Int']>;
  limit: Maybe<Scalars['Int']>;
  field: DriverMtPhGitJsonFieldsEnum;
};

type DriverMtPhGitJsonEdge = {
  readonly next: Maybe<DriverMtPhGitJson>;
  readonly node: DriverMtPhGitJson;
  readonly previous: Maybe<DriverMtPhGitJson>;
};

enum DriverMtPhGitJsonFieldsEnum {
  id = 'id',
  parent___id = 'parent.id',
  parent___parent___id = 'parent.parent.id',
  parent___parent___parent___id = 'parent.parent.parent.id',
  parent___parent___parent___children = 'parent.parent.parent.children',
  parent___parent___children = 'parent.parent.children',
  parent___parent___children___id = 'parent.parent.children.id',
  parent___parent___children___children = 'parent.parent.children.children',
  parent___parent___internal___content = 'parent.parent.internal.content',
  parent___parent___internal___contentDigest = 'parent.parent.internal.contentDigest',
  parent___parent___internal___description = 'parent.parent.internal.description',
  parent___parent___internal___fieldOwners = 'parent.parent.internal.fieldOwners',
  parent___parent___internal___ignoreType = 'parent.parent.internal.ignoreType',
  parent___parent___internal___mediaType = 'parent.parent.internal.mediaType',
  parent___parent___internal___owner = 'parent.parent.internal.owner',
  parent___parent___internal___type = 'parent.parent.internal.type',
  parent___children = 'parent.children',
  parent___children___id = 'parent.children.id',
  parent___children___parent___id = 'parent.children.parent.id',
  parent___children___parent___children = 'parent.children.parent.children',
  parent___children___children = 'parent.children.children',
  parent___children___children___id = 'parent.children.children.id',
  parent___children___children___children = 'parent.children.children.children',
  parent___children___internal___content = 'parent.children.internal.content',
  parent___children___internal___contentDigest = 'parent.children.internal.contentDigest',
  parent___children___internal___description = 'parent.children.internal.description',
  parent___children___internal___fieldOwners = 'parent.children.internal.fieldOwners',
  parent___children___internal___ignoreType = 'parent.children.internal.ignoreType',
  parent___children___internal___mediaType = 'parent.children.internal.mediaType',
  parent___children___internal___owner = 'parent.children.internal.owner',
  parent___children___internal___type = 'parent.children.internal.type',
  parent___internal___content = 'parent.internal.content',
  parent___internal___contentDigest = 'parent.internal.contentDigest',
  parent___internal___description = 'parent.internal.description',
  parent___internal___fieldOwners = 'parent.internal.fieldOwners',
  parent___internal___ignoreType = 'parent.internal.ignoreType',
  parent___internal___mediaType = 'parent.internal.mediaType',
  parent___internal___owner = 'parent.internal.owner',
  parent___internal___type = 'parent.internal.type',
  children = 'children',
  children___id = 'children.id',
  children___parent___id = 'children.parent.id',
  children___parent___parent___id = 'children.parent.parent.id',
  children___parent___parent___children = 'children.parent.parent.children',
  children___parent___children = 'children.parent.children',
  children___parent___children___id = 'children.parent.children.id',
  children___parent___children___children = 'children.parent.children.children',
  children___parent___internal___content = 'children.parent.internal.content',
  children___parent___internal___contentDigest = 'children.parent.internal.contentDigest',
  children___parent___internal___description = 'children.parent.internal.description',
  children___parent___internal___fieldOwners = 'children.parent.internal.fieldOwners',
  children___parent___internal___ignoreType = 'children.parent.internal.ignoreType',
  children___parent___internal___mediaType = 'children.parent.internal.mediaType',
  children___parent___internal___owner = 'children.parent.internal.owner',
  children___parent___internal___type = 'children.parent.internal.type',
  children___children = 'children.children',
  children___children___id = 'children.children.id',
  children___children___parent___id = 'children.children.parent.id',
  children___children___parent___children = 'children.children.parent.children',
  children___children___children = 'children.children.children',
  children___children___children___id = 'children.children.children.id',
  children___children___children___children = 'children.children.children.children',
  children___children___internal___content = 'children.children.internal.content',
  children___children___internal___contentDigest = 'children.children.internal.contentDigest',
  children___children___internal___description = 'children.children.internal.description',
  children___children___internal___fieldOwners = 'children.children.internal.fieldOwners',
  children___children___internal___ignoreType = 'children.children.internal.ignoreType',
  children___children___internal___mediaType = 'children.children.internal.mediaType',
  children___children___internal___owner = 'children.children.internal.owner',
  children___children___internal___type = 'children.children.internal.type',
  children___internal___content = 'children.internal.content',
  children___internal___contentDigest = 'children.internal.contentDigest',
  children___internal___description = 'children.internal.description',
  children___internal___fieldOwners = 'children.internal.fieldOwners',
  children___internal___ignoreType = 'children.internal.ignoreType',
  children___internal___mediaType = 'children.internal.mediaType',
  children___internal___owner = 'children.internal.owner',
  children___internal___type = 'children.internal.type',
  internal___content = 'internal.content',
  internal___contentDigest = 'internal.contentDigest',
  internal___description = 'internal.description',
  internal___fieldOwners = 'internal.fieldOwners',
  internal___ignoreType = 'internal.ignoreType',
  internal___mediaType = 'internal.mediaType',
  internal___owner = 'internal.owner',
  internal___type = 'internal.type',
  _schema = '_schema',
  title = 'title',
  type = 'type',
  required = 'required',
  properties___usb_serial___type = 'properties.usb_serial.type'
}

type DriverMtPhGitJsonFilterInput = {
  readonly id: Maybe<StringQueryOperatorInput>;
  readonly parent: Maybe<NodeFilterInput>;
  readonly children: Maybe<NodeFilterListInput>;
  readonly internal: Maybe<InternalFilterInput>;
  readonly _schema: Maybe<StringQueryOperatorInput>;
  readonly title: Maybe<StringQueryOperatorInput>;
  readonly type: Maybe<StringQueryOperatorInput>;
  readonly required: Maybe<StringQueryOperatorInput>;
  readonly properties: Maybe<DriverMtPhGitJsonPropertiesFilterInput>;
};

type DriverMtPhGitJsonGroupConnection = {
  readonly totalCount: Scalars['Int'];
  readonly edges: ReadonlyArray<DriverMtPhGitJsonEdge>;
  readonly nodes: ReadonlyArray<DriverMtPhGitJson>;
  readonly pageInfo: PageInfo;
  readonly field: Scalars['String'];
  readonly fieldValue: Maybe<Scalars['String']>;
};

type DriverMtPhGitJsonProperties = {
  readonly usb_serial: Maybe<DriverMtPhGitJsonPropertiesUsb_serial>;
};

type DriverMtPhGitJsonPropertiesFilterInput = {
  readonly usb_serial: Maybe<DriverMtPhGitJsonPropertiesUsb_serialFilterInput>;
};

type DriverMtPhGitJsonPropertiesUsb_serial = {
  readonly type: Maybe<Scalars['String']>;
  readonly properties: Maybe<DriverMtPhGitJsonPropertiesUsb_serialProperties>;
};

type DriverMtPhGitJsonPropertiesUsb_serialFilterInput = {
  readonly type: Maybe<StringQueryOperatorInput>;
  readonly properties: Maybe<DriverMtPhGitJsonPropertiesUsb_serialPropertiesFilterInput>;
};

type DriverMtPhGitJsonPropertiesUsb_serialProperties = {
  readonly baudrate: Maybe<DriverMtPhGitJsonPropertiesUsb_serialPropertiesBaudrate>;
  readonly parity: Maybe<DriverMtPhGitJsonPropertiesUsb_serialPropertiesParity>;
  readonly databits: Maybe<DriverMtPhGitJsonPropertiesUsb_serialPropertiesDatabits>;
  readonly stopbits: Maybe<DriverMtPhGitJsonPropertiesUsb_serialPropertiesStopbits>;
  readonly rtscts: Maybe<DriverMtPhGitJsonPropertiesUsb_serialPropertiesRtscts>;
};

type DriverMtPhGitJsonPropertiesUsb_serialPropertiesBaudrate = {
  readonly type: Maybe<Scalars['String']>;
  readonly enum: Maybe<ReadonlyArray<Maybe<Scalars['Int']>>>;
};

type DriverMtPhGitJsonPropertiesUsb_serialPropertiesBaudrateFilterInput = {
  readonly type: Maybe<StringQueryOperatorInput>;
  readonly enum: Maybe<IntQueryOperatorInput>;
};

type DriverMtPhGitJsonPropertiesUsb_serialPropertiesDatabits = {
  readonly type: Maybe<Scalars['String']>;
  readonly enum: Maybe<ReadonlyArray<Maybe<Scalars['Int']>>>;
};

type DriverMtPhGitJsonPropertiesUsb_serialPropertiesDatabitsFilterInput = {
  readonly type: Maybe<StringQueryOperatorInput>;
  readonly enum: Maybe<IntQueryOperatorInput>;
};

type DriverMtPhGitJsonPropertiesUsb_serialPropertiesFilterInput = {
  readonly baudrate: Maybe<DriverMtPhGitJsonPropertiesUsb_serialPropertiesBaudrateFilterInput>;
  readonly parity: Maybe<DriverMtPhGitJsonPropertiesUsb_serialPropertiesParityFilterInput>;
  readonly databits: Maybe<DriverMtPhGitJsonPropertiesUsb_serialPropertiesDatabitsFilterInput>;
  readonly stopbits: Maybe<DriverMtPhGitJsonPropertiesUsb_serialPropertiesStopbitsFilterInput>;
  readonly rtscts: Maybe<DriverMtPhGitJsonPropertiesUsb_serialPropertiesRtsctsFilterInput>;
};

type DriverMtPhGitJsonPropertiesUsb_serialPropertiesParity = {
  readonly type: Maybe<Scalars['String']>;
  readonly enum: Maybe<ReadonlyArray<Maybe<Scalars['String']>>>;
};

type DriverMtPhGitJsonPropertiesUsb_serialPropertiesParityFilterInput = {
  readonly type: Maybe<StringQueryOperatorInput>;
  readonly enum: Maybe<StringQueryOperatorInput>;
};

type DriverMtPhGitJsonPropertiesUsb_serialPropertiesRtscts = {
  readonly type: Maybe<Scalars['String']>;
  readonly default: Maybe<Scalars['Boolean']>;
};

type DriverMtPhGitJsonPropertiesUsb_serialPropertiesRtsctsFilterInput = {
  readonly type: Maybe<StringQueryOperatorInput>;
  readonly default: Maybe<BooleanQueryOperatorInput>;
};

type DriverMtPhGitJsonPropertiesUsb_serialPropertiesStopbits = {
  readonly type: Maybe<Scalars['String']>;
  readonly enum: Maybe<ReadonlyArray<Maybe<Scalars['Int']>>>;
};

type DriverMtPhGitJsonPropertiesUsb_serialPropertiesStopbitsFilterInput = {
  readonly type: Maybe<StringQueryOperatorInput>;
  readonly enum: Maybe<IntQueryOperatorInput>;
};

type DriverMtPhGitJsonSortInput = {
  readonly fields: Maybe<ReadonlyArray<Maybe<DriverMtPhGitJsonFieldsEnum>>>;
  readonly order: Maybe<ReadonlyArray<Maybe<SortOrderEnum>>>;
};

type DriverSicsserialGitJson = Node & {
  readonly id: Scalars['ID'];
  readonly parent: Maybe<Node>;
  readonly children: ReadonlyArray<Node>;
  readonly internal: Internal;
  readonly _schema: Maybe<Scalars['String']>;
  readonly title: Maybe<Scalars['String']>;
  readonly type: Maybe<Scalars['String']>;
  readonly required: Maybe<ReadonlyArray<Maybe<Scalars['String']>>>;
  readonly properties: Maybe<DriverSicsserialGitJsonProperties>;
};

type DriverSicsserialGitJsonConnection = {
  readonly totalCount: Scalars['Int'];
  readonly edges: ReadonlyArray<DriverSicsserialGitJsonEdge>;
  readonly nodes: ReadonlyArray<DriverSicsserialGitJson>;
  readonly pageInfo: PageInfo;
  readonly distinct: ReadonlyArray<Scalars['String']>;
  readonly group: ReadonlyArray<DriverSicsserialGitJsonGroupConnection>;
};


type DriverSicsserialGitJsonConnection_distinctArgs = {
  field: DriverSicsserialGitJsonFieldsEnum;
};


type DriverSicsserialGitJsonConnection_groupArgs = {
  skip: Maybe<Scalars['Int']>;
  limit: Maybe<Scalars['Int']>;
  field: DriverSicsserialGitJsonFieldsEnum;
};

type DriverSicsserialGitJsonEdge = {
  readonly next: Maybe<DriverSicsserialGitJson>;
  readonly node: DriverSicsserialGitJson;
  readonly previous: Maybe<DriverSicsserialGitJson>;
};

enum DriverSicsserialGitJsonFieldsEnum {
  id = 'id',
  parent___id = 'parent.id',
  parent___parent___id = 'parent.parent.id',
  parent___parent___parent___id = 'parent.parent.parent.id',
  parent___parent___parent___children = 'parent.parent.parent.children',
  parent___parent___children = 'parent.parent.children',
  parent___parent___children___id = 'parent.parent.children.id',
  parent___parent___children___children = 'parent.parent.children.children',
  parent___parent___internal___content = 'parent.parent.internal.content',
  parent___parent___internal___contentDigest = 'parent.parent.internal.contentDigest',
  parent___parent___internal___description = 'parent.parent.internal.description',
  parent___parent___internal___fieldOwners = 'parent.parent.internal.fieldOwners',
  parent___parent___internal___ignoreType = 'parent.parent.internal.ignoreType',
  parent___parent___internal___mediaType = 'parent.parent.internal.mediaType',
  parent___parent___internal___owner = 'parent.parent.internal.owner',
  parent___parent___internal___type = 'parent.parent.internal.type',
  parent___children = 'parent.children',
  parent___children___id = 'parent.children.id',
  parent___children___parent___id = 'parent.children.parent.id',
  parent___children___parent___children = 'parent.children.parent.children',
  parent___children___children = 'parent.children.children',
  parent___children___children___id = 'parent.children.children.id',
  parent___children___children___children = 'parent.children.children.children',
  parent___children___internal___content = 'parent.children.internal.content',
  parent___children___internal___contentDigest = 'parent.children.internal.contentDigest',
  parent___children___internal___description = 'parent.children.internal.description',
  parent___children___internal___fieldOwners = 'parent.children.internal.fieldOwners',
  parent___children___internal___ignoreType = 'parent.children.internal.ignoreType',
  parent___children___internal___mediaType = 'parent.children.internal.mediaType',
  parent___children___internal___owner = 'parent.children.internal.owner',
  parent___children___internal___type = 'parent.children.internal.type',
  parent___internal___content = 'parent.internal.content',
  parent___internal___contentDigest = 'parent.internal.contentDigest',
  parent___internal___description = 'parent.internal.description',
  parent___internal___fieldOwners = 'parent.internal.fieldOwners',
  parent___internal___ignoreType = 'parent.internal.ignoreType',
  parent___internal___mediaType = 'parent.internal.mediaType',
  parent___internal___owner = 'parent.internal.owner',
  parent___internal___type = 'parent.internal.type',
  children = 'children',
  children___id = 'children.id',
  children___parent___id = 'children.parent.id',
  children___parent___parent___id = 'children.parent.parent.id',
  children___parent___parent___children = 'children.parent.parent.children',
  children___parent___children = 'children.parent.children',
  children___parent___children___id = 'children.parent.children.id',
  children___parent___children___children = 'children.parent.children.children',
  children___parent___internal___content = 'children.parent.internal.content',
  children___parent___internal___contentDigest = 'children.parent.internal.contentDigest',
  children___parent___internal___description = 'children.parent.internal.description',
  children___parent___internal___fieldOwners = 'children.parent.internal.fieldOwners',
  children___parent___internal___ignoreType = 'children.parent.internal.ignoreType',
  children___parent___internal___mediaType = 'children.parent.internal.mediaType',
  children___parent___internal___owner = 'children.parent.internal.owner',
  children___parent___internal___type = 'children.parent.internal.type',
  children___children = 'children.children',
  children___children___id = 'children.children.id',
  children___children___parent___id = 'children.children.parent.id',
  children___children___parent___children = 'children.children.parent.children',
  children___children___children = 'children.children.children',
  children___children___children___id = 'children.children.children.id',
  children___children___children___children = 'children.children.children.children',
  children___children___internal___content = 'children.children.internal.content',
  children___children___internal___contentDigest = 'children.children.internal.contentDigest',
  children___children___internal___description = 'children.children.internal.description',
  children___children___internal___fieldOwners = 'children.children.internal.fieldOwners',
  children___children___internal___ignoreType = 'children.children.internal.ignoreType',
  children___children___internal___mediaType = 'children.children.internal.mediaType',
  children___children___internal___owner = 'children.children.internal.owner',
  children___children___internal___type = 'children.children.internal.type',
  children___internal___content = 'children.internal.content',
  children___internal___contentDigest = 'children.internal.contentDigest',
  children___internal___description = 'children.internal.description',
  children___internal___fieldOwners = 'children.internal.fieldOwners',
  children___internal___ignoreType = 'children.internal.ignoreType',
  children___internal___mediaType = 'children.internal.mediaType',
  children___internal___owner = 'children.internal.owner',
  children___internal___type = 'children.internal.type',
  internal___content = 'internal.content',
  internal___contentDigest = 'internal.contentDigest',
  internal___description = 'internal.description',
  internal___fieldOwners = 'internal.fieldOwners',
  internal___ignoreType = 'internal.ignoreType',
  internal___mediaType = 'internal.mediaType',
  internal___owner = 'internal.owner',
  internal___type = 'internal.type',
  _schema = '_schema',
  title = 'title',
  type = 'type',
  required = 'required',
  properties___usb_serial___type = 'properties.usb_serial.type'
}

type DriverSicsserialGitJsonFilterInput = {
  readonly id: Maybe<StringQueryOperatorInput>;
  readonly parent: Maybe<NodeFilterInput>;
  readonly children: Maybe<NodeFilterListInput>;
  readonly internal: Maybe<InternalFilterInput>;
  readonly _schema: Maybe<StringQueryOperatorInput>;
  readonly title: Maybe<StringQueryOperatorInput>;
  readonly type: Maybe<StringQueryOperatorInput>;
  readonly required: Maybe<StringQueryOperatorInput>;
  readonly properties: Maybe<DriverSicsserialGitJsonPropertiesFilterInput>;
};

type DriverSicsserialGitJsonGroupConnection = {
  readonly totalCount: Scalars['Int'];
  readonly edges: ReadonlyArray<DriverSicsserialGitJsonEdge>;
  readonly nodes: ReadonlyArray<DriverSicsserialGitJson>;
  readonly pageInfo: PageInfo;
  readonly field: Scalars['String'];
  readonly fieldValue: Maybe<Scalars['String']>;
};

type DriverSicsserialGitJsonProperties = {
  readonly usb_serial: Maybe<DriverSicsserialGitJsonPropertiesUsb_serial>;
};

type DriverSicsserialGitJsonPropertiesFilterInput = {
  readonly usb_serial: Maybe<DriverSicsserialGitJsonPropertiesUsb_serialFilterInput>;
};

type DriverSicsserialGitJsonPropertiesUsb_serial = {
  readonly type: Maybe<Scalars['String']>;
  readonly properties: Maybe<DriverSicsserialGitJsonPropertiesUsb_serialProperties>;
};

type DriverSicsserialGitJsonPropertiesUsb_serialFilterInput = {
  readonly type: Maybe<StringQueryOperatorInput>;
  readonly properties: Maybe<DriverSicsserialGitJsonPropertiesUsb_serialPropertiesFilterInput>;
};

type DriverSicsserialGitJsonPropertiesUsb_serialProperties = {
  readonly baudrate: Maybe<DriverSicsserialGitJsonPropertiesUsb_serialPropertiesBaudrate>;
  readonly parity: Maybe<DriverSicsserialGitJsonPropertiesUsb_serialPropertiesParity>;
  readonly databits: Maybe<DriverSicsserialGitJsonPropertiesUsb_serialPropertiesDatabits>;
  readonly stopbits: Maybe<DriverSicsserialGitJsonPropertiesUsb_serialPropertiesStopbits>;
  readonly rtscts: Maybe<DriverSicsserialGitJsonPropertiesUsb_serialPropertiesRtscts>;
};

type DriverSicsserialGitJsonPropertiesUsb_serialPropertiesBaudrate = {
  readonly type: Maybe<Scalars['String']>;
  readonly enum: Maybe<ReadonlyArray<Maybe<Scalars['Int']>>>;
};

type DriverSicsserialGitJsonPropertiesUsb_serialPropertiesBaudrateFilterInput = {
  readonly type: Maybe<StringQueryOperatorInput>;
  readonly enum: Maybe<IntQueryOperatorInput>;
};

type DriverSicsserialGitJsonPropertiesUsb_serialPropertiesDatabits = {
  readonly type: Maybe<Scalars['String']>;
  readonly enum: Maybe<ReadonlyArray<Maybe<Scalars['Int']>>>;
};

type DriverSicsserialGitJsonPropertiesUsb_serialPropertiesDatabitsFilterInput = {
  readonly type: Maybe<StringQueryOperatorInput>;
  readonly enum: Maybe<IntQueryOperatorInput>;
};

type DriverSicsserialGitJsonPropertiesUsb_serialPropertiesFilterInput = {
  readonly baudrate: Maybe<DriverSicsserialGitJsonPropertiesUsb_serialPropertiesBaudrateFilterInput>;
  readonly parity: Maybe<DriverSicsserialGitJsonPropertiesUsb_serialPropertiesParityFilterInput>;
  readonly databits: Maybe<DriverSicsserialGitJsonPropertiesUsb_serialPropertiesDatabitsFilterInput>;
  readonly stopbits: Maybe<DriverSicsserialGitJsonPropertiesUsb_serialPropertiesStopbitsFilterInput>;
  readonly rtscts: Maybe<DriverSicsserialGitJsonPropertiesUsb_serialPropertiesRtsctsFilterInput>;
};

type DriverSicsserialGitJsonPropertiesUsb_serialPropertiesParity = {
  readonly type: Maybe<Scalars['String']>;
  readonly enum: Maybe<ReadonlyArray<Maybe<Scalars['String']>>>;
};

type DriverSicsserialGitJsonPropertiesUsb_serialPropertiesParityFilterInput = {
  readonly type: Maybe<StringQueryOperatorInput>;
  readonly enum: Maybe<StringQueryOperatorInput>;
};

type DriverSicsserialGitJsonPropertiesUsb_serialPropertiesRtscts = {
  readonly type: Maybe<Scalars['String']>;
  readonly default: Maybe<Scalars['Boolean']>;
};

type DriverSicsserialGitJsonPropertiesUsb_serialPropertiesRtsctsFilterInput = {
  readonly type: Maybe<StringQueryOperatorInput>;
  readonly default: Maybe<BooleanQueryOperatorInput>;
};

type DriverSicsserialGitJsonPropertiesUsb_serialPropertiesStopbits = {
  readonly type: Maybe<Scalars['String']>;
  readonly enum: Maybe<ReadonlyArray<Maybe<Scalars['Int']>>>;
};

type DriverSicsserialGitJsonPropertiesUsb_serialPropertiesStopbitsFilterInput = {
  readonly type: Maybe<StringQueryOperatorInput>;
  readonly enum: Maybe<IntQueryOperatorInput>;
};

type DriverSicsserialGitJsonSortInput = {
  readonly fields: Maybe<ReadonlyArray<Maybe<DriverSicsserialGitJsonFieldsEnum>>>;
  readonly order: Maybe<ReadonlyArray<Maybe<SortOrderEnum>>>;
};

type DuotoneGradient = {
  readonly highlight: Scalars['String'];
  readonly shadow: Scalars['String'];
  readonly opacity: Maybe<Scalars['Int']>;
};

type File = Node & {
  readonly sourceInstanceName: Scalars['String'];
  readonly absolutePath: Scalars['String'];
  readonly relativePath: Scalars['String'];
  readonly extension: Scalars['String'];
  readonly size: Scalars['Int'];
  readonly prettySize: Scalars['String'];
  readonly modifiedTime: Scalars['Date'];
  readonly accessTime: Scalars['Date'];
  readonly changeTime: Scalars['Date'];
  readonly birthTime: Scalars['Date'];
  readonly root: Scalars['String'];
  readonly dir: Scalars['String'];
  readonly base: Scalars['String'];
  readonly ext: Scalars['String'];
  readonly name: Scalars['String'];
  readonly relativeDirectory: Scalars['String'];
  readonly dev: Scalars['Int'];
  readonly mode: Scalars['Int'];
  readonly nlink: Scalars['Int'];
  readonly uid: Scalars['Int'];
  readonly gid: Scalars['Int'];
  readonly rdev: Scalars['Int'];
  readonly ino: Scalars['Float'];
  readonly atimeMs: Scalars['Float'];
  readonly mtimeMs: Scalars['Float'];
  readonly ctimeMs: Scalars['Float'];
  readonly atime: Scalars['Date'];
  readonly mtime: Scalars['Date'];
  readonly ctime: Scalars['Date'];
  /** @deprecated Use `birthTime` instead */
  readonly birthtime: Maybe<Scalars['Date']>;
  /** @deprecated Use `birthTime` instead */
  readonly birthtimeMs: Maybe<Scalars['Float']>;
  readonly blksize: Maybe<Scalars['Int']>;
  readonly blocks: Maybe<Scalars['Int']>;
  readonly gitRemote: Maybe<GitRemote>;
  /** Copy file to static directory and return public url to it */
  readonly publicURL: Maybe<Scalars['String']>;
  readonly childImageSharp: Maybe<ImageSharp>;
  readonly id: Scalars['ID'];
  readonly parent: Maybe<Node>;
  readonly children: ReadonlyArray<Node>;
  readonly internal: Internal;
  readonly childMdx: Maybe<Mdx>;
  readonly childDriverMtPhGitJson: Maybe<DriverMtPhGitJson>;
  readonly childDriverSicsserialGitJson: Maybe<DriverSicsserialGitJson>;
};


type File_modifiedTimeArgs = {
  formatString: Maybe<Scalars['String']>;
  fromNow: Maybe<Scalars['Boolean']>;
  difference: Maybe<Scalars['String']>;
  locale: Maybe<Scalars['String']>;
};


type File_accessTimeArgs = {
  formatString: Maybe<Scalars['String']>;
  fromNow: Maybe<Scalars['Boolean']>;
  difference: Maybe<Scalars['String']>;
  locale: Maybe<Scalars['String']>;
};


type File_changeTimeArgs = {
  formatString: Maybe<Scalars['String']>;
  fromNow: Maybe<Scalars['Boolean']>;
  difference: Maybe<Scalars['String']>;
  locale: Maybe<Scalars['String']>;
};


type File_birthTimeArgs = {
  formatString: Maybe<Scalars['String']>;
  fromNow: Maybe<Scalars['Boolean']>;
  difference: Maybe<Scalars['String']>;
  locale: Maybe<Scalars['String']>;
};


type File_atimeArgs = {
  formatString: Maybe<Scalars['String']>;
  fromNow: Maybe<Scalars['Boolean']>;
  difference: Maybe<Scalars['String']>;
  locale: Maybe<Scalars['String']>;
};


type File_mtimeArgs = {
  formatString: Maybe<Scalars['String']>;
  fromNow: Maybe<Scalars['Boolean']>;
  difference: Maybe<Scalars['String']>;
  locale: Maybe<Scalars['String']>;
};


type File_ctimeArgs = {
  formatString: Maybe<Scalars['String']>;
  fromNow: Maybe<Scalars['Boolean']>;
  difference: Maybe<Scalars['String']>;
  locale: Maybe<Scalars['String']>;
};

type FileConnection = {
  readonly totalCount: Scalars['Int'];
  readonly edges: ReadonlyArray<FileEdge>;
  readonly nodes: ReadonlyArray<File>;
  readonly pageInfo: PageInfo;
  readonly distinct: ReadonlyArray<Scalars['String']>;
  readonly group: ReadonlyArray<FileGroupConnection>;
};


type FileConnection_distinctArgs = {
  field: FileFieldsEnum;
};


type FileConnection_groupArgs = {
  skip: Maybe<Scalars['Int']>;
  limit: Maybe<Scalars['Int']>;
  field: FileFieldsEnum;
};

type FileEdge = {
  readonly next: Maybe<File>;
  readonly node: File;
  readonly previous: Maybe<File>;
};

enum FileFieldsEnum {
  sourceInstanceName = 'sourceInstanceName',
  absolutePath = 'absolutePath',
  relativePath = 'relativePath',
  extension = 'extension',
  size = 'size',
  prettySize = 'prettySize',
  modifiedTime = 'modifiedTime',
  accessTime = 'accessTime',
  changeTime = 'changeTime',
  birthTime = 'birthTime',
  root = 'root',
  dir = 'dir',
  base = 'base',
  ext = 'ext',
  name = 'name',
  relativeDirectory = 'relativeDirectory',
  dev = 'dev',
  mode = 'mode',
  nlink = 'nlink',
  uid = 'uid',
  gid = 'gid',
  rdev = 'rdev',
  ino = 'ino',
  atimeMs = 'atimeMs',
  mtimeMs = 'mtimeMs',
  ctimeMs = 'ctimeMs',
  atime = 'atime',
  mtime = 'mtime',
  ctime = 'ctime',
  birthtime = 'birthtime',
  birthtimeMs = 'birthtimeMs',
  blksize = 'blksize',
  blocks = 'blocks',
  gitRemote___id = 'gitRemote.id',
  gitRemote___parent___id = 'gitRemote.parent.id',
  gitRemote___parent___parent___id = 'gitRemote.parent.parent.id',
  gitRemote___parent___parent___children = 'gitRemote.parent.parent.children',
  gitRemote___parent___children = 'gitRemote.parent.children',
  gitRemote___parent___children___id = 'gitRemote.parent.children.id',
  gitRemote___parent___children___children = 'gitRemote.parent.children.children',
  gitRemote___parent___internal___content = 'gitRemote.parent.internal.content',
  gitRemote___parent___internal___contentDigest = 'gitRemote.parent.internal.contentDigest',
  gitRemote___parent___internal___description = 'gitRemote.parent.internal.description',
  gitRemote___parent___internal___fieldOwners = 'gitRemote.parent.internal.fieldOwners',
  gitRemote___parent___internal___ignoreType = 'gitRemote.parent.internal.ignoreType',
  gitRemote___parent___internal___mediaType = 'gitRemote.parent.internal.mediaType',
  gitRemote___parent___internal___owner = 'gitRemote.parent.internal.owner',
  gitRemote___parent___internal___type = 'gitRemote.parent.internal.type',
  gitRemote___children = 'gitRemote.children',
  gitRemote___children___id = 'gitRemote.children.id',
  gitRemote___children___parent___id = 'gitRemote.children.parent.id',
  gitRemote___children___parent___children = 'gitRemote.children.parent.children',
  gitRemote___children___children = 'gitRemote.children.children',
  gitRemote___children___children___id = 'gitRemote.children.children.id',
  gitRemote___children___children___children = 'gitRemote.children.children.children',
  gitRemote___children___internal___content = 'gitRemote.children.internal.content',
  gitRemote___children___internal___contentDigest = 'gitRemote.children.internal.contentDigest',
  gitRemote___children___internal___description = 'gitRemote.children.internal.description',
  gitRemote___children___internal___fieldOwners = 'gitRemote.children.internal.fieldOwners',
  gitRemote___children___internal___ignoreType = 'gitRemote.children.internal.ignoreType',
  gitRemote___children___internal___mediaType = 'gitRemote.children.internal.mediaType',
  gitRemote___children___internal___owner = 'gitRemote.children.internal.owner',
  gitRemote___children___internal___type = 'gitRemote.children.internal.type',
  gitRemote___internal___content = 'gitRemote.internal.content',
  gitRemote___internal___contentDigest = 'gitRemote.internal.contentDigest',
  gitRemote___internal___description = 'gitRemote.internal.description',
  gitRemote___internal___fieldOwners = 'gitRemote.internal.fieldOwners',
  gitRemote___internal___ignoreType = 'gitRemote.internal.ignoreType',
  gitRemote___internal___mediaType = 'gitRemote.internal.mediaType',
  gitRemote___internal___owner = 'gitRemote.internal.owner',
  gitRemote___internal___type = 'gitRemote.internal.type',
  gitRemote___protocol = 'gitRemote.protocol',
  gitRemote___resource = 'gitRemote.resource',
  gitRemote___user = 'gitRemote.user',
  gitRemote___pathname = 'gitRemote.pathname',
  gitRemote___hash = 'gitRemote.hash',
  gitRemote___search = 'gitRemote.search',
  gitRemote___href = 'gitRemote.href',
  gitRemote___token = 'gitRemote.token',
  gitRemote___source = 'gitRemote.source',
  gitRemote___name = 'gitRemote.name',
  gitRemote___owner = 'gitRemote.owner',
  gitRemote___ref = 'gitRemote.ref',
  gitRemote___filepathtype = 'gitRemote.filepathtype',
  gitRemote___filepath = 'gitRemote.filepath',
  gitRemote___organization = 'gitRemote.organization',
  gitRemote___full_name = 'gitRemote.full_name',
  gitRemote___webLink = 'gitRemote.webLink',
  gitRemote___sourceInstanceName = 'gitRemote.sourceInstanceName',
  gitRemote___childrenFile = 'gitRemote.childrenFile',
  gitRemote___childrenFile___sourceInstanceName = 'gitRemote.childrenFile.sourceInstanceName',
  gitRemote___childrenFile___absolutePath = 'gitRemote.childrenFile.absolutePath',
  gitRemote___childrenFile___relativePath = 'gitRemote.childrenFile.relativePath',
  gitRemote___childrenFile___extension = 'gitRemote.childrenFile.extension',
  gitRemote___childrenFile___size = 'gitRemote.childrenFile.size',
  gitRemote___childrenFile___prettySize = 'gitRemote.childrenFile.prettySize',
  gitRemote___childrenFile___modifiedTime = 'gitRemote.childrenFile.modifiedTime',
  gitRemote___childrenFile___accessTime = 'gitRemote.childrenFile.accessTime',
  gitRemote___childrenFile___changeTime = 'gitRemote.childrenFile.changeTime',
  gitRemote___childrenFile___birthTime = 'gitRemote.childrenFile.birthTime',
  gitRemote___childrenFile___root = 'gitRemote.childrenFile.root',
  gitRemote___childrenFile___dir = 'gitRemote.childrenFile.dir',
  gitRemote___childrenFile___base = 'gitRemote.childrenFile.base',
  gitRemote___childrenFile___ext = 'gitRemote.childrenFile.ext',
  gitRemote___childrenFile___name = 'gitRemote.childrenFile.name',
  gitRemote___childrenFile___relativeDirectory = 'gitRemote.childrenFile.relativeDirectory',
  gitRemote___childrenFile___dev = 'gitRemote.childrenFile.dev',
  gitRemote___childrenFile___mode = 'gitRemote.childrenFile.mode',
  gitRemote___childrenFile___nlink = 'gitRemote.childrenFile.nlink',
  gitRemote___childrenFile___uid = 'gitRemote.childrenFile.uid',
  gitRemote___childrenFile___gid = 'gitRemote.childrenFile.gid',
  gitRemote___childrenFile___rdev = 'gitRemote.childrenFile.rdev',
  gitRemote___childrenFile___ino = 'gitRemote.childrenFile.ino',
  gitRemote___childrenFile___atimeMs = 'gitRemote.childrenFile.atimeMs',
  gitRemote___childrenFile___mtimeMs = 'gitRemote.childrenFile.mtimeMs',
  gitRemote___childrenFile___ctimeMs = 'gitRemote.childrenFile.ctimeMs',
  gitRemote___childrenFile___atime = 'gitRemote.childrenFile.atime',
  gitRemote___childrenFile___mtime = 'gitRemote.childrenFile.mtime',
  gitRemote___childrenFile___ctime = 'gitRemote.childrenFile.ctime',
  gitRemote___childrenFile___birthtime = 'gitRemote.childrenFile.birthtime',
  gitRemote___childrenFile___birthtimeMs = 'gitRemote.childrenFile.birthtimeMs',
  gitRemote___childrenFile___blksize = 'gitRemote.childrenFile.blksize',
  gitRemote___childrenFile___blocks = 'gitRemote.childrenFile.blocks',
  gitRemote___childrenFile___gitRemote___id = 'gitRemote.childrenFile.gitRemote.id',
  gitRemote___childrenFile___gitRemote___children = 'gitRemote.childrenFile.gitRemote.children',
  gitRemote___childrenFile___gitRemote___protocol = 'gitRemote.childrenFile.gitRemote.protocol',
  gitRemote___childrenFile___gitRemote___resource = 'gitRemote.childrenFile.gitRemote.resource',
  gitRemote___childrenFile___gitRemote___user = 'gitRemote.childrenFile.gitRemote.user',
  gitRemote___childrenFile___gitRemote___pathname = 'gitRemote.childrenFile.gitRemote.pathname',
  gitRemote___childrenFile___gitRemote___hash = 'gitRemote.childrenFile.gitRemote.hash',
  gitRemote___childrenFile___gitRemote___search = 'gitRemote.childrenFile.gitRemote.search',
  gitRemote___childrenFile___gitRemote___href = 'gitRemote.childrenFile.gitRemote.href',
  gitRemote___childrenFile___gitRemote___token = 'gitRemote.childrenFile.gitRemote.token',
  gitRemote___childrenFile___gitRemote___source = 'gitRemote.childrenFile.gitRemote.source',
  gitRemote___childrenFile___gitRemote___name = 'gitRemote.childrenFile.gitRemote.name',
  gitRemote___childrenFile___gitRemote___owner = 'gitRemote.childrenFile.gitRemote.owner',
  gitRemote___childrenFile___gitRemote___ref = 'gitRemote.childrenFile.gitRemote.ref',
  gitRemote___childrenFile___gitRemote___filepathtype = 'gitRemote.childrenFile.gitRemote.filepathtype',
  gitRemote___childrenFile___gitRemote___filepath = 'gitRemote.childrenFile.gitRemote.filepath',
  gitRemote___childrenFile___gitRemote___organization = 'gitRemote.childrenFile.gitRemote.organization',
  gitRemote___childrenFile___gitRemote___full_name = 'gitRemote.childrenFile.gitRemote.full_name',
  gitRemote___childrenFile___gitRemote___webLink = 'gitRemote.childrenFile.gitRemote.webLink',
  gitRemote___childrenFile___gitRemote___sourceInstanceName = 'gitRemote.childrenFile.gitRemote.sourceInstanceName',
  gitRemote___childrenFile___gitRemote___childrenFile = 'gitRemote.childrenFile.gitRemote.childrenFile',
  gitRemote___childrenFile___publicURL = 'gitRemote.childrenFile.publicURL',
  gitRemote___childrenFile___childImageSharp___id = 'gitRemote.childrenFile.childImageSharp.id',
  gitRemote___childrenFile___childImageSharp___children = 'gitRemote.childrenFile.childImageSharp.children',
  gitRemote___childrenFile___id = 'gitRemote.childrenFile.id',
  gitRemote___childrenFile___parent___id = 'gitRemote.childrenFile.parent.id',
  gitRemote___childrenFile___parent___children = 'gitRemote.childrenFile.parent.children',
  gitRemote___childrenFile___children = 'gitRemote.childrenFile.children',
  gitRemote___childrenFile___children___id = 'gitRemote.childrenFile.children.id',
  gitRemote___childrenFile___children___children = 'gitRemote.childrenFile.children.children',
  gitRemote___childrenFile___internal___content = 'gitRemote.childrenFile.internal.content',
  gitRemote___childrenFile___internal___contentDigest = 'gitRemote.childrenFile.internal.contentDigest',
  gitRemote___childrenFile___internal___description = 'gitRemote.childrenFile.internal.description',
  gitRemote___childrenFile___internal___fieldOwners = 'gitRemote.childrenFile.internal.fieldOwners',
  gitRemote___childrenFile___internal___ignoreType = 'gitRemote.childrenFile.internal.ignoreType',
  gitRemote___childrenFile___internal___mediaType = 'gitRemote.childrenFile.internal.mediaType',
  gitRemote___childrenFile___internal___owner = 'gitRemote.childrenFile.internal.owner',
  gitRemote___childrenFile___internal___type = 'gitRemote.childrenFile.internal.type',
  gitRemote___childrenFile___childMdx___rawBody = 'gitRemote.childrenFile.childMdx.rawBody',
  gitRemote___childrenFile___childMdx___fileAbsolutePath = 'gitRemote.childrenFile.childMdx.fileAbsolutePath',
  gitRemote___childrenFile___childMdx___slug = 'gitRemote.childrenFile.childMdx.slug',
  gitRemote___childrenFile___childMdx___body = 'gitRemote.childrenFile.childMdx.body',
  gitRemote___childrenFile___childMdx___excerpt = 'gitRemote.childrenFile.childMdx.excerpt',
  gitRemote___childrenFile___childMdx___headings = 'gitRemote.childrenFile.childMdx.headings',
  gitRemote___childrenFile___childMdx___html = 'gitRemote.childrenFile.childMdx.html',
  gitRemote___childrenFile___childMdx___mdxAST = 'gitRemote.childrenFile.childMdx.mdxAST',
  gitRemote___childrenFile___childMdx___tableOfContents = 'gitRemote.childrenFile.childMdx.tableOfContents',
  gitRemote___childrenFile___childMdx___timeToRead = 'gitRemote.childrenFile.childMdx.timeToRead',
  gitRemote___childrenFile___childMdx___id = 'gitRemote.childrenFile.childMdx.id',
  gitRemote___childrenFile___childMdx___children = 'gitRemote.childrenFile.childMdx.children',
  gitRemote___childrenFile___childDriverMtPhGitJson___id = 'gitRemote.childrenFile.childDriverMtPhGitJson.id',
  gitRemote___childrenFile___childDriverMtPhGitJson___children = 'gitRemote.childrenFile.childDriverMtPhGitJson.children',
  gitRemote___childrenFile___childDriverMtPhGitJson____schema = 'gitRemote.childrenFile.childDriverMtPhGitJson._schema',
  gitRemote___childrenFile___childDriverMtPhGitJson___title = 'gitRemote.childrenFile.childDriverMtPhGitJson.title',
  gitRemote___childrenFile___childDriverMtPhGitJson___type = 'gitRemote.childrenFile.childDriverMtPhGitJson.type',
  gitRemote___childrenFile___childDriverMtPhGitJson___required = 'gitRemote.childrenFile.childDriverMtPhGitJson.required',
  gitRemote___childrenFile___childDriverSicsserialGitJson___id = 'gitRemote.childrenFile.childDriverSicsserialGitJson.id',
  gitRemote___childrenFile___childDriverSicsserialGitJson___children = 'gitRemote.childrenFile.childDriverSicsserialGitJson.children',
  gitRemote___childrenFile___childDriverSicsserialGitJson____schema = 'gitRemote.childrenFile.childDriverSicsserialGitJson._schema',
  gitRemote___childrenFile___childDriverSicsserialGitJson___title = 'gitRemote.childrenFile.childDriverSicsserialGitJson.title',
  gitRemote___childrenFile___childDriverSicsserialGitJson___type = 'gitRemote.childrenFile.childDriverSicsserialGitJson.type',
  gitRemote___childrenFile___childDriverSicsserialGitJson___required = 'gitRemote.childrenFile.childDriverSicsserialGitJson.required',
  publicURL = 'publicURL',
  childImageSharp___fixed___base64 = 'childImageSharp.fixed.base64',
  childImageSharp___fixed___tracedSVG = 'childImageSharp.fixed.tracedSVG',
  childImageSharp___fixed___aspectRatio = 'childImageSharp.fixed.aspectRatio',
  childImageSharp___fixed___width = 'childImageSharp.fixed.width',
  childImageSharp___fixed___height = 'childImageSharp.fixed.height',
  childImageSharp___fixed___src = 'childImageSharp.fixed.src',
  childImageSharp___fixed___srcSet = 'childImageSharp.fixed.srcSet',
  childImageSharp___fixed___srcWebp = 'childImageSharp.fixed.srcWebp',
  childImageSharp___fixed___srcSetWebp = 'childImageSharp.fixed.srcSetWebp',
  childImageSharp___fixed___originalName = 'childImageSharp.fixed.originalName',
  childImageSharp___resolutions___base64 = 'childImageSharp.resolutions.base64',
  childImageSharp___resolutions___tracedSVG = 'childImageSharp.resolutions.tracedSVG',
  childImageSharp___resolutions___aspectRatio = 'childImageSharp.resolutions.aspectRatio',
  childImageSharp___resolutions___width = 'childImageSharp.resolutions.width',
  childImageSharp___resolutions___height = 'childImageSharp.resolutions.height',
  childImageSharp___resolutions___src = 'childImageSharp.resolutions.src',
  childImageSharp___resolutions___srcSet = 'childImageSharp.resolutions.srcSet',
  childImageSharp___resolutions___srcWebp = 'childImageSharp.resolutions.srcWebp',
  childImageSharp___resolutions___srcSetWebp = 'childImageSharp.resolutions.srcSetWebp',
  childImageSharp___resolutions___originalName = 'childImageSharp.resolutions.originalName',
  childImageSharp___fluid___base64 = 'childImageSharp.fluid.base64',
  childImageSharp___fluid___tracedSVG = 'childImageSharp.fluid.tracedSVG',
  childImageSharp___fluid___aspectRatio = 'childImageSharp.fluid.aspectRatio',
  childImageSharp___fluid___src = 'childImageSharp.fluid.src',
  childImageSharp___fluid___srcSet = 'childImageSharp.fluid.srcSet',
  childImageSharp___fluid___srcWebp = 'childImageSharp.fluid.srcWebp',
  childImageSharp___fluid___srcSetWebp = 'childImageSharp.fluid.srcSetWebp',
  childImageSharp___fluid___sizes = 'childImageSharp.fluid.sizes',
  childImageSharp___fluid___originalImg = 'childImageSharp.fluid.originalImg',
  childImageSharp___fluid___originalName = 'childImageSharp.fluid.originalName',
  childImageSharp___fluid___presentationWidth = 'childImageSharp.fluid.presentationWidth',
  childImageSharp___fluid___presentationHeight = 'childImageSharp.fluid.presentationHeight',
  childImageSharp___sizes___base64 = 'childImageSharp.sizes.base64',
  childImageSharp___sizes___tracedSVG = 'childImageSharp.sizes.tracedSVG',
  childImageSharp___sizes___aspectRatio = 'childImageSharp.sizes.aspectRatio',
  childImageSharp___sizes___src = 'childImageSharp.sizes.src',
  childImageSharp___sizes___srcSet = 'childImageSharp.sizes.srcSet',
  childImageSharp___sizes___srcWebp = 'childImageSharp.sizes.srcWebp',
  childImageSharp___sizes___srcSetWebp = 'childImageSharp.sizes.srcSetWebp',
  childImageSharp___sizes___sizes = 'childImageSharp.sizes.sizes',
  childImageSharp___sizes___originalImg = 'childImageSharp.sizes.originalImg',
  childImageSharp___sizes___originalName = 'childImageSharp.sizes.originalName',
  childImageSharp___sizes___presentationWidth = 'childImageSharp.sizes.presentationWidth',
  childImageSharp___sizes___presentationHeight = 'childImageSharp.sizes.presentationHeight',
  childImageSharp___original___width = 'childImageSharp.original.width',
  childImageSharp___original___height = 'childImageSharp.original.height',
  childImageSharp___original___src = 'childImageSharp.original.src',
  childImageSharp___resize___src = 'childImageSharp.resize.src',
  childImageSharp___resize___tracedSVG = 'childImageSharp.resize.tracedSVG',
  childImageSharp___resize___width = 'childImageSharp.resize.width',
  childImageSharp___resize___height = 'childImageSharp.resize.height',
  childImageSharp___resize___aspectRatio = 'childImageSharp.resize.aspectRatio',
  childImageSharp___resize___originalName = 'childImageSharp.resize.originalName',
  childImageSharp___id = 'childImageSharp.id',
  childImageSharp___parent___id = 'childImageSharp.parent.id',
  childImageSharp___parent___parent___id = 'childImageSharp.parent.parent.id',
  childImageSharp___parent___parent___children = 'childImageSharp.parent.parent.children',
  childImageSharp___parent___children = 'childImageSharp.parent.children',
  childImageSharp___parent___children___id = 'childImageSharp.parent.children.id',
  childImageSharp___parent___children___children = 'childImageSharp.parent.children.children',
  childImageSharp___parent___internal___content = 'childImageSharp.parent.internal.content',
  childImageSharp___parent___internal___contentDigest = 'childImageSharp.parent.internal.contentDigest',
  childImageSharp___parent___internal___description = 'childImageSharp.parent.internal.description',
  childImageSharp___parent___internal___fieldOwners = 'childImageSharp.parent.internal.fieldOwners',
  childImageSharp___parent___internal___ignoreType = 'childImageSharp.parent.internal.ignoreType',
  childImageSharp___parent___internal___mediaType = 'childImageSharp.parent.internal.mediaType',
  childImageSharp___parent___internal___owner = 'childImageSharp.parent.internal.owner',
  childImageSharp___parent___internal___type = 'childImageSharp.parent.internal.type',
  childImageSharp___children = 'childImageSharp.children',
  childImageSharp___children___id = 'childImageSharp.children.id',
  childImageSharp___children___parent___id = 'childImageSharp.children.parent.id',
  childImageSharp___children___parent___children = 'childImageSharp.children.parent.children',
  childImageSharp___children___children = 'childImageSharp.children.children',
  childImageSharp___children___children___id = 'childImageSharp.children.children.id',
  childImageSharp___children___children___children = 'childImageSharp.children.children.children',
  childImageSharp___children___internal___content = 'childImageSharp.children.internal.content',
  childImageSharp___children___internal___contentDigest = 'childImageSharp.children.internal.contentDigest',
  childImageSharp___children___internal___description = 'childImageSharp.children.internal.description',
  childImageSharp___children___internal___fieldOwners = 'childImageSharp.children.internal.fieldOwners',
  childImageSharp___children___internal___ignoreType = 'childImageSharp.children.internal.ignoreType',
  childImageSharp___children___internal___mediaType = 'childImageSharp.children.internal.mediaType',
  childImageSharp___children___internal___owner = 'childImageSharp.children.internal.owner',
  childImageSharp___children___internal___type = 'childImageSharp.children.internal.type',
  childImageSharp___internal___content = 'childImageSharp.internal.content',
  childImageSharp___internal___contentDigest = 'childImageSharp.internal.contentDigest',
  childImageSharp___internal___description = 'childImageSharp.internal.description',
  childImageSharp___internal___fieldOwners = 'childImageSharp.internal.fieldOwners',
  childImageSharp___internal___ignoreType = 'childImageSharp.internal.ignoreType',
  childImageSharp___internal___mediaType = 'childImageSharp.internal.mediaType',
  childImageSharp___internal___owner = 'childImageSharp.internal.owner',
  childImageSharp___internal___type = 'childImageSharp.internal.type',
  id = 'id',
  parent___id = 'parent.id',
  parent___parent___id = 'parent.parent.id',
  parent___parent___parent___id = 'parent.parent.parent.id',
  parent___parent___parent___children = 'parent.parent.parent.children',
  parent___parent___children = 'parent.parent.children',
  parent___parent___children___id = 'parent.parent.children.id',
  parent___parent___children___children = 'parent.parent.children.children',
  parent___parent___internal___content = 'parent.parent.internal.content',
  parent___parent___internal___contentDigest = 'parent.parent.internal.contentDigest',
  parent___parent___internal___description = 'parent.parent.internal.description',
  parent___parent___internal___fieldOwners = 'parent.parent.internal.fieldOwners',
  parent___parent___internal___ignoreType = 'parent.parent.internal.ignoreType',
  parent___parent___internal___mediaType = 'parent.parent.internal.mediaType',
  parent___parent___internal___owner = 'parent.parent.internal.owner',
  parent___parent___internal___type = 'parent.parent.internal.type',
  parent___children = 'parent.children',
  parent___children___id = 'parent.children.id',
  parent___children___parent___id = 'parent.children.parent.id',
  parent___children___parent___children = 'parent.children.parent.children',
  parent___children___children = 'parent.children.children',
  parent___children___children___id = 'parent.children.children.id',
  parent___children___children___children = 'parent.children.children.children',
  parent___children___internal___content = 'parent.children.internal.content',
  parent___children___internal___contentDigest = 'parent.children.internal.contentDigest',
  parent___children___internal___description = 'parent.children.internal.description',
  parent___children___internal___fieldOwners = 'parent.children.internal.fieldOwners',
  parent___children___internal___ignoreType = 'parent.children.internal.ignoreType',
  parent___children___internal___mediaType = 'parent.children.internal.mediaType',
  parent___children___internal___owner = 'parent.children.internal.owner',
  parent___children___internal___type = 'parent.children.internal.type',
  parent___internal___content = 'parent.internal.content',
  parent___internal___contentDigest = 'parent.internal.contentDigest',
  parent___internal___description = 'parent.internal.description',
  parent___internal___fieldOwners = 'parent.internal.fieldOwners',
  parent___internal___ignoreType = 'parent.internal.ignoreType',
  parent___internal___mediaType = 'parent.internal.mediaType',
  parent___internal___owner = 'parent.internal.owner',
  parent___internal___type = 'parent.internal.type',
  children = 'children',
  children___id = 'children.id',
  children___parent___id = 'children.parent.id',
  children___parent___parent___id = 'children.parent.parent.id',
  children___parent___parent___children = 'children.parent.parent.children',
  children___parent___children = 'children.parent.children',
  children___parent___children___id = 'children.parent.children.id',
  children___parent___children___children = 'children.parent.children.children',
  children___parent___internal___content = 'children.parent.internal.content',
  children___parent___internal___contentDigest = 'children.parent.internal.contentDigest',
  children___parent___internal___description = 'children.parent.internal.description',
  children___parent___internal___fieldOwners = 'children.parent.internal.fieldOwners',
  children___parent___internal___ignoreType = 'children.parent.internal.ignoreType',
  children___parent___internal___mediaType = 'children.parent.internal.mediaType',
  children___parent___internal___owner = 'children.parent.internal.owner',
  children___parent___internal___type = 'children.parent.internal.type',
  children___children = 'children.children',
  children___children___id = 'children.children.id',
  children___children___parent___id = 'children.children.parent.id',
  children___children___parent___children = 'children.children.parent.children',
  children___children___children = 'children.children.children',
  children___children___children___id = 'children.children.children.id',
  children___children___children___children = 'children.children.children.children',
  children___children___internal___content = 'children.children.internal.content',
  children___children___internal___contentDigest = 'children.children.internal.contentDigest',
  children___children___internal___description = 'children.children.internal.description',
  children___children___internal___fieldOwners = 'children.children.internal.fieldOwners',
  children___children___internal___ignoreType = 'children.children.internal.ignoreType',
  children___children___internal___mediaType = 'children.children.internal.mediaType',
  children___children___internal___owner = 'children.children.internal.owner',
  children___children___internal___type = 'children.children.internal.type',
  children___internal___content = 'children.internal.content',
  children___internal___contentDigest = 'children.internal.contentDigest',
  children___internal___description = 'children.internal.description',
  children___internal___fieldOwners = 'children.internal.fieldOwners',
  children___internal___ignoreType = 'children.internal.ignoreType',
  children___internal___mediaType = 'children.internal.mediaType',
  children___internal___owner = 'children.internal.owner',
  children___internal___type = 'children.internal.type',
  internal___content = 'internal.content',
  internal___contentDigest = 'internal.contentDigest',
  internal___description = 'internal.description',
  internal___fieldOwners = 'internal.fieldOwners',
  internal___ignoreType = 'internal.ignoreType',
  internal___mediaType = 'internal.mediaType',
  internal___owner = 'internal.owner',
  internal___type = 'internal.type',
  childMdx___frontmatter___title = 'childMdx.frontmatter.title',
  childMdx___frontmatter___subheader = 'childMdx.frontmatter.subheader',
  childMdx___frontmatter___order = 'childMdx.frontmatter.order',
  childMdx___frontmatter___embeddedImages = 'childMdx.frontmatter.embeddedImages',
  childMdx___frontmatter___embeddedImages___sourceInstanceName = 'childMdx.frontmatter.embeddedImages.sourceInstanceName',
  childMdx___frontmatter___embeddedImages___absolutePath = 'childMdx.frontmatter.embeddedImages.absolutePath',
  childMdx___frontmatter___embeddedImages___relativePath = 'childMdx.frontmatter.embeddedImages.relativePath',
  childMdx___frontmatter___embeddedImages___extension = 'childMdx.frontmatter.embeddedImages.extension',
  childMdx___frontmatter___embeddedImages___size = 'childMdx.frontmatter.embeddedImages.size',
  childMdx___frontmatter___embeddedImages___prettySize = 'childMdx.frontmatter.embeddedImages.prettySize',
  childMdx___frontmatter___embeddedImages___modifiedTime = 'childMdx.frontmatter.embeddedImages.modifiedTime',
  childMdx___frontmatter___embeddedImages___accessTime = 'childMdx.frontmatter.embeddedImages.accessTime',
  childMdx___frontmatter___embeddedImages___changeTime = 'childMdx.frontmatter.embeddedImages.changeTime',
  childMdx___frontmatter___embeddedImages___birthTime = 'childMdx.frontmatter.embeddedImages.birthTime',
  childMdx___frontmatter___embeddedImages___root = 'childMdx.frontmatter.embeddedImages.root',
  childMdx___frontmatter___embeddedImages___dir = 'childMdx.frontmatter.embeddedImages.dir',
  childMdx___frontmatter___embeddedImages___base = 'childMdx.frontmatter.embeddedImages.base',
  childMdx___frontmatter___embeddedImages___ext = 'childMdx.frontmatter.embeddedImages.ext',
  childMdx___frontmatter___embeddedImages___name = 'childMdx.frontmatter.embeddedImages.name',
  childMdx___frontmatter___embeddedImages___relativeDirectory = 'childMdx.frontmatter.embeddedImages.relativeDirectory',
  childMdx___frontmatter___embeddedImages___dev = 'childMdx.frontmatter.embeddedImages.dev',
  childMdx___frontmatter___embeddedImages___mode = 'childMdx.frontmatter.embeddedImages.mode',
  childMdx___frontmatter___embeddedImages___nlink = 'childMdx.frontmatter.embeddedImages.nlink',
  childMdx___frontmatter___embeddedImages___uid = 'childMdx.frontmatter.embeddedImages.uid',
  childMdx___frontmatter___embeddedImages___gid = 'childMdx.frontmatter.embeddedImages.gid',
  childMdx___frontmatter___embeddedImages___rdev = 'childMdx.frontmatter.embeddedImages.rdev',
  childMdx___frontmatter___embeddedImages___ino = 'childMdx.frontmatter.embeddedImages.ino',
  childMdx___frontmatter___embeddedImages___atimeMs = 'childMdx.frontmatter.embeddedImages.atimeMs',
  childMdx___frontmatter___embeddedImages___mtimeMs = 'childMdx.frontmatter.embeddedImages.mtimeMs',
  childMdx___frontmatter___embeddedImages___ctimeMs = 'childMdx.frontmatter.embeddedImages.ctimeMs',
  childMdx___frontmatter___embeddedImages___atime = 'childMdx.frontmatter.embeddedImages.atime',
  childMdx___frontmatter___embeddedImages___mtime = 'childMdx.frontmatter.embeddedImages.mtime',
  childMdx___frontmatter___embeddedImages___ctime = 'childMdx.frontmatter.embeddedImages.ctime',
  childMdx___frontmatter___embeddedImages___birthtime = 'childMdx.frontmatter.embeddedImages.birthtime',
  childMdx___frontmatter___embeddedImages___birthtimeMs = 'childMdx.frontmatter.embeddedImages.birthtimeMs',
  childMdx___frontmatter___embeddedImages___blksize = 'childMdx.frontmatter.embeddedImages.blksize',
  childMdx___frontmatter___embeddedImages___blocks = 'childMdx.frontmatter.embeddedImages.blocks',
  childMdx___frontmatter___embeddedImages___publicURL = 'childMdx.frontmatter.embeddedImages.publicURL',
  childMdx___frontmatter___embeddedImages___id = 'childMdx.frontmatter.embeddedImages.id',
  childMdx___frontmatter___embeddedImages___children = 'childMdx.frontmatter.embeddedImages.children',
  childMdx___rawBody = 'childMdx.rawBody',
  childMdx___fileAbsolutePath = 'childMdx.fileAbsolutePath',
  childMdx___slug = 'childMdx.slug',
  childMdx___body = 'childMdx.body',
  childMdx___excerpt = 'childMdx.excerpt',
  childMdx___headings = 'childMdx.headings',
  childMdx___headings___value = 'childMdx.headings.value',
  childMdx___headings___depth = 'childMdx.headings.depth',
  childMdx___html = 'childMdx.html',
  childMdx___mdxAST = 'childMdx.mdxAST',
  childMdx___tableOfContents = 'childMdx.tableOfContents',
  childMdx___timeToRead = 'childMdx.timeToRead',
  childMdx___wordCount___paragraphs = 'childMdx.wordCount.paragraphs',
  childMdx___wordCount___sentences = 'childMdx.wordCount.sentences',
  childMdx___wordCount___words = 'childMdx.wordCount.words',
  childMdx___fields___slug = 'childMdx.fields.slug',
  childMdx___fields___sourceInstanceName = 'childMdx.fields.sourceInstanceName',
  childMdx___fields___title = 'childMdx.fields.title',
  childMdx___id = 'childMdx.id',
  childMdx___parent___id = 'childMdx.parent.id',
  childMdx___parent___parent___id = 'childMdx.parent.parent.id',
  childMdx___parent___parent___children = 'childMdx.parent.parent.children',
  childMdx___parent___children = 'childMdx.parent.children',
  childMdx___parent___children___id = 'childMdx.parent.children.id',
  childMdx___parent___children___children = 'childMdx.parent.children.children',
  childMdx___parent___internal___content = 'childMdx.parent.internal.content',
  childMdx___parent___internal___contentDigest = 'childMdx.parent.internal.contentDigest',
  childMdx___parent___internal___description = 'childMdx.parent.internal.description',
  childMdx___parent___internal___fieldOwners = 'childMdx.parent.internal.fieldOwners',
  childMdx___parent___internal___ignoreType = 'childMdx.parent.internal.ignoreType',
  childMdx___parent___internal___mediaType = 'childMdx.parent.internal.mediaType',
  childMdx___parent___internal___owner = 'childMdx.parent.internal.owner',
  childMdx___parent___internal___type = 'childMdx.parent.internal.type',
  childMdx___children = 'childMdx.children',
  childMdx___children___id = 'childMdx.children.id',
  childMdx___children___parent___id = 'childMdx.children.parent.id',
  childMdx___children___parent___children = 'childMdx.children.parent.children',
  childMdx___children___children = 'childMdx.children.children',
  childMdx___children___children___id = 'childMdx.children.children.id',
  childMdx___children___children___children = 'childMdx.children.children.children',
  childMdx___children___internal___content = 'childMdx.children.internal.content',
  childMdx___children___internal___contentDigest = 'childMdx.children.internal.contentDigest',
  childMdx___children___internal___description = 'childMdx.children.internal.description',
  childMdx___children___internal___fieldOwners = 'childMdx.children.internal.fieldOwners',
  childMdx___children___internal___ignoreType = 'childMdx.children.internal.ignoreType',
  childMdx___children___internal___mediaType = 'childMdx.children.internal.mediaType',
  childMdx___children___internal___owner = 'childMdx.children.internal.owner',
  childMdx___children___internal___type = 'childMdx.children.internal.type',
  childMdx___internal___content = 'childMdx.internal.content',
  childMdx___internal___contentDigest = 'childMdx.internal.contentDigest',
  childMdx___internal___description = 'childMdx.internal.description',
  childMdx___internal___fieldOwners = 'childMdx.internal.fieldOwners',
  childMdx___internal___ignoreType = 'childMdx.internal.ignoreType',
  childMdx___internal___mediaType = 'childMdx.internal.mediaType',
  childMdx___internal___owner = 'childMdx.internal.owner',
  childMdx___internal___type = 'childMdx.internal.type',
  childDriverMtPhGitJson___id = 'childDriverMtPhGitJson.id',
  childDriverMtPhGitJson___parent___id = 'childDriverMtPhGitJson.parent.id',
  childDriverMtPhGitJson___parent___parent___id = 'childDriverMtPhGitJson.parent.parent.id',
  childDriverMtPhGitJson___parent___parent___children = 'childDriverMtPhGitJson.parent.parent.children',
  childDriverMtPhGitJson___parent___children = 'childDriverMtPhGitJson.parent.children',
  childDriverMtPhGitJson___parent___children___id = 'childDriverMtPhGitJson.parent.children.id',
  childDriverMtPhGitJson___parent___children___children = 'childDriverMtPhGitJson.parent.children.children',
  childDriverMtPhGitJson___parent___internal___content = 'childDriverMtPhGitJson.parent.internal.content',
  childDriverMtPhGitJson___parent___internal___contentDigest = 'childDriverMtPhGitJson.parent.internal.contentDigest',
  childDriverMtPhGitJson___parent___internal___description = 'childDriverMtPhGitJson.parent.internal.description',
  childDriverMtPhGitJson___parent___internal___fieldOwners = 'childDriverMtPhGitJson.parent.internal.fieldOwners',
  childDriverMtPhGitJson___parent___internal___ignoreType = 'childDriverMtPhGitJson.parent.internal.ignoreType',
  childDriverMtPhGitJson___parent___internal___mediaType = 'childDriverMtPhGitJson.parent.internal.mediaType',
  childDriverMtPhGitJson___parent___internal___owner = 'childDriverMtPhGitJson.parent.internal.owner',
  childDriverMtPhGitJson___parent___internal___type = 'childDriverMtPhGitJson.parent.internal.type',
  childDriverMtPhGitJson___children = 'childDriverMtPhGitJson.children',
  childDriverMtPhGitJson___children___id = 'childDriverMtPhGitJson.children.id',
  childDriverMtPhGitJson___children___parent___id = 'childDriverMtPhGitJson.children.parent.id',
  childDriverMtPhGitJson___children___parent___children = 'childDriverMtPhGitJson.children.parent.children',
  childDriverMtPhGitJson___children___children = 'childDriverMtPhGitJson.children.children',
  childDriverMtPhGitJson___children___children___id = 'childDriverMtPhGitJson.children.children.id',
  childDriverMtPhGitJson___children___children___children = 'childDriverMtPhGitJson.children.children.children',
  childDriverMtPhGitJson___children___internal___content = 'childDriverMtPhGitJson.children.internal.content',
  childDriverMtPhGitJson___children___internal___contentDigest = 'childDriverMtPhGitJson.children.internal.contentDigest',
  childDriverMtPhGitJson___children___internal___description = 'childDriverMtPhGitJson.children.internal.description',
  childDriverMtPhGitJson___children___internal___fieldOwners = 'childDriverMtPhGitJson.children.internal.fieldOwners',
  childDriverMtPhGitJson___children___internal___ignoreType = 'childDriverMtPhGitJson.children.internal.ignoreType',
  childDriverMtPhGitJson___children___internal___mediaType = 'childDriverMtPhGitJson.children.internal.mediaType',
  childDriverMtPhGitJson___children___internal___owner = 'childDriverMtPhGitJson.children.internal.owner',
  childDriverMtPhGitJson___children___internal___type = 'childDriverMtPhGitJson.children.internal.type',
  childDriverMtPhGitJson___internal___content = 'childDriverMtPhGitJson.internal.content',
  childDriverMtPhGitJson___internal___contentDigest = 'childDriverMtPhGitJson.internal.contentDigest',
  childDriverMtPhGitJson___internal___description = 'childDriverMtPhGitJson.internal.description',
  childDriverMtPhGitJson___internal___fieldOwners = 'childDriverMtPhGitJson.internal.fieldOwners',
  childDriverMtPhGitJson___internal___ignoreType = 'childDriverMtPhGitJson.internal.ignoreType',
  childDriverMtPhGitJson___internal___mediaType = 'childDriverMtPhGitJson.internal.mediaType',
  childDriverMtPhGitJson___internal___owner = 'childDriverMtPhGitJson.internal.owner',
  childDriverMtPhGitJson___internal___type = 'childDriverMtPhGitJson.internal.type',
  childDriverMtPhGitJson____schema = 'childDriverMtPhGitJson._schema',
  childDriverMtPhGitJson___title = 'childDriverMtPhGitJson.title',
  childDriverMtPhGitJson___type = 'childDriverMtPhGitJson.type',
  childDriverMtPhGitJson___required = 'childDriverMtPhGitJson.required',
  childDriverMtPhGitJson___properties___usb_serial___type = 'childDriverMtPhGitJson.properties.usb_serial.type',
  childDriverSicsserialGitJson___id = 'childDriverSicsserialGitJson.id',
  childDriverSicsserialGitJson___parent___id = 'childDriverSicsserialGitJson.parent.id',
  childDriverSicsserialGitJson___parent___parent___id = 'childDriverSicsserialGitJson.parent.parent.id',
  childDriverSicsserialGitJson___parent___parent___children = 'childDriverSicsserialGitJson.parent.parent.children',
  childDriverSicsserialGitJson___parent___children = 'childDriverSicsserialGitJson.parent.children',
  childDriverSicsserialGitJson___parent___children___id = 'childDriverSicsserialGitJson.parent.children.id',
  childDriverSicsserialGitJson___parent___children___children = 'childDriverSicsserialGitJson.parent.children.children',
  childDriverSicsserialGitJson___parent___internal___content = 'childDriverSicsserialGitJson.parent.internal.content',
  childDriverSicsserialGitJson___parent___internal___contentDigest = 'childDriverSicsserialGitJson.parent.internal.contentDigest',
  childDriverSicsserialGitJson___parent___internal___description = 'childDriverSicsserialGitJson.parent.internal.description',
  childDriverSicsserialGitJson___parent___internal___fieldOwners = 'childDriverSicsserialGitJson.parent.internal.fieldOwners',
  childDriverSicsserialGitJson___parent___internal___ignoreType = 'childDriverSicsserialGitJson.parent.internal.ignoreType',
  childDriverSicsserialGitJson___parent___internal___mediaType = 'childDriverSicsserialGitJson.parent.internal.mediaType',
  childDriverSicsserialGitJson___parent___internal___owner = 'childDriverSicsserialGitJson.parent.internal.owner',
  childDriverSicsserialGitJson___parent___internal___type = 'childDriverSicsserialGitJson.parent.internal.type',
  childDriverSicsserialGitJson___children = 'childDriverSicsserialGitJson.children',
  childDriverSicsserialGitJson___children___id = 'childDriverSicsserialGitJson.children.id',
  childDriverSicsserialGitJson___children___parent___id = 'childDriverSicsserialGitJson.children.parent.id',
  childDriverSicsserialGitJson___children___parent___children = 'childDriverSicsserialGitJson.children.parent.children',
  childDriverSicsserialGitJson___children___children = 'childDriverSicsserialGitJson.children.children',
  childDriverSicsserialGitJson___children___children___id = 'childDriverSicsserialGitJson.children.children.id',
  childDriverSicsserialGitJson___children___children___children = 'childDriverSicsserialGitJson.children.children.children',
  childDriverSicsserialGitJson___children___internal___content = 'childDriverSicsserialGitJson.children.internal.content',
  childDriverSicsserialGitJson___children___internal___contentDigest = 'childDriverSicsserialGitJson.children.internal.contentDigest',
  childDriverSicsserialGitJson___children___internal___description = 'childDriverSicsserialGitJson.children.internal.description',
  childDriverSicsserialGitJson___children___internal___fieldOwners = 'childDriverSicsserialGitJson.children.internal.fieldOwners',
  childDriverSicsserialGitJson___children___internal___ignoreType = 'childDriverSicsserialGitJson.children.internal.ignoreType',
  childDriverSicsserialGitJson___children___internal___mediaType = 'childDriverSicsserialGitJson.children.internal.mediaType',
  childDriverSicsserialGitJson___children___internal___owner = 'childDriverSicsserialGitJson.children.internal.owner',
  childDriverSicsserialGitJson___children___internal___type = 'childDriverSicsserialGitJson.children.internal.type',
  childDriverSicsserialGitJson___internal___content = 'childDriverSicsserialGitJson.internal.content',
  childDriverSicsserialGitJson___internal___contentDigest = 'childDriverSicsserialGitJson.internal.contentDigest',
  childDriverSicsserialGitJson___internal___description = 'childDriverSicsserialGitJson.internal.description',
  childDriverSicsserialGitJson___internal___fieldOwners = 'childDriverSicsserialGitJson.internal.fieldOwners',
  childDriverSicsserialGitJson___internal___ignoreType = 'childDriverSicsserialGitJson.internal.ignoreType',
  childDriverSicsserialGitJson___internal___mediaType = 'childDriverSicsserialGitJson.internal.mediaType',
  childDriverSicsserialGitJson___internal___owner = 'childDriverSicsserialGitJson.internal.owner',
  childDriverSicsserialGitJson___internal___type = 'childDriverSicsserialGitJson.internal.type',
  childDriverSicsserialGitJson____schema = 'childDriverSicsserialGitJson._schema',
  childDriverSicsserialGitJson___title = 'childDriverSicsserialGitJson.title',
  childDriverSicsserialGitJson___type = 'childDriverSicsserialGitJson.type',
  childDriverSicsserialGitJson___required = 'childDriverSicsserialGitJson.required',
  childDriverSicsserialGitJson___properties___usb_serial___type = 'childDriverSicsserialGitJson.properties.usb_serial.type'
}

type FileFilterInput = {
  readonly sourceInstanceName: Maybe<StringQueryOperatorInput>;
  readonly absolutePath: Maybe<StringQueryOperatorInput>;
  readonly relativePath: Maybe<StringQueryOperatorInput>;
  readonly extension: Maybe<StringQueryOperatorInput>;
  readonly size: Maybe<IntQueryOperatorInput>;
  readonly prettySize: Maybe<StringQueryOperatorInput>;
  readonly modifiedTime: Maybe<DateQueryOperatorInput>;
  readonly accessTime: Maybe<DateQueryOperatorInput>;
  readonly changeTime: Maybe<DateQueryOperatorInput>;
  readonly birthTime: Maybe<DateQueryOperatorInput>;
  readonly root: Maybe<StringQueryOperatorInput>;
  readonly dir: Maybe<StringQueryOperatorInput>;
  readonly base: Maybe<StringQueryOperatorInput>;
  readonly ext: Maybe<StringQueryOperatorInput>;
  readonly name: Maybe<StringQueryOperatorInput>;
  readonly relativeDirectory: Maybe<StringQueryOperatorInput>;
  readonly dev: Maybe<IntQueryOperatorInput>;
  readonly mode: Maybe<IntQueryOperatorInput>;
  readonly nlink: Maybe<IntQueryOperatorInput>;
  readonly uid: Maybe<IntQueryOperatorInput>;
  readonly gid: Maybe<IntQueryOperatorInput>;
  readonly rdev: Maybe<IntQueryOperatorInput>;
  readonly ino: Maybe<FloatQueryOperatorInput>;
  readonly atimeMs: Maybe<FloatQueryOperatorInput>;
  readonly mtimeMs: Maybe<FloatQueryOperatorInput>;
  readonly ctimeMs: Maybe<FloatQueryOperatorInput>;
  readonly atime: Maybe<DateQueryOperatorInput>;
  readonly mtime: Maybe<DateQueryOperatorInput>;
  readonly ctime: Maybe<DateQueryOperatorInput>;
  readonly birthtime: Maybe<DateQueryOperatorInput>;
  readonly birthtimeMs: Maybe<FloatQueryOperatorInput>;
  readonly blksize: Maybe<IntQueryOperatorInput>;
  readonly blocks: Maybe<IntQueryOperatorInput>;
  readonly gitRemote: Maybe<GitRemoteFilterInput>;
  readonly publicURL: Maybe<StringQueryOperatorInput>;
  readonly childImageSharp: Maybe<ImageSharpFilterInput>;
  readonly id: Maybe<StringQueryOperatorInput>;
  readonly parent: Maybe<NodeFilterInput>;
  readonly children: Maybe<NodeFilterListInput>;
  readonly internal: Maybe<InternalFilterInput>;
  readonly childMdx: Maybe<MdxFilterInput>;
  readonly childDriverMtPhGitJson: Maybe<DriverMtPhGitJsonFilterInput>;
  readonly childDriverSicsserialGitJson: Maybe<DriverSicsserialGitJsonFilterInput>;
};

type FileFilterListInput = {
  readonly elemMatch: Maybe<FileFilterInput>;
};

type FileGroupConnection = {
  readonly totalCount: Scalars['Int'];
  readonly edges: ReadonlyArray<FileEdge>;
  readonly nodes: ReadonlyArray<File>;
  readonly pageInfo: PageInfo;
  readonly field: Scalars['String'];
  readonly fieldValue: Maybe<Scalars['String']>;
};

type FileSortInput = {
  readonly fields: Maybe<ReadonlyArray<Maybe<FileFieldsEnum>>>;
  readonly order: Maybe<ReadonlyArray<Maybe<SortOrderEnum>>>;
};

type FloatQueryOperatorInput = {
  readonly eq: Maybe<Scalars['Float']>;
  readonly ne: Maybe<Scalars['Float']>;
  readonly gt: Maybe<Scalars['Float']>;
  readonly gte: Maybe<Scalars['Float']>;
  readonly lt: Maybe<Scalars['Float']>;
  readonly lte: Maybe<Scalars['Float']>;
  readonly in: Maybe<ReadonlyArray<Maybe<Scalars['Float']>>>;
  readonly nin: Maybe<ReadonlyArray<Maybe<Scalars['Float']>>>;
};

type GitRemote = Node & {
  readonly id: Scalars['ID'];
  readonly parent: Maybe<Node>;
  readonly children: ReadonlyArray<Node>;
  readonly internal: Internal;
  readonly protocol: Maybe<Scalars['String']>;
  readonly resource: Maybe<Scalars['String']>;
  readonly user: Maybe<Scalars['String']>;
  readonly pathname: Maybe<Scalars['String']>;
  readonly hash: Maybe<Scalars['String']>;
  readonly search: Maybe<Scalars['String']>;
  readonly href: Maybe<Scalars['String']>;
  readonly token: Maybe<Scalars['String']>;
  readonly source: Maybe<Scalars['String']>;
  readonly name: Maybe<Scalars['String']>;
  readonly owner: Maybe<Scalars['String']>;
  readonly ref: Maybe<Scalars['String']>;
  readonly filepathtype: Maybe<Scalars['String']>;
  readonly filepath: Maybe<Scalars['String']>;
  readonly organization: Maybe<Scalars['String']>;
  readonly full_name: Maybe<Scalars['String']>;
  readonly webLink: Maybe<Scalars['String']>;
  readonly sourceInstanceName: Maybe<Scalars['String']>;
  readonly childrenFile: Maybe<ReadonlyArray<Maybe<File>>>;
};

type GitRemoteConnection = {
  readonly totalCount: Scalars['Int'];
  readonly edges: ReadonlyArray<GitRemoteEdge>;
  readonly nodes: ReadonlyArray<GitRemote>;
  readonly pageInfo: PageInfo;
  readonly distinct: ReadonlyArray<Scalars['String']>;
  readonly group: ReadonlyArray<GitRemoteGroupConnection>;
};


type GitRemoteConnection_distinctArgs = {
  field: GitRemoteFieldsEnum;
};


type GitRemoteConnection_groupArgs = {
  skip: Maybe<Scalars['Int']>;
  limit: Maybe<Scalars['Int']>;
  field: GitRemoteFieldsEnum;
};

type GitRemoteEdge = {
  readonly next: Maybe<GitRemote>;
  readonly node: GitRemote;
  readonly previous: Maybe<GitRemote>;
};

enum GitRemoteFieldsEnum {
  id = 'id',
  parent___id = 'parent.id',
  parent___parent___id = 'parent.parent.id',
  parent___parent___parent___id = 'parent.parent.parent.id',
  parent___parent___parent___children = 'parent.parent.parent.children',
  parent___parent___children = 'parent.parent.children',
  parent___parent___children___id = 'parent.parent.children.id',
  parent___parent___children___children = 'parent.parent.children.children',
  parent___parent___internal___content = 'parent.parent.internal.content',
  parent___parent___internal___contentDigest = 'parent.parent.internal.contentDigest',
  parent___parent___internal___description = 'parent.parent.internal.description',
  parent___parent___internal___fieldOwners = 'parent.parent.internal.fieldOwners',
  parent___parent___internal___ignoreType = 'parent.parent.internal.ignoreType',
  parent___parent___internal___mediaType = 'parent.parent.internal.mediaType',
  parent___parent___internal___owner = 'parent.parent.internal.owner',
  parent___parent___internal___type = 'parent.parent.internal.type',
  parent___children = 'parent.children',
  parent___children___id = 'parent.children.id',
  parent___children___parent___id = 'parent.children.parent.id',
  parent___children___parent___children = 'parent.children.parent.children',
  parent___children___children = 'parent.children.children',
  parent___children___children___id = 'parent.children.children.id',
  parent___children___children___children = 'parent.children.children.children',
  parent___children___internal___content = 'parent.children.internal.content',
  parent___children___internal___contentDigest = 'parent.children.internal.contentDigest',
  parent___children___internal___description = 'parent.children.internal.description',
  parent___children___internal___fieldOwners = 'parent.children.internal.fieldOwners',
  parent___children___internal___ignoreType = 'parent.children.internal.ignoreType',
  parent___children___internal___mediaType = 'parent.children.internal.mediaType',
  parent___children___internal___owner = 'parent.children.internal.owner',
  parent___children___internal___type = 'parent.children.internal.type',
  parent___internal___content = 'parent.internal.content',
  parent___internal___contentDigest = 'parent.internal.contentDigest',
  parent___internal___description = 'parent.internal.description',
  parent___internal___fieldOwners = 'parent.internal.fieldOwners',
  parent___internal___ignoreType = 'parent.internal.ignoreType',
  parent___internal___mediaType = 'parent.internal.mediaType',
  parent___internal___owner = 'parent.internal.owner',
  parent___internal___type = 'parent.internal.type',
  children = 'children',
  children___id = 'children.id',
  children___parent___id = 'children.parent.id',
  children___parent___parent___id = 'children.parent.parent.id',
  children___parent___parent___children = 'children.parent.parent.children',
  children___parent___children = 'children.parent.children',
  children___parent___children___id = 'children.parent.children.id',
  children___parent___children___children = 'children.parent.children.children',
  children___parent___internal___content = 'children.parent.internal.content',
  children___parent___internal___contentDigest = 'children.parent.internal.contentDigest',
  children___parent___internal___description = 'children.parent.internal.description',
  children___parent___internal___fieldOwners = 'children.parent.internal.fieldOwners',
  children___parent___internal___ignoreType = 'children.parent.internal.ignoreType',
  children___parent___internal___mediaType = 'children.parent.internal.mediaType',
  children___parent___internal___owner = 'children.parent.internal.owner',
  children___parent___internal___type = 'children.parent.internal.type',
  children___children = 'children.children',
  children___children___id = 'children.children.id',
  children___children___parent___id = 'children.children.parent.id',
  children___children___parent___children = 'children.children.parent.children',
  children___children___children = 'children.children.children',
  children___children___children___id = 'children.children.children.id',
  children___children___children___children = 'children.children.children.children',
  children___children___internal___content = 'children.children.internal.content',
  children___children___internal___contentDigest = 'children.children.internal.contentDigest',
  children___children___internal___description = 'children.children.internal.description',
  children___children___internal___fieldOwners = 'children.children.internal.fieldOwners',
  children___children___internal___ignoreType = 'children.children.internal.ignoreType',
  children___children___internal___mediaType = 'children.children.internal.mediaType',
  children___children___internal___owner = 'children.children.internal.owner',
  children___children___internal___type = 'children.children.internal.type',
  children___internal___content = 'children.internal.content',
  children___internal___contentDigest = 'children.internal.contentDigest',
  children___internal___description = 'children.internal.description',
  children___internal___fieldOwners = 'children.internal.fieldOwners',
  children___internal___ignoreType = 'children.internal.ignoreType',
  children___internal___mediaType = 'children.internal.mediaType',
  children___internal___owner = 'children.internal.owner',
  children___internal___type = 'children.internal.type',
  internal___content = 'internal.content',
  internal___contentDigest = 'internal.contentDigest',
  internal___description = 'internal.description',
  internal___fieldOwners = 'internal.fieldOwners',
  internal___ignoreType = 'internal.ignoreType',
  internal___mediaType = 'internal.mediaType',
  internal___owner = 'internal.owner',
  internal___type = 'internal.type',
  protocol = 'protocol',
  resource = 'resource',
  user = 'user',
  pathname = 'pathname',
  hash = 'hash',
  search = 'search',
  href = 'href',
  token = 'token',
  source = 'source',
  name = 'name',
  owner = 'owner',
  ref = 'ref',
  filepathtype = 'filepathtype',
  filepath = 'filepath',
  organization = 'organization',
  full_name = 'full_name',
  webLink = 'webLink',
  sourceInstanceName = 'sourceInstanceName',
  childrenFile = 'childrenFile',
  childrenFile___sourceInstanceName = 'childrenFile.sourceInstanceName',
  childrenFile___absolutePath = 'childrenFile.absolutePath',
  childrenFile___relativePath = 'childrenFile.relativePath',
  childrenFile___extension = 'childrenFile.extension',
  childrenFile___size = 'childrenFile.size',
  childrenFile___prettySize = 'childrenFile.prettySize',
  childrenFile___modifiedTime = 'childrenFile.modifiedTime',
  childrenFile___accessTime = 'childrenFile.accessTime',
  childrenFile___changeTime = 'childrenFile.changeTime',
  childrenFile___birthTime = 'childrenFile.birthTime',
  childrenFile___root = 'childrenFile.root',
  childrenFile___dir = 'childrenFile.dir',
  childrenFile___base = 'childrenFile.base',
  childrenFile___ext = 'childrenFile.ext',
  childrenFile___name = 'childrenFile.name',
  childrenFile___relativeDirectory = 'childrenFile.relativeDirectory',
  childrenFile___dev = 'childrenFile.dev',
  childrenFile___mode = 'childrenFile.mode',
  childrenFile___nlink = 'childrenFile.nlink',
  childrenFile___uid = 'childrenFile.uid',
  childrenFile___gid = 'childrenFile.gid',
  childrenFile___rdev = 'childrenFile.rdev',
  childrenFile___ino = 'childrenFile.ino',
  childrenFile___atimeMs = 'childrenFile.atimeMs',
  childrenFile___mtimeMs = 'childrenFile.mtimeMs',
  childrenFile___ctimeMs = 'childrenFile.ctimeMs',
  childrenFile___atime = 'childrenFile.atime',
  childrenFile___mtime = 'childrenFile.mtime',
  childrenFile___ctime = 'childrenFile.ctime',
  childrenFile___birthtime = 'childrenFile.birthtime',
  childrenFile___birthtimeMs = 'childrenFile.birthtimeMs',
  childrenFile___blksize = 'childrenFile.blksize',
  childrenFile___blocks = 'childrenFile.blocks',
  childrenFile___gitRemote___id = 'childrenFile.gitRemote.id',
  childrenFile___gitRemote___parent___id = 'childrenFile.gitRemote.parent.id',
  childrenFile___gitRemote___parent___children = 'childrenFile.gitRemote.parent.children',
  childrenFile___gitRemote___children = 'childrenFile.gitRemote.children',
  childrenFile___gitRemote___children___id = 'childrenFile.gitRemote.children.id',
  childrenFile___gitRemote___children___children = 'childrenFile.gitRemote.children.children',
  childrenFile___gitRemote___internal___content = 'childrenFile.gitRemote.internal.content',
  childrenFile___gitRemote___internal___contentDigest = 'childrenFile.gitRemote.internal.contentDigest',
  childrenFile___gitRemote___internal___description = 'childrenFile.gitRemote.internal.description',
  childrenFile___gitRemote___internal___fieldOwners = 'childrenFile.gitRemote.internal.fieldOwners',
  childrenFile___gitRemote___internal___ignoreType = 'childrenFile.gitRemote.internal.ignoreType',
  childrenFile___gitRemote___internal___mediaType = 'childrenFile.gitRemote.internal.mediaType',
  childrenFile___gitRemote___internal___owner = 'childrenFile.gitRemote.internal.owner',
  childrenFile___gitRemote___internal___type = 'childrenFile.gitRemote.internal.type',
  childrenFile___gitRemote___protocol = 'childrenFile.gitRemote.protocol',
  childrenFile___gitRemote___resource = 'childrenFile.gitRemote.resource',
  childrenFile___gitRemote___user = 'childrenFile.gitRemote.user',
  childrenFile___gitRemote___pathname = 'childrenFile.gitRemote.pathname',
  childrenFile___gitRemote___hash = 'childrenFile.gitRemote.hash',
  childrenFile___gitRemote___search = 'childrenFile.gitRemote.search',
  childrenFile___gitRemote___href = 'childrenFile.gitRemote.href',
  childrenFile___gitRemote___token = 'childrenFile.gitRemote.token',
  childrenFile___gitRemote___source = 'childrenFile.gitRemote.source',
  childrenFile___gitRemote___name = 'childrenFile.gitRemote.name',
  childrenFile___gitRemote___owner = 'childrenFile.gitRemote.owner',
  childrenFile___gitRemote___ref = 'childrenFile.gitRemote.ref',
  childrenFile___gitRemote___filepathtype = 'childrenFile.gitRemote.filepathtype',
  childrenFile___gitRemote___filepath = 'childrenFile.gitRemote.filepath',
  childrenFile___gitRemote___organization = 'childrenFile.gitRemote.organization',
  childrenFile___gitRemote___full_name = 'childrenFile.gitRemote.full_name',
  childrenFile___gitRemote___webLink = 'childrenFile.gitRemote.webLink',
  childrenFile___gitRemote___sourceInstanceName = 'childrenFile.gitRemote.sourceInstanceName',
  childrenFile___gitRemote___childrenFile = 'childrenFile.gitRemote.childrenFile',
  childrenFile___gitRemote___childrenFile___sourceInstanceName = 'childrenFile.gitRemote.childrenFile.sourceInstanceName',
  childrenFile___gitRemote___childrenFile___absolutePath = 'childrenFile.gitRemote.childrenFile.absolutePath',
  childrenFile___gitRemote___childrenFile___relativePath = 'childrenFile.gitRemote.childrenFile.relativePath',
  childrenFile___gitRemote___childrenFile___extension = 'childrenFile.gitRemote.childrenFile.extension',
  childrenFile___gitRemote___childrenFile___size = 'childrenFile.gitRemote.childrenFile.size',
  childrenFile___gitRemote___childrenFile___prettySize = 'childrenFile.gitRemote.childrenFile.prettySize',
  childrenFile___gitRemote___childrenFile___modifiedTime = 'childrenFile.gitRemote.childrenFile.modifiedTime',
  childrenFile___gitRemote___childrenFile___accessTime = 'childrenFile.gitRemote.childrenFile.accessTime',
  childrenFile___gitRemote___childrenFile___changeTime = 'childrenFile.gitRemote.childrenFile.changeTime',
  childrenFile___gitRemote___childrenFile___birthTime = 'childrenFile.gitRemote.childrenFile.birthTime',
  childrenFile___gitRemote___childrenFile___root = 'childrenFile.gitRemote.childrenFile.root',
  childrenFile___gitRemote___childrenFile___dir = 'childrenFile.gitRemote.childrenFile.dir',
  childrenFile___gitRemote___childrenFile___base = 'childrenFile.gitRemote.childrenFile.base',
  childrenFile___gitRemote___childrenFile___ext = 'childrenFile.gitRemote.childrenFile.ext',
  childrenFile___gitRemote___childrenFile___name = 'childrenFile.gitRemote.childrenFile.name',
  childrenFile___gitRemote___childrenFile___relativeDirectory = 'childrenFile.gitRemote.childrenFile.relativeDirectory',
  childrenFile___gitRemote___childrenFile___dev = 'childrenFile.gitRemote.childrenFile.dev',
  childrenFile___gitRemote___childrenFile___mode = 'childrenFile.gitRemote.childrenFile.mode',
  childrenFile___gitRemote___childrenFile___nlink = 'childrenFile.gitRemote.childrenFile.nlink',
  childrenFile___gitRemote___childrenFile___uid = 'childrenFile.gitRemote.childrenFile.uid',
  childrenFile___gitRemote___childrenFile___gid = 'childrenFile.gitRemote.childrenFile.gid',
  childrenFile___gitRemote___childrenFile___rdev = 'childrenFile.gitRemote.childrenFile.rdev',
  childrenFile___gitRemote___childrenFile___ino = 'childrenFile.gitRemote.childrenFile.ino',
  childrenFile___gitRemote___childrenFile___atimeMs = 'childrenFile.gitRemote.childrenFile.atimeMs',
  childrenFile___gitRemote___childrenFile___mtimeMs = 'childrenFile.gitRemote.childrenFile.mtimeMs',
  childrenFile___gitRemote___childrenFile___ctimeMs = 'childrenFile.gitRemote.childrenFile.ctimeMs',
  childrenFile___gitRemote___childrenFile___atime = 'childrenFile.gitRemote.childrenFile.atime',
  childrenFile___gitRemote___childrenFile___mtime = 'childrenFile.gitRemote.childrenFile.mtime',
  childrenFile___gitRemote___childrenFile___ctime = 'childrenFile.gitRemote.childrenFile.ctime',
  childrenFile___gitRemote___childrenFile___birthtime = 'childrenFile.gitRemote.childrenFile.birthtime',
  childrenFile___gitRemote___childrenFile___birthtimeMs = 'childrenFile.gitRemote.childrenFile.birthtimeMs',
  childrenFile___gitRemote___childrenFile___blksize = 'childrenFile.gitRemote.childrenFile.blksize',
  childrenFile___gitRemote___childrenFile___blocks = 'childrenFile.gitRemote.childrenFile.blocks',
  childrenFile___gitRemote___childrenFile___publicURL = 'childrenFile.gitRemote.childrenFile.publicURL',
  childrenFile___gitRemote___childrenFile___id = 'childrenFile.gitRemote.childrenFile.id',
  childrenFile___gitRemote___childrenFile___children = 'childrenFile.gitRemote.childrenFile.children',
  childrenFile___publicURL = 'childrenFile.publicURL',
  childrenFile___childImageSharp___fixed___base64 = 'childrenFile.childImageSharp.fixed.base64',
  childrenFile___childImageSharp___fixed___tracedSVG = 'childrenFile.childImageSharp.fixed.tracedSVG',
  childrenFile___childImageSharp___fixed___aspectRatio = 'childrenFile.childImageSharp.fixed.aspectRatio',
  childrenFile___childImageSharp___fixed___width = 'childrenFile.childImageSharp.fixed.width',
  childrenFile___childImageSharp___fixed___height = 'childrenFile.childImageSharp.fixed.height',
  childrenFile___childImageSharp___fixed___src = 'childrenFile.childImageSharp.fixed.src',
  childrenFile___childImageSharp___fixed___srcSet = 'childrenFile.childImageSharp.fixed.srcSet',
  childrenFile___childImageSharp___fixed___srcWebp = 'childrenFile.childImageSharp.fixed.srcWebp',
  childrenFile___childImageSharp___fixed___srcSetWebp = 'childrenFile.childImageSharp.fixed.srcSetWebp',
  childrenFile___childImageSharp___fixed___originalName = 'childrenFile.childImageSharp.fixed.originalName',
  childrenFile___childImageSharp___resolutions___base64 = 'childrenFile.childImageSharp.resolutions.base64',
  childrenFile___childImageSharp___resolutions___tracedSVG = 'childrenFile.childImageSharp.resolutions.tracedSVG',
  childrenFile___childImageSharp___resolutions___aspectRatio = 'childrenFile.childImageSharp.resolutions.aspectRatio',
  childrenFile___childImageSharp___resolutions___width = 'childrenFile.childImageSharp.resolutions.width',
  childrenFile___childImageSharp___resolutions___height = 'childrenFile.childImageSharp.resolutions.height',
  childrenFile___childImageSharp___resolutions___src = 'childrenFile.childImageSharp.resolutions.src',
  childrenFile___childImageSharp___resolutions___srcSet = 'childrenFile.childImageSharp.resolutions.srcSet',
  childrenFile___childImageSharp___resolutions___srcWebp = 'childrenFile.childImageSharp.resolutions.srcWebp',
  childrenFile___childImageSharp___resolutions___srcSetWebp = 'childrenFile.childImageSharp.resolutions.srcSetWebp',
  childrenFile___childImageSharp___resolutions___originalName = 'childrenFile.childImageSharp.resolutions.originalName',
  childrenFile___childImageSharp___fluid___base64 = 'childrenFile.childImageSharp.fluid.base64',
  childrenFile___childImageSharp___fluid___tracedSVG = 'childrenFile.childImageSharp.fluid.tracedSVG',
  childrenFile___childImageSharp___fluid___aspectRatio = 'childrenFile.childImageSharp.fluid.aspectRatio',
  childrenFile___childImageSharp___fluid___src = 'childrenFile.childImageSharp.fluid.src',
  childrenFile___childImageSharp___fluid___srcSet = 'childrenFile.childImageSharp.fluid.srcSet',
  childrenFile___childImageSharp___fluid___srcWebp = 'childrenFile.childImageSharp.fluid.srcWebp',
  childrenFile___childImageSharp___fluid___srcSetWebp = 'childrenFile.childImageSharp.fluid.srcSetWebp',
  childrenFile___childImageSharp___fluid___sizes = 'childrenFile.childImageSharp.fluid.sizes',
  childrenFile___childImageSharp___fluid___originalImg = 'childrenFile.childImageSharp.fluid.originalImg',
  childrenFile___childImageSharp___fluid___originalName = 'childrenFile.childImageSharp.fluid.originalName',
  childrenFile___childImageSharp___fluid___presentationWidth = 'childrenFile.childImageSharp.fluid.presentationWidth',
  childrenFile___childImageSharp___fluid___presentationHeight = 'childrenFile.childImageSharp.fluid.presentationHeight',
  childrenFile___childImageSharp___sizes___base64 = 'childrenFile.childImageSharp.sizes.base64',
  childrenFile___childImageSharp___sizes___tracedSVG = 'childrenFile.childImageSharp.sizes.tracedSVG',
  childrenFile___childImageSharp___sizes___aspectRatio = 'childrenFile.childImageSharp.sizes.aspectRatio',
  childrenFile___childImageSharp___sizes___src = 'childrenFile.childImageSharp.sizes.src',
  childrenFile___childImageSharp___sizes___srcSet = 'childrenFile.childImageSharp.sizes.srcSet',
  childrenFile___childImageSharp___sizes___srcWebp = 'childrenFile.childImageSharp.sizes.srcWebp',
  childrenFile___childImageSharp___sizes___srcSetWebp = 'childrenFile.childImageSharp.sizes.srcSetWebp',
  childrenFile___childImageSharp___sizes___sizes = 'childrenFile.childImageSharp.sizes.sizes',
  childrenFile___childImageSharp___sizes___originalImg = 'childrenFile.childImageSharp.sizes.originalImg',
  childrenFile___childImageSharp___sizes___originalName = 'childrenFile.childImageSharp.sizes.originalName',
  childrenFile___childImageSharp___sizes___presentationWidth = 'childrenFile.childImageSharp.sizes.presentationWidth',
  childrenFile___childImageSharp___sizes___presentationHeight = 'childrenFile.childImageSharp.sizes.presentationHeight',
  childrenFile___childImageSharp___original___width = 'childrenFile.childImageSharp.original.width',
  childrenFile___childImageSharp___original___height = 'childrenFile.childImageSharp.original.height',
  childrenFile___childImageSharp___original___src = 'childrenFile.childImageSharp.original.src',
  childrenFile___childImageSharp___resize___src = 'childrenFile.childImageSharp.resize.src',
  childrenFile___childImageSharp___resize___tracedSVG = 'childrenFile.childImageSharp.resize.tracedSVG',
  childrenFile___childImageSharp___resize___width = 'childrenFile.childImageSharp.resize.width',
  childrenFile___childImageSharp___resize___height = 'childrenFile.childImageSharp.resize.height',
  childrenFile___childImageSharp___resize___aspectRatio = 'childrenFile.childImageSharp.resize.aspectRatio',
  childrenFile___childImageSharp___resize___originalName = 'childrenFile.childImageSharp.resize.originalName',
  childrenFile___childImageSharp___id = 'childrenFile.childImageSharp.id',
  childrenFile___childImageSharp___parent___id = 'childrenFile.childImageSharp.parent.id',
  childrenFile___childImageSharp___parent___children = 'childrenFile.childImageSharp.parent.children',
  childrenFile___childImageSharp___children = 'childrenFile.childImageSharp.children',
  childrenFile___childImageSharp___children___id = 'childrenFile.childImageSharp.children.id',
  childrenFile___childImageSharp___children___children = 'childrenFile.childImageSharp.children.children',
  childrenFile___childImageSharp___internal___content = 'childrenFile.childImageSharp.internal.content',
  childrenFile___childImageSharp___internal___contentDigest = 'childrenFile.childImageSharp.internal.contentDigest',
  childrenFile___childImageSharp___internal___description = 'childrenFile.childImageSharp.internal.description',
  childrenFile___childImageSharp___internal___fieldOwners = 'childrenFile.childImageSharp.internal.fieldOwners',
  childrenFile___childImageSharp___internal___ignoreType = 'childrenFile.childImageSharp.internal.ignoreType',
  childrenFile___childImageSharp___internal___mediaType = 'childrenFile.childImageSharp.internal.mediaType',
  childrenFile___childImageSharp___internal___owner = 'childrenFile.childImageSharp.internal.owner',
  childrenFile___childImageSharp___internal___type = 'childrenFile.childImageSharp.internal.type',
  childrenFile___id = 'childrenFile.id',
  childrenFile___parent___id = 'childrenFile.parent.id',
  childrenFile___parent___parent___id = 'childrenFile.parent.parent.id',
  childrenFile___parent___parent___children = 'childrenFile.parent.parent.children',
  childrenFile___parent___children = 'childrenFile.parent.children',
  childrenFile___parent___children___id = 'childrenFile.parent.children.id',
  childrenFile___parent___children___children = 'childrenFile.parent.children.children',
  childrenFile___parent___internal___content = 'childrenFile.parent.internal.content',
  childrenFile___parent___internal___contentDigest = 'childrenFile.parent.internal.contentDigest',
  childrenFile___parent___internal___description = 'childrenFile.parent.internal.description',
  childrenFile___parent___internal___fieldOwners = 'childrenFile.parent.internal.fieldOwners',
  childrenFile___parent___internal___ignoreType = 'childrenFile.parent.internal.ignoreType',
  childrenFile___parent___internal___mediaType = 'childrenFile.parent.internal.mediaType',
  childrenFile___parent___internal___owner = 'childrenFile.parent.internal.owner',
  childrenFile___parent___internal___type = 'childrenFile.parent.internal.type',
  childrenFile___children = 'childrenFile.children',
  childrenFile___children___id = 'childrenFile.children.id',
  childrenFile___children___parent___id = 'childrenFile.children.parent.id',
  childrenFile___children___parent___children = 'childrenFile.children.parent.children',
  childrenFile___children___children = 'childrenFile.children.children',
  childrenFile___children___children___id = 'childrenFile.children.children.id',
  childrenFile___children___children___children = 'childrenFile.children.children.children',
  childrenFile___children___internal___content = 'childrenFile.children.internal.content',
  childrenFile___children___internal___contentDigest = 'childrenFile.children.internal.contentDigest',
  childrenFile___children___internal___description = 'childrenFile.children.internal.description',
  childrenFile___children___internal___fieldOwners = 'childrenFile.children.internal.fieldOwners',
  childrenFile___children___internal___ignoreType = 'childrenFile.children.internal.ignoreType',
  childrenFile___children___internal___mediaType = 'childrenFile.children.internal.mediaType',
  childrenFile___children___internal___owner = 'childrenFile.children.internal.owner',
  childrenFile___children___internal___type = 'childrenFile.children.internal.type',
  childrenFile___internal___content = 'childrenFile.internal.content',
  childrenFile___internal___contentDigest = 'childrenFile.internal.contentDigest',
  childrenFile___internal___description = 'childrenFile.internal.description',
  childrenFile___internal___fieldOwners = 'childrenFile.internal.fieldOwners',
  childrenFile___internal___ignoreType = 'childrenFile.internal.ignoreType',
  childrenFile___internal___mediaType = 'childrenFile.internal.mediaType',
  childrenFile___internal___owner = 'childrenFile.internal.owner',
  childrenFile___internal___type = 'childrenFile.internal.type',
  childrenFile___childMdx___frontmatter___title = 'childrenFile.childMdx.frontmatter.title',
  childrenFile___childMdx___frontmatter___subheader = 'childrenFile.childMdx.frontmatter.subheader',
  childrenFile___childMdx___frontmatter___order = 'childrenFile.childMdx.frontmatter.order',
  childrenFile___childMdx___frontmatter___embeddedImages = 'childrenFile.childMdx.frontmatter.embeddedImages',
  childrenFile___childMdx___rawBody = 'childrenFile.childMdx.rawBody',
  childrenFile___childMdx___fileAbsolutePath = 'childrenFile.childMdx.fileAbsolutePath',
  childrenFile___childMdx___slug = 'childrenFile.childMdx.slug',
  childrenFile___childMdx___body = 'childrenFile.childMdx.body',
  childrenFile___childMdx___excerpt = 'childrenFile.childMdx.excerpt',
  childrenFile___childMdx___headings = 'childrenFile.childMdx.headings',
  childrenFile___childMdx___headings___value = 'childrenFile.childMdx.headings.value',
  childrenFile___childMdx___headings___depth = 'childrenFile.childMdx.headings.depth',
  childrenFile___childMdx___html = 'childrenFile.childMdx.html',
  childrenFile___childMdx___mdxAST = 'childrenFile.childMdx.mdxAST',
  childrenFile___childMdx___tableOfContents = 'childrenFile.childMdx.tableOfContents',
  childrenFile___childMdx___timeToRead = 'childrenFile.childMdx.timeToRead',
  childrenFile___childMdx___wordCount___paragraphs = 'childrenFile.childMdx.wordCount.paragraphs',
  childrenFile___childMdx___wordCount___sentences = 'childrenFile.childMdx.wordCount.sentences',
  childrenFile___childMdx___wordCount___words = 'childrenFile.childMdx.wordCount.words',
  childrenFile___childMdx___fields___slug = 'childrenFile.childMdx.fields.slug',
  childrenFile___childMdx___fields___sourceInstanceName = 'childrenFile.childMdx.fields.sourceInstanceName',
  childrenFile___childMdx___fields___title = 'childrenFile.childMdx.fields.title',
  childrenFile___childMdx___id = 'childrenFile.childMdx.id',
  childrenFile___childMdx___parent___id = 'childrenFile.childMdx.parent.id',
  childrenFile___childMdx___parent___children = 'childrenFile.childMdx.parent.children',
  childrenFile___childMdx___children = 'childrenFile.childMdx.children',
  childrenFile___childMdx___children___id = 'childrenFile.childMdx.children.id',
  childrenFile___childMdx___children___children = 'childrenFile.childMdx.children.children',
  childrenFile___childMdx___internal___content = 'childrenFile.childMdx.internal.content',
  childrenFile___childMdx___internal___contentDigest = 'childrenFile.childMdx.internal.contentDigest',
  childrenFile___childMdx___internal___description = 'childrenFile.childMdx.internal.description',
  childrenFile___childMdx___internal___fieldOwners = 'childrenFile.childMdx.internal.fieldOwners',
  childrenFile___childMdx___internal___ignoreType = 'childrenFile.childMdx.internal.ignoreType',
  childrenFile___childMdx___internal___mediaType = 'childrenFile.childMdx.internal.mediaType',
  childrenFile___childMdx___internal___owner = 'childrenFile.childMdx.internal.owner',
  childrenFile___childMdx___internal___type = 'childrenFile.childMdx.internal.type',
  childrenFile___childDriverMtPhGitJson___id = 'childrenFile.childDriverMtPhGitJson.id',
  childrenFile___childDriverMtPhGitJson___parent___id = 'childrenFile.childDriverMtPhGitJson.parent.id',
  childrenFile___childDriverMtPhGitJson___parent___children = 'childrenFile.childDriverMtPhGitJson.parent.children',
  childrenFile___childDriverMtPhGitJson___children = 'childrenFile.childDriverMtPhGitJson.children',
  childrenFile___childDriverMtPhGitJson___children___id = 'childrenFile.childDriverMtPhGitJson.children.id',
  childrenFile___childDriverMtPhGitJson___children___children = 'childrenFile.childDriverMtPhGitJson.children.children',
  childrenFile___childDriverMtPhGitJson___internal___content = 'childrenFile.childDriverMtPhGitJson.internal.content',
  childrenFile___childDriverMtPhGitJson___internal___contentDigest = 'childrenFile.childDriverMtPhGitJson.internal.contentDigest',
  childrenFile___childDriverMtPhGitJson___internal___description = 'childrenFile.childDriverMtPhGitJson.internal.description',
  childrenFile___childDriverMtPhGitJson___internal___fieldOwners = 'childrenFile.childDriverMtPhGitJson.internal.fieldOwners',
  childrenFile___childDriverMtPhGitJson___internal___ignoreType = 'childrenFile.childDriverMtPhGitJson.internal.ignoreType',
  childrenFile___childDriverMtPhGitJson___internal___mediaType = 'childrenFile.childDriverMtPhGitJson.internal.mediaType',
  childrenFile___childDriverMtPhGitJson___internal___owner = 'childrenFile.childDriverMtPhGitJson.internal.owner',
  childrenFile___childDriverMtPhGitJson___internal___type = 'childrenFile.childDriverMtPhGitJson.internal.type',
  childrenFile___childDriverMtPhGitJson____schema = 'childrenFile.childDriverMtPhGitJson._schema',
  childrenFile___childDriverMtPhGitJson___title = 'childrenFile.childDriverMtPhGitJson.title',
  childrenFile___childDriverMtPhGitJson___type = 'childrenFile.childDriverMtPhGitJson.type',
  childrenFile___childDriverMtPhGitJson___required = 'childrenFile.childDriverMtPhGitJson.required',
  childrenFile___childDriverSicsserialGitJson___id = 'childrenFile.childDriverSicsserialGitJson.id',
  childrenFile___childDriverSicsserialGitJson___parent___id = 'childrenFile.childDriverSicsserialGitJson.parent.id',
  childrenFile___childDriverSicsserialGitJson___parent___children = 'childrenFile.childDriverSicsserialGitJson.parent.children',
  childrenFile___childDriverSicsserialGitJson___children = 'childrenFile.childDriverSicsserialGitJson.children',
  childrenFile___childDriverSicsserialGitJson___children___id = 'childrenFile.childDriverSicsserialGitJson.children.id',
  childrenFile___childDriverSicsserialGitJson___children___children = 'childrenFile.childDriverSicsserialGitJson.children.children',
  childrenFile___childDriverSicsserialGitJson___internal___content = 'childrenFile.childDriverSicsserialGitJson.internal.content',
  childrenFile___childDriverSicsserialGitJson___internal___contentDigest = 'childrenFile.childDriverSicsserialGitJson.internal.contentDigest',
  childrenFile___childDriverSicsserialGitJson___internal___description = 'childrenFile.childDriverSicsserialGitJson.internal.description',
  childrenFile___childDriverSicsserialGitJson___internal___fieldOwners = 'childrenFile.childDriverSicsserialGitJson.internal.fieldOwners',
  childrenFile___childDriverSicsserialGitJson___internal___ignoreType = 'childrenFile.childDriverSicsserialGitJson.internal.ignoreType',
  childrenFile___childDriverSicsserialGitJson___internal___mediaType = 'childrenFile.childDriverSicsserialGitJson.internal.mediaType',
  childrenFile___childDriverSicsserialGitJson___internal___owner = 'childrenFile.childDriverSicsserialGitJson.internal.owner',
  childrenFile___childDriverSicsserialGitJson___internal___type = 'childrenFile.childDriverSicsserialGitJson.internal.type',
  childrenFile___childDriverSicsserialGitJson____schema = 'childrenFile.childDriverSicsserialGitJson._schema',
  childrenFile___childDriverSicsserialGitJson___title = 'childrenFile.childDriverSicsserialGitJson.title',
  childrenFile___childDriverSicsserialGitJson___type = 'childrenFile.childDriverSicsserialGitJson.type',
  childrenFile___childDriverSicsserialGitJson___required = 'childrenFile.childDriverSicsserialGitJson.required'
}

type GitRemoteFilterInput = {
  readonly id: Maybe<StringQueryOperatorInput>;
  readonly parent: Maybe<NodeFilterInput>;
  readonly children: Maybe<NodeFilterListInput>;
  readonly internal: Maybe<InternalFilterInput>;
  readonly protocol: Maybe<StringQueryOperatorInput>;
  readonly resource: Maybe<StringQueryOperatorInput>;
  readonly user: Maybe<StringQueryOperatorInput>;
  readonly pathname: Maybe<StringQueryOperatorInput>;
  readonly hash: Maybe<StringQueryOperatorInput>;
  readonly search: Maybe<StringQueryOperatorInput>;
  readonly href: Maybe<StringQueryOperatorInput>;
  readonly token: Maybe<StringQueryOperatorInput>;
  readonly source: Maybe<StringQueryOperatorInput>;
  readonly name: Maybe<StringQueryOperatorInput>;
  readonly owner: Maybe<StringQueryOperatorInput>;
  readonly ref: Maybe<StringQueryOperatorInput>;
  readonly filepathtype: Maybe<StringQueryOperatorInput>;
  readonly filepath: Maybe<StringQueryOperatorInput>;
  readonly organization: Maybe<StringQueryOperatorInput>;
  readonly full_name: Maybe<StringQueryOperatorInput>;
  readonly webLink: Maybe<StringQueryOperatorInput>;
  readonly sourceInstanceName: Maybe<StringQueryOperatorInput>;
  readonly childrenFile: Maybe<FileFilterListInput>;
};

type GitRemoteGroupConnection = {
  readonly totalCount: Scalars['Int'];
  readonly edges: ReadonlyArray<GitRemoteEdge>;
  readonly nodes: ReadonlyArray<GitRemote>;
  readonly pageInfo: PageInfo;
  readonly field: Scalars['String'];
  readonly fieldValue: Maybe<Scalars['String']>;
};

type GitRemoteSortInput = {
  readonly fields: Maybe<ReadonlyArray<Maybe<GitRemoteFieldsEnum>>>;
  readonly order: Maybe<ReadonlyArray<Maybe<SortOrderEnum>>>;
};

enum HeadingsMdx {
  h1 = 'h1',
  h2 = 'h2',
  h3 = 'h3',
  h4 = 'h4',
  h5 = 'h5',
  h6 = 'h6'
}

enum ImageCropFocus {
  CENTER = 0,
  NORTH = 1,
  NORTHEAST = 5,
  EAST = 2,
  SOUTHEAST = 6,
  SOUTH = 3,
  SOUTHWEST = 7,
  WEST = 4,
  NORTHWEST = 8,
  ENTROPY = 16,
  ATTENTION = 17
}

enum ImageFit {
  COVER = 'cover',
  CONTAIN = 'contain',
  FILL = 'fill',
  INSIDE = 'inside',
  OUTSIDE = 'outside'
}

enum ImageFormat {
  NO_CHANGE = '',
  JPG = 'jpg',
  PNG = 'png',
  WEBP = 'webp'
}

type ImageSharp = Node & {
  readonly fixed: Maybe<ImageSharpFixed>;
  /** @deprecated Resolutions was deprecated in Gatsby v2. It's been renamed to "fixed" https://example.com/write-docs-and-fix-this-example-link */
  readonly resolutions: Maybe<ImageSharpResolutions>;
  readonly fluid: Maybe<ImageSharpFluid>;
  /** @deprecated Sizes was deprecated in Gatsby v2. It's been renamed to "fluid" https://example.com/write-docs-and-fix-this-example-link */
  readonly sizes: Maybe<ImageSharpSizes>;
  readonly original: Maybe<ImageSharpOriginal>;
  readonly resize: Maybe<ImageSharpResize>;
  readonly id: Scalars['ID'];
  readonly parent: Maybe<Node>;
  readonly children: ReadonlyArray<Node>;
  readonly internal: Internal;
};


type ImageSharp_fixedArgs = {
  width: Maybe<Scalars['Int']>;
  height: Maybe<Scalars['Int']>;
  base64Width: Maybe<Scalars['Int']>;
  jpegProgressive?: Maybe<Scalars['Boolean']>;
  pngCompressionSpeed?: Maybe<Scalars['Int']>;
  grayscale?: Maybe<Scalars['Boolean']>;
  duotone: Maybe<DuotoneGradient>;
  traceSVG: Maybe<Potrace>;
  quality: Maybe<Scalars['Int']>;
  jpegQuality: Maybe<Scalars['Int']>;
  pngQuality: Maybe<Scalars['Int']>;
  webpQuality: Maybe<Scalars['Int']>;
  toFormat?: Maybe<ImageFormat>;
  toFormatBase64?: Maybe<ImageFormat>;
  cropFocus?: Maybe<ImageCropFocus>;
  fit?: Maybe<ImageFit>;
  background?: Maybe<Scalars['String']>;
  rotate?: Maybe<Scalars['Int']>;
  trim?: Maybe<Scalars['Float']>;
};


type ImageSharp_resolutionsArgs = {
  width: Maybe<Scalars['Int']>;
  height: Maybe<Scalars['Int']>;
  base64Width: Maybe<Scalars['Int']>;
  jpegProgressive?: Maybe<Scalars['Boolean']>;
  pngCompressionSpeed?: Maybe<Scalars['Int']>;
  grayscale?: Maybe<Scalars['Boolean']>;
  duotone: Maybe<DuotoneGradient>;
  traceSVG: Maybe<Potrace>;
  quality: Maybe<Scalars['Int']>;
  jpegQuality: Maybe<Scalars['Int']>;
  pngQuality: Maybe<Scalars['Int']>;
  webpQuality: Maybe<Scalars['Int']>;
  toFormat?: Maybe<ImageFormat>;
  toFormatBase64?: Maybe<ImageFormat>;
  cropFocus?: Maybe<ImageCropFocus>;
  fit?: Maybe<ImageFit>;
  background?: Maybe<Scalars['String']>;
  rotate?: Maybe<Scalars['Int']>;
  trim?: Maybe<Scalars['Float']>;
};


type ImageSharp_fluidArgs = {
  maxWidth: Maybe<Scalars['Int']>;
  maxHeight: Maybe<Scalars['Int']>;
  base64Width: Maybe<Scalars['Int']>;
  grayscale?: Maybe<Scalars['Boolean']>;
  jpegProgressive?: Maybe<Scalars['Boolean']>;
  pngCompressionSpeed?: Maybe<Scalars['Int']>;
  duotone: Maybe<DuotoneGradient>;
  traceSVG: Maybe<Potrace>;
  quality: Maybe<Scalars['Int']>;
  jpegQuality: Maybe<Scalars['Int']>;
  pngQuality: Maybe<Scalars['Int']>;
  webpQuality: Maybe<Scalars['Int']>;
  toFormat?: Maybe<ImageFormat>;
  toFormatBase64?: Maybe<ImageFormat>;
  cropFocus?: Maybe<ImageCropFocus>;
  fit?: Maybe<ImageFit>;
  background?: Maybe<Scalars['String']>;
  rotate?: Maybe<Scalars['Int']>;
  trim?: Maybe<Scalars['Float']>;
  sizes?: Maybe<Scalars['String']>;
  srcSetBreakpoints?: Maybe<ReadonlyArray<Maybe<Scalars['Int']>>>;
};


type ImageSharp_sizesArgs = {
  maxWidth: Maybe<Scalars['Int']>;
  maxHeight: Maybe<Scalars['Int']>;
  base64Width: Maybe<Scalars['Int']>;
  grayscale?: Maybe<Scalars['Boolean']>;
  jpegProgressive?: Maybe<Scalars['Boolean']>;
  pngCompressionSpeed?: Maybe<Scalars['Int']>;
  duotone: Maybe<DuotoneGradient>;
  traceSVG: Maybe<Potrace>;
  quality: Maybe<Scalars['Int']>;
  jpegQuality: Maybe<Scalars['Int']>;
  pngQuality: Maybe<Scalars['Int']>;
  webpQuality: Maybe<Scalars['Int']>;
  toFormat?: Maybe<ImageFormat>;
  toFormatBase64?: Maybe<ImageFormat>;
  cropFocus?: Maybe<ImageCropFocus>;
  fit?: Maybe<ImageFit>;
  background?: Maybe<Scalars['String']>;
  rotate?: Maybe<Scalars['Int']>;
  trim?: Maybe<Scalars['Float']>;
  sizes?: Maybe<Scalars['String']>;
  srcSetBreakpoints?: Maybe<ReadonlyArray<Maybe<Scalars['Int']>>>;
};


type ImageSharp_resizeArgs = {
  width: Maybe<Scalars['Int']>;
  height: Maybe<Scalars['Int']>;
  quality: Maybe<Scalars['Int']>;
  jpegQuality: Maybe<Scalars['Int']>;
  pngQuality: Maybe<Scalars['Int']>;
  webpQuality: Maybe<Scalars['Int']>;
  jpegProgressive?: Maybe<Scalars['Boolean']>;
  pngCompressionLevel?: Maybe<Scalars['Int']>;
  pngCompressionSpeed?: Maybe<Scalars['Int']>;
  grayscale?: Maybe<Scalars['Boolean']>;
  duotone: Maybe<DuotoneGradient>;
  base64?: Maybe<Scalars['Boolean']>;
  traceSVG: Maybe<Potrace>;
  toFormat?: Maybe<ImageFormat>;
  cropFocus?: Maybe<ImageCropFocus>;
  fit?: Maybe<ImageFit>;
  background?: Maybe<Scalars['String']>;
  rotate?: Maybe<Scalars['Int']>;
  trim?: Maybe<Scalars['Float']>;
};

type ImageSharpConnection = {
  readonly totalCount: Scalars['Int'];
  readonly edges: ReadonlyArray<ImageSharpEdge>;
  readonly nodes: ReadonlyArray<ImageSharp>;
  readonly pageInfo: PageInfo;
  readonly distinct: ReadonlyArray<Scalars['String']>;
  readonly group: ReadonlyArray<ImageSharpGroupConnection>;
};


type ImageSharpConnection_distinctArgs = {
  field: ImageSharpFieldsEnum;
};


type ImageSharpConnection_groupArgs = {
  skip: Maybe<Scalars['Int']>;
  limit: Maybe<Scalars['Int']>;
  field: ImageSharpFieldsEnum;
};

type ImageSharpEdge = {
  readonly next: Maybe<ImageSharp>;
  readonly node: ImageSharp;
  readonly previous: Maybe<ImageSharp>;
};

enum ImageSharpFieldsEnum {
  fixed___base64 = 'fixed.base64',
  fixed___tracedSVG = 'fixed.tracedSVG',
  fixed___aspectRatio = 'fixed.aspectRatio',
  fixed___width = 'fixed.width',
  fixed___height = 'fixed.height',
  fixed___src = 'fixed.src',
  fixed___srcSet = 'fixed.srcSet',
  fixed___srcWebp = 'fixed.srcWebp',
  fixed___srcSetWebp = 'fixed.srcSetWebp',
  fixed___originalName = 'fixed.originalName',
  resolutions___base64 = 'resolutions.base64',
  resolutions___tracedSVG = 'resolutions.tracedSVG',
  resolutions___aspectRatio = 'resolutions.aspectRatio',
  resolutions___width = 'resolutions.width',
  resolutions___height = 'resolutions.height',
  resolutions___src = 'resolutions.src',
  resolutions___srcSet = 'resolutions.srcSet',
  resolutions___srcWebp = 'resolutions.srcWebp',
  resolutions___srcSetWebp = 'resolutions.srcSetWebp',
  resolutions___originalName = 'resolutions.originalName',
  fluid___base64 = 'fluid.base64',
  fluid___tracedSVG = 'fluid.tracedSVG',
  fluid___aspectRatio = 'fluid.aspectRatio',
  fluid___src = 'fluid.src',
  fluid___srcSet = 'fluid.srcSet',
  fluid___srcWebp = 'fluid.srcWebp',
  fluid___srcSetWebp = 'fluid.srcSetWebp',
  fluid___sizes = 'fluid.sizes',
  fluid___originalImg = 'fluid.originalImg',
  fluid___originalName = 'fluid.originalName',
  fluid___presentationWidth = 'fluid.presentationWidth',
  fluid___presentationHeight = 'fluid.presentationHeight',
  sizes___base64 = 'sizes.base64',
  sizes___tracedSVG = 'sizes.tracedSVG',
  sizes___aspectRatio = 'sizes.aspectRatio',
  sizes___src = 'sizes.src',
  sizes___srcSet = 'sizes.srcSet',
  sizes___srcWebp = 'sizes.srcWebp',
  sizes___srcSetWebp = 'sizes.srcSetWebp',
  sizes___sizes = 'sizes.sizes',
  sizes___originalImg = 'sizes.originalImg',
  sizes___originalName = 'sizes.originalName',
  sizes___presentationWidth = 'sizes.presentationWidth',
  sizes___presentationHeight = 'sizes.presentationHeight',
  original___width = 'original.width',
  original___height = 'original.height',
  original___src = 'original.src',
  resize___src = 'resize.src',
  resize___tracedSVG = 'resize.tracedSVG',
  resize___width = 'resize.width',
  resize___height = 'resize.height',
  resize___aspectRatio = 'resize.aspectRatio',
  resize___originalName = 'resize.originalName',
  id = 'id',
  parent___id = 'parent.id',
  parent___parent___id = 'parent.parent.id',
  parent___parent___parent___id = 'parent.parent.parent.id',
  parent___parent___parent___children = 'parent.parent.parent.children',
  parent___parent___children = 'parent.parent.children',
  parent___parent___children___id = 'parent.parent.children.id',
  parent___parent___children___children = 'parent.parent.children.children',
  parent___parent___internal___content = 'parent.parent.internal.content',
  parent___parent___internal___contentDigest = 'parent.parent.internal.contentDigest',
  parent___parent___internal___description = 'parent.parent.internal.description',
  parent___parent___internal___fieldOwners = 'parent.parent.internal.fieldOwners',
  parent___parent___internal___ignoreType = 'parent.parent.internal.ignoreType',
  parent___parent___internal___mediaType = 'parent.parent.internal.mediaType',
  parent___parent___internal___owner = 'parent.parent.internal.owner',
  parent___parent___internal___type = 'parent.parent.internal.type',
  parent___children = 'parent.children',
  parent___children___id = 'parent.children.id',
  parent___children___parent___id = 'parent.children.parent.id',
  parent___children___parent___children = 'parent.children.parent.children',
  parent___children___children = 'parent.children.children',
  parent___children___children___id = 'parent.children.children.id',
  parent___children___children___children = 'parent.children.children.children',
  parent___children___internal___content = 'parent.children.internal.content',
  parent___children___internal___contentDigest = 'parent.children.internal.contentDigest',
  parent___children___internal___description = 'parent.children.internal.description',
  parent___children___internal___fieldOwners = 'parent.children.internal.fieldOwners',
  parent___children___internal___ignoreType = 'parent.children.internal.ignoreType',
  parent___children___internal___mediaType = 'parent.children.internal.mediaType',
  parent___children___internal___owner = 'parent.children.internal.owner',
  parent___children___internal___type = 'parent.children.internal.type',
  parent___internal___content = 'parent.internal.content',
  parent___internal___contentDigest = 'parent.internal.contentDigest',
  parent___internal___description = 'parent.internal.description',
  parent___internal___fieldOwners = 'parent.internal.fieldOwners',
  parent___internal___ignoreType = 'parent.internal.ignoreType',
  parent___internal___mediaType = 'parent.internal.mediaType',
  parent___internal___owner = 'parent.internal.owner',
  parent___internal___type = 'parent.internal.type',
  children = 'children',
  children___id = 'children.id',
  children___parent___id = 'children.parent.id',
  children___parent___parent___id = 'children.parent.parent.id',
  children___parent___parent___children = 'children.parent.parent.children',
  children___parent___children = 'children.parent.children',
  children___parent___children___id = 'children.parent.children.id',
  children___parent___children___children = 'children.parent.children.children',
  children___parent___internal___content = 'children.parent.internal.content',
  children___parent___internal___contentDigest = 'children.parent.internal.contentDigest',
  children___parent___internal___description = 'children.parent.internal.description',
  children___parent___internal___fieldOwners = 'children.parent.internal.fieldOwners',
  children___parent___internal___ignoreType = 'children.parent.internal.ignoreType',
  children___parent___internal___mediaType = 'children.parent.internal.mediaType',
  children___parent___internal___owner = 'children.parent.internal.owner',
  children___parent___internal___type = 'children.parent.internal.type',
  children___children = 'children.children',
  children___children___id = 'children.children.id',
  children___children___parent___id = 'children.children.parent.id',
  children___children___parent___children = 'children.children.parent.children',
  children___children___children = 'children.children.children',
  children___children___children___id = 'children.children.children.id',
  children___children___children___children = 'children.children.children.children',
  children___children___internal___content = 'children.children.internal.content',
  children___children___internal___contentDigest = 'children.children.internal.contentDigest',
  children___children___internal___description = 'children.children.internal.description',
  children___children___internal___fieldOwners = 'children.children.internal.fieldOwners',
  children___children___internal___ignoreType = 'children.children.internal.ignoreType',
  children___children___internal___mediaType = 'children.children.internal.mediaType',
  children___children___internal___owner = 'children.children.internal.owner',
  children___children___internal___type = 'children.children.internal.type',
  children___internal___content = 'children.internal.content',
  children___internal___contentDigest = 'children.internal.contentDigest',
  children___internal___description = 'children.internal.description',
  children___internal___fieldOwners = 'children.internal.fieldOwners',
  children___internal___ignoreType = 'children.internal.ignoreType',
  children___internal___mediaType = 'children.internal.mediaType',
  children___internal___owner = 'children.internal.owner',
  children___internal___type = 'children.internal.type',
  internal___content = 'internal.content',
  internal___contentDigest = 'internal.contentDigest',
  internal___description = 'internal.description',
  internal___fieldOwners = 'internal.fieldOwners',
  internal___ignoreType = 'internal.ignoreType',
  internal___mediaType = 'internal.mediaType',
  internal___owner = 'internal.owner',
  internal___type = 'internal.type'
}

type ImageSharpFilterInput = {
  readonly fixed: Maybe<ImageSharpFixedFilterInput>;
  readonly resolutions: Maybe<ImageSharpResolutionsFilterInput>;
  readonly fluid: Maybe<ImageSharpFluidFilterInput>;
  readonly sizes: Maybe<ImageSharpSizesFilterInput>;
  readonly original: Maybe<ImageSharpOriginalFilterInput>;
  readonly resize: Maybe<ImageSharpResizeFilterInput>;
  readonly id: Maybe<StringQueryOperatorInput>;
  readonly parent: Maybe<NodeFilterInput>;
  readonly children: Maybe<NodeFilterListInput>;
  readonly internal: Maybe<InternalFilterInput>;
};

type ImageSharpFixed = {
  readonly base64: Maybe<Scalars['String']>;
  readonly tracedSVG: Maybe<Scalars['String']>;
  readonly aspectRatio: Maybe<Scalars['Float']>;
  readonly width: Scalars['Float'];
  readonly height: Scalars['Float'];
  readonly src: Scalars['String'];
  readonly srcSet: Scalars['String'];
  readonly srcWebp: Maybe<Scalars['String']>;
  readonly srcSetWebp: Maybe<Scalars['String']>;
  readonly originalName: Maybe<Scalars['String']>;
};

type ImageSharpFixedFilterInput = {
  readonly base64: Maybe<StringQueryOperatorInput>;
  readonly tracedSVG: Maybe<StringQueryOperatorInput>;
  readonly aspectRatio: Maybe<FloatQueryOperatorInput>;
  readonly width: Maybe<FloatQueryOperatorInput>;
  readonly height: Maybe<FloatQueryOperatorInput>;
  readonly src: Maybe<StringQueryOperatorInput>;
  readonly srcSet: Maybe<StringQueryOperatorInput>;
  readonly srcWebp: Maybe<StringQueryOperatorInput>;
  readonly srcSetWebp: Maybe<StringQueryOperatorInput>;
  readonly originalName: Maybe<StringQueryOperatorInput>;
};

type ImageSharpFluid = {
  readonly base64: Maybe<Scalars['String']>;
  readonly tracedSVG: Maybe<Scalars['String']>;
  readonly aspectRatio: Scalars['Float'];
  readonly src: Scalars['String'];
  readonly srcSet: Scalars['String'];
  readonly srcWebp: Maybe<Scalars['String']>;
  readonly srcSetWebp: Maybe<Scalars['String']>;
  readonly sizes: Scalars['String'];
  readonly originalImg: Maybe<Scalars['String']>;
  readonly originalName: Maybe<Scalars['String']>;
  readonly presentationWidth: Scalars['Int'];
  readonly presentationHeight: Scalars['Int'];
};

type ImageSharpFluidFilterInput = {
  readonly base64: Maybe<StringQueryOperatorInput>;
  readonly tracedSVG: Maybe<StringQueryOperatorInput>;
  readonly aspectRatio: Maybe<FloatQueryOperatorInput>;
  readonly src: Maybe<StringQueryOperatorInput>;
  readonly srcSet: Maybe<StringQueryOperatorInput>;
  readonly srcWebp: Maybe<StringQueryOperatorInput>;
  readonly srcSetWebp: Maybe<StringQueryOperatorInput>;
  readonly sizes: Maybe<StringQueryOperatorInput>;
  readonly originalImg: Maybe<StringQueryOperatorInput>;
  readonly originalName: Maybe<StringQueryOperatorInput>;
  readonly presentationWidth: Maybe<IntQueryOperatorInput>;
  readonly presentationHeight: Maybe<IntQueryOperatorInput>;
};

type ImageSharpGroupConnection = {
  readonly totalCount: Scalars['Int'];
  readonly edges: ReadonlyArray<ImageSharpEdge>;
  readonly nodes: ReadonlyArray<ImageSharp>;
  readonly pageInfo: PageInfo;
  readonly field: Scalars['String'];
  readonly fieldValue: Maybe<Scalars['String']>;
};

type ImageSharpOriginal = {
  readonly width: Maybe<Scalars['Float']>;
  readonly height: Maybe<Scalars['Float']>;
  readonly src: Maybe<Scalars['String']>;
};

type ImageSharpOriginalFilterInput = {
  readonly width: Maybe<FloatQueryOperatorInput>;
  readonly height: Maybe<FloatQueryOperatorInput>;
  readonly src: Maybe<StringQueryOperatorInput>;
};

type ImageSharpResize = {
  readonly src: Maybe<Scalars['String']>;
  readonly tracedSVG: Maybe<Scalars['String']>;
  readonly width: Maybe<Scalars['Int']>;
  readonly height: Maybe<Scalars['Int']>;
  readonly aspectRatio: Maybe<Scalars['Float']>;
  readonly originalName: Maybe<Scalars['String']>;
};

type ImageSharpResizeFilterInput = {
  readonly src: Maybe<StringQueryOperatorInput>;
  readonly tracedSVG: Maybe<StringQueryOperatorInput>;
  readonly width: Maybe<IntQueryOperatorInput>;
  readonly height: Maybe<IntQueryOperatorInput>;
  readonly aspectRatio: Maybe<FloatQueryOperatorInput>;
  readonly originalName: Maybe<StringQueryOperatorInput>;
};

type ImageSharpResolutions = {
  readonly base64: Maybe<Scalars['String']>;
  readonly tracedSVG: Maybe<Scalars['String']>;
  readonly aspectRatio: Maybe<Scalars['Float']>;
  readonly width: Scalars['Float'];
  readonly height: Scalars['Float'];
  readonly src: Scalars['String'];
  readonly srcSet: Scalars['String'];
  readonly srcWebp: Maybe<Scalars['String']>;
  readonly srcSetWebp: Maybe<Scalars['String']>;
  readonly originalName: Maybe<Scalars['String']>;
};

type ImageSharpResolutionsFilterInput = {
  readonly base64: Maybe<StringQueryOperatorInput>;
  readonly tracedSVG: Maybe<StringQueryOperatorInput>;
  readonly aspectRatio: Maybe<FloatQueryOperatorInput>;
  readonly width: Maybe<FloatQueryOperatorInput>;
  readonly height: Maybe<FloatQueryOperatorInput>;
  readonly src: Maybe<StringQueryOperatorInput>;
  readonly srcSet: Maybe<StringQueryOperatorInput>;
  readonly srcWebp: Maybe<StringQueryOperatorInput>;
  readonly srcSetWebp: Maybe<StringQueryOperatorInput>;
  readonly originalName: Maybe<StringQueryOperatorInput>;
};

type ImageSharpSizes = {
  readonly base64: Maybe<Scalars['String']>;
  readonly tracedSVG: Maybe<Scalars['String']>;
  readonly aspectRatio: Scalars['Float'];
  readonly src: Scalars['String'];
  readonly srcSet: Scalars['String'];
  readonly srcWebp: Maybe<Scalars['String']>;
  readonly srcSetWebp: Maybe<Scalars['String']>;
  readonly sizes: Scalars['String'];
  readonly originalImg: Maybe<Scalars['String']>;
  readonly originalName: Maybe<Scalars['String']>;
  readonly presentationWidth: Scalars['Int'];
  readonly presentationHeight: Scalars['Int'];
};

type ImageSharpSizesFilterInput = {
  readonly base64: Maybe<StringQueryOperatorInput>;
  readonly tracedSVG: Maybe<StringQueryOperatorInput>;
  readonly aspectRatio: Maybe<FloatQueryOperatorInput>;
  readonly src: Maybe<StringQueryOperatorInput>;
  readonly srcSet: Maybe<StringQueryOperatorInput>;
  readonly srcWebp: Maybe<StringQueryOperatorInput>;
  readonly srcSetWebp: Maybe<StringQueryOperatorInput>;
  readonly sizes: Maybe<StringQueryOperatorInput>;
  readonly originalImg: Maybe<StringQueryOperatorInput>;
  readonly originalName: Maybe<StringQueryOperatorInput>;
  readonly presentationWidth: Maybe<IntQueryOperatorInput>;
  readonly presentationHeight: Maybe<IntQueryOperatorInput>;
};

type ImageSharpSortInput = {
  readonly fields: Maybe<ReadonlyArray<Maybe<ImageSharpFieldsEnum>>>;
  readonly order: Maybe<ReadonlyArray<Maybe<SortOrderEnum>>>;
};

type Internal = {
  readonly content: Maybe<Scalars['String']>;
  readonly contentDigest: Scalars['String'];
  readonly description: Maybe<Scalars['String']>;
  readonly fieldOwners: Maybe<ReadonlyArray<Maybe<Scalars['String']>>>;
  readonly ignoreType: Maybe<Scalars['Boolean']>;
  readonly mediaType: Maybe<Scalars['String']>;
  readonly owner: Scalars['String'];
  readonly type: Scalars['String'];
};

type InternalFilterInput = {
  readonly content: Maybe<StringQueryOperatorInput>;
  readonly contentDigest: Maybe<StringQueryOperatorInput>;
  readonly description: Maybe<StringQueryOperatorInput>;
  readonly fieldOwners: Maybe<StringQueryOperatorInput>;
  readonly ignoreType: Maybe<BooleanQueryOperatorInput>;
  readonly mediaType: Maybe<StringQueryOperatorInput>;
  readonly owner: Maybe<StringQueryOperatorInput>;
  readonly type: Maybe<StringQueryOperatorInput>;
};

type IntQueryOperatorInput = {
  readonly eq: Maybe<Scalars['Int']>;
  readonly ne: Maybe<Scalars['Int']>;
  readonly gt: Maybe<Scalars['Int']>;
  readonly gte: Maybe<Scalars['Int']>;
  readonly lt: Maybe<Scalars['Int']>;
  readonly lte: Maybe<Scalars['Int']>;
  readonly in: Maybe<ReadonlyArray<Maybe<Scalars['Int']>>>;
  readonly nin: Maybe<ReadonlyArray<Maybe<Scalars['Int']>>>;
};


type JSONQueryOperatorInput = {
  readonly eq: Maybe<Scalars['JSON']>;
  readonly ne: Maybe<Scalars['JSON']>;
  readonly in: Maybe<ReadonlyArray<Maybe<Scalars['JSON']>>>;
  readonly nin: Maybe<ReadonlyArray<Maybe<Scalars['JSON']>>>;
  readonly regex: Maybe<Scalars['JSON']>;
  readonly glob: Maybe<Scalars['JSON']>;
};

type Mdx = Node & {
  readonly frontmatter: Maybe<MdxFrontmatter>;
  readonly rawBody: Scalars['String'];
  readonly fileAbsolutePath: Scalars['String'];
  readonly slug: Maybe<Scalars['String']>;
  readonly body: Scalars['String'];
  readonly excerpt: Scalars['String'];
  readonly headings: Maybe<ReadonlyArray<Maybe<MdxHeadingMdx>>>;
  readonly html: Maybe<Scalars['String']>;
  readonly mdxAST: Maybe<Scalars['JSON']>;
  readonly tableOfContents: Maybe<Scalars['JSON']>;
  readonly timeToRead: Maybe<Scalars['Int']>;
  readonly wordCount: Maybe<MdxWordCount>;
  readonly fields: Maybe<MdxFields>;
  readonly id: Scalars['ID'];
  readonly parent: Maybe<Node>;
  readonly children: ReadonlyArray<Node>;
  readonly internal: Internal;
};


type Mdx_excerptArgs = {
  pruneLength?: Maybe<Scalars['Int']>;
  truncate?: Maybe<Scalars['Boolean']>;
};


type Mdx_headingsArgs = {
  depth: Maybe<HeadingsMdx>;
};


type Mdx_tableOfContentsArgs = {
  maxDepth: Maybe<Scalars['Int']>;
};

type MdxConnection = {
  readonly totalCount: Scalars['Int'];
  readonly edges: ReadonlyArray<MdxEdge>;
  readonly nodes: ReadonlyArray<Mdx>;
  readonly pageInfo: PageInfo;
  readonly distinct: ReadonlyArray<Scalars['String']>;
  readonly group: ReadonlyArray<MdxGroupConnection>;
};


type MdxConnection_distinctArgs = {
  field: MdxFieldsEnum;
};


type MdxConnection_groupArgs = {
  skip: Maybe<Scalars['Int']>;
  limit: Maybe<Scalars['Int']>;
  field: MdxFieldsEnum;
};

type MdxEdge = {
  readonly next: Maybe<Mdx>;
  readonly node: Mdx;
  readonly previous: Maybe<Mdx>;
};

type MdxFields = {
  readonly slug: Maybe<Scalars['String']>;
  readonly sourceInstanceName: Maybe<Scalars['String']>;
  readonly title: Maybe<Scalars['String']>;
};

enum MdxFieldsEnum {
  frontmatter___title = 'frontmatter.title',
  frontmatter___subheader = 'frontmatter.subheader',
  frontmatter___order = 'frontmatter.order',
  frontmatter___embeddedImages = 'frontmatter.embeddedImages',
  frontmatter___embeddedImages___sourceInstanceName = 'frontmatter.embeddedImages.sourceInstanceName',
  frontmatter___embeddedImages___absolutePath = 'frontmatter.embeddedImages.absolutePath',
  frontmatter___embeddedImages___relativePath = 'frontmatter.embeddedImages.relativePath',
  frontmatter___embeddedImages___extension = 'frontmatter.embeddedImages.extension',
  frontmatter___embeddedImages___size = 'frontmatter.embeddedImages.size',
  frontmatter___embeddedImages___prettySize = 'frontmatter.embeddedImages.prettySize',
  frontmatter___embeddedImages___modifiedTime = 'frontmatter.embeddedImages.modifiedTime',
  frontmatter___embeddedImages___accessTime = 'frontmatter.embeddedImages.accessTime',
  frontmatter___embeddedImages___changeTime = 'frontmatter.embeddedImages.changeTime',
  frontmatter___embeddedImages___birthTime = 'frontmatter.embeddedImages.birthTime',
  frontmatter___embeddedImages___root = 'frontmatter.embeddedImages.root',
  frontmatter___embeddedImages___dir = 'frontmatter.embeddedImages.dir',
  frontmatter___embeddedImages___base = 'frontmatter.embeddedImages.base',
  frontmatter___embeddedImages___ext = 'frontmatter.embeddedImages.ext',
  frontmatter___embeddedImages___name = 'frontmatter.embeddedImages.name',
  frontmatter___embeddedImages___relativeDirectory = 'frontmatter.embeddedImages.relativeDirectory',
  frontmatter___embeddedImages___dev = 'frontmatter.embeddedImages.dev',
  frontmatter___embeddedImages___mode = 'frontmatter.embeddedImages.mode',
  frontmatter___embeddedImages___nlink = 'frontmatter.embeddedImages.nlink',
  frontmatter___embeddedImages___uid = 'frontmatter.embeddedImages.uid',
  frontmatter___embeddedImages___gid = 'frontmatter.embeddedImages.gid',
  frontmatter___embeddedImages___rdev = 'frontmatter.embeddedImages.rdev',
  frontmatter___embeddedImages___ino = 'frontmatter.embeddedImages.ino',
  frontmatter___embeddedImages___atimeMs = 'frontmatter.embeddedImages.atimeMs',
  frontmatter___embeddedImages___mtimeMs = 'frontmatter.embeddedImages.mtimeMs',
  frontmatter___embeddedImages___ctimeMs = 'frontmatter.embeddedImages.ctimeMs',
  frontmatter___embeddedImages___atime = 'frontmatter.embeddedImages.atime',
  frontmatter___embeddedImages___mtime = 'frontmatter.embeddedImages.mtime',
  frontmatter___embeddedImages___ctime = 'frontmatter.embeddedImages.ctime',
  frontmatter___embeddedImages___birthtime = 'frontmatter.embeddedImages.birthtime',
  frontmatter___embeddedImages___birthtimeMs = 'frontmatter.embeddedImages.birthtimeMs',
  frontmatter___embeddedImages___blksize = 'frontmatter.embeddedImages.blksize',
  frontmatter___embeddedImages___blocks = 'frontmatter.embeddedImages.blocks',
  frontmatter___embeddedImages___gitRemote___id = 'frontmatter.embeddedImages.gitRemote.id',
  frontmatter___embeddedImages___gitRemote___children = 'frontmatter.embeddedImages.gitRemote.children',
  frontmatter___embeddedImages___gitRemote___protocol = 'frontmatter.embeddedImages.gitRemote.protocol',
  frontmatter___embeddedImages___gitRemote___resource = 'frontmatter.embeddedImages.gitRemote.resource',
  frontmatter___embeddedImages___gitRemote___user = 'frontmatter.embeddedImages.gitRemote.user',
  frontmatter___embeddedImages___gitRemote___pathname = 'frontmatter.embeddedImages.gitRemote.pathname',
  frontmatter___embeddedImages___gitRemote___hash = 'frontmatter.embeddedImages.gitRemote.hash',
  frontmatter___embeddedImages___gitRemote___search = 'frontmatter.embeddedImages.gitRemote.search',
  frontmatter___embeddedImages___gitRemote___href = 'frontmatter.embeddedImages.gitRemote.href',
  frontmatter___embeddedImages___gitRemote___token = 'frontmatter.embeddedImages.gitRemote.token',
  frontmatter___embeddedImages___gitRemote___source = 'frontmatter.embeddedImages.gitRemote.source',
  frontmatter___embeddedImages___gitRemote___name = 'frontmatter.embeddedImages.gitRemote.name',
  frontmatter___embeddedImages___gitRemote___owner = 'frontmatter.embeddedImages.gitRemote.owner',
  frontmatter___embeddedImages___gitRemote___ref = 'frontmatter.embeddedImages.gitRemote.ref',
  frontmatter___embeddedImages___gitRemote___filepathtype = 'frontmatter.embeddedImages.gitRemote.filepathtype',
  frontmatter___embeddedImages___gitRemote___filepath = 'frontmatter.embeddedImages.gitRemote.filepath',
  frontmatter___embeddedImages___gitRemote___organization = 'frontmatter.embeddedImages.gitRemote.organization',
  frontmatter___embeddedImages___gitRemote___full_name = 'frontmatter.embeddedImages.gitRemote.full_name',
  frontmatter___embeddedImages___gitRemote___webLink = 'frontmatter.embeddedImages.gitRemote.webLink',
  frontmatter___embeddedImages___gitRemote___sourceInstanceName = 'frontmatter.embeddedImages.gitRemote.sourceInstanceName',
  frontmatter___embeddedImages___gitRemote___childrenFile = 'frontmatter.embeddedImages.gitRemote.childrenFile',
  frontmatter___embeddedImages___publicURL = 'frontmatter.embeddedImages.publicURL',
  frontmatter___embeddedImages___childImageSharp___id = 'frontmatter.embeddedImages.childImageSharp.id',
  frontmatter___embeddedImages___childImageSharp___children = 'frontmatter.embeddedImages.childImageSharp.children',
  frontmatter___embeddedImages___id = 'frontmatter.embeddedImages.id',
  frontmatter___embeddedImages___parent___id = 'frontmatter.embeddedImages.parent.id',
  frontmatter___embeddedImages___parent___children = 'frontmatter.embeddedImages.parent.children',
  frontmatter___embeddedImages___children = 'frontmatter.embeddedImages.children',
  frontmatter___embeddedImages___children___id = 'frontmatter.embeddedImages.children.id',
  frontmatter___embeddedImages___children___children = 'frontmatter.embeddedImages.children.children',
  frontmatter___embeddedImages___internal___content = 'frontmatter.embeddedImages.internal.content',
  frontmatter___embeddedImages___internal___contentDigest = 'frontmatter.embeddedImages.internal.contentDigest',
  frontmatter___embeddedImages___internal___description = 'frontmatter.embeddedImages.internal.description',
  frontmatter___embeddedImages___internal___fieldOwners = 'frontmatter.embeddedImages.internal.fieldOwners',
  frontmatter___embeddedImages___internal___ignoreType = 'frontmatter.embeddedImages.internal.ignoreType',
  frontmatter___embeddedImages___internal___mediaType = 'frontmatter.embeddedImages.internal.mediaType',
  frontmatter___embeddedImages___internal___owner = 'frontmatter.embeddedImages.internal.owner',
  frontmatter___embeddedImages___internal___type = 'frontmatter.embeddedImages.internal.type',
  frontmatter___embeddedImages___childMdx___rawBody = 'frontmatter.embeddedImages.childMdx.rawBody',
  frontmatter___embeddedImages___childMdx___fileAbsolutePath = 'frontmatter.embeddedImages.childMdx.fileAbsolutePath',
  frontmatter___embeddedImages___childMdx___slug = 'frontmatter.embeddedImages.childMdx.slug',
  frontmatter___embeddedImages___childMdx___body = 'frontmatter.embeddedImages.childMdx.body',
  frontmatter___embeddedImages___childMdx___excerpt = 'frontmatter.embeddedImages.childMdx.excerpt',
  frontmatter___embeddedImages___childMdx___headings = 'frontmatter.embeddedImages.childMdx.headings',
  frontmatter___embeddedImages___childMdx___html = 'frontmatter.embeddedImages.childMdx.html',
  frontmatter___embeddedImages___childMdx___mdxAST = 'frontmatter.embeddedImages.childMdx.mdxAST',
  frontmatter___embeddedImages___childMdx___tableOfContents = 'frontmatter.embeddedImages.childMdx.tableOfContents',
  frontmatter___embeddedImages___childMdx___timeToRead = 'frontmatter.embeddedImages.childMdx.timeToRead',
  frontmatter___embeddedImages___childMdx___id = 'frontmatter.embeddedImages.childMdx.id',
  frontmatter___embeddedImages___childMdx___children = 'frontmatter.embeddedImages.childMdx.children',
  frontmatter___embeddedImages___childDriverMtPhGitJson___id = 'frontmatter.embeddedImages.childDriverMtPhGitJson.id',
  frontmatter___embeddedImages___childDriverMtPhGitJson___children = 'frontmatter.embeddedImages.childDriverMtPhGitJson.children',
  frontmatter___embeddedImages___childDriverMtPhGitJson____schema = 'frontmatter.embeddedImages.childDriverMtPhGitJson._schema',
  frontmatter___embeddedImages___childDriverMtPhGitJson___title = 'frontmatter.embeddedImages.childDriverMtPhGitJson.title',
  frontmatter___embeddedImages___childDriverMtPhGitJson___type = 'frontmatter.embeddedImages.childDriverMtPhGitJson.type',
  frontmatter___embeddedImages___childDriverMtPhGitJson___required = 'frontmatter.embeddedImages.childDriverMtPhGitJson.required',
  frontmatter___embeddedImages___childDriverSicsserialGitJson___id = 'frontmatter.embeddedImages.childDriverSicsserialGitJson.id',
  frontmatter___embeddedImages___childDriverSicsserialGitJson___children = 'frontmatter.embeddedImages.childDriverSicsserialGitJson.children',
  frontmatter___embeddedImages___childDriverSicsserialGitJson____schema = 'frontmatter.embeddedImages.childDriverSicsserialGitJson._schema',
  frontmatter___embeddedImages___childDriverSicsserialGitJson___title = 'frontmatter.embeddedImages.childDriverSicsserialGitJson.title',
  frontmatter___embeddedImages___childDriverSicsserialGitJson___type = 'frontmatter.embeddedImages.childDriverSicsserialGitJson.type',
  frontmatter___embeddedImages___childDriverSicsserialGitJson___required = 'frontmatter.embeddedImages.childDriverSicsserialGitJson.required',
  rawBody = 'rawBody',
  fileAbsolutePath = 'fileAbsolutePath',
  slug = 'slug',
  body = 'body',
  excerpt = 'excerpt',
  headings = 'headings',
  headings___value = 'headings.value',
  headings___depth = 'headings.depth',
  html = 'html',
  mdxAST = 'mdxAST',
  tableOfContents = 'tableOfContents',
  timeToRead = 'timeToRead',
  wordCount___paragraphs = 'wordCount.paragraphs',
  wordCount___sentences = 'wordCount.sentences',
  wordCount___words = 'wordCount.words',
  fields___slug = 'fields.slug',
  fields___sourceInstanceName = 'fields.sourceInstanceName',
  fields___title = 'fields.title',
  id = 'id',
  parent___id = 'parent.id',
  parent___parent___id = 'parent.parent.id',
  parent___parent___parent___id = 'parent.parent.parent.id',
  parent___parent___parent___children = 'parent.parent.parent.children',
  parent___parent___children = 'parent.parent.children',
  parent___parent___children___id = 'parent.parent.children.id',
  parent___parent___children___children = 'parent.parent.children.children',
  parent___parent___internal___content = 'parent.parent.internal.content',
  parent___parent___internal___contentDigest = 'parent.parent.internal.contentDigest',
  parent___parent___internal___description = 'parent.parent.internal.description',
  parent___parent___internal___fieldOwners = 'parent.parent.internal.fieldOwners',
  parent___parent___internal___ignoreType = 'parent.parent.internal.ignoreType',
  parent___parent___internal___mediaType = 'parent.parent.internal.mediaType',
  parent___parent___internal___owner = 'parent.parent.internal.owner',
  parent___parent___internal___type = 'parent.parent.internal.type',
  parent___children = 'parent.children',
  parent___children___id = 'parent.children.id',
  parent___children___parent___id = 'parent.children.parent.id',
  parent___children___parent___children = 'parent.children.parent.children',
  parent___children___children = 'parent.children.children',
  parent___children___children___id = 'parent.children.children.id',
  parent___children___children___children = 'parent.children.children.children',
  parent___children___internal___content = 'parent.children.internal.content',
  parent___children___internal___contentDigest = 'parent.children.internal.contentDigest',
  parent___children___internal___description = 'parent.children.internal.description',
  parent___children___internal___fieldOwners = 'parent.children.internal.fieldOwners',
  parent___children___internal___ignoreType = 'parent.children.internal.ignoreType',
  parent___children___internal___mediaType = 'parent.children.internal.mediaType',
  parent___children___internal___owner = 'parent.children.internal.owner',
  parent___children___internal___type = 'parent.children.internal.type',
  parent___internal___content = 'parent.internal.content',
  parent___internal___contentDigest = 'parent.internal.contentDigest',
  parent___internal___description = 'parent.internal.description',
  parent___internal___fieldOwners = 'parent.internal.fieldOwners',
  parent___internal___ignoreType = 'parent.internal.ignoreType',
  parent___internal___mediaType = 'parent.internal.mediaType',
  parent___internal___owner = 'parent.internal.owner',
  parent___internal___type = 'parent.internal.type',
  children = 'children',
  children___id = 'children.id',
  children___parent___id = 'children.parent.id',
  children___parent___parent___id = 'children.parent.parent.id',
  children___parent___parent___children = 'children.parent.parent.children',
  children___parent___children = 'children.parent.children',
  children___parent___children___id = 'children.parent.children.id',
  children___parent___children___children = 'children.parent.children.children',
  children___parent___internal___content = 'children.parent.internal.content',
  children___parent___internal___contentDigest = 'children.parent.internal.contentDigest',
  children___parent___internal___description = 'children.parent.internal.description',
  children___parent___internal___fieldOwners = 'children.parent.internal.fieldOwners',
  children___parent___internal___ignoreType = 'children.parent.internal.ignoreType',
  children___parent___internal___mediaType = 'children.parent.internal.mediaType',
  children___parent___internal___owner = 'children.parent.internal.owner',
  children___parent___internal___type = 'children.parent.internal.type',
  children___children = 'children.children',
  children___children___id = 'children.children.id',
  children___children___parent___id = 'children.children.parent.id',
  children___children___parent___children = 'children.children.parent.children',
  children___children___children = 'children.children.children',
  children___children___children___id = 'children.children.children.id',
  children___children___children___children = 'children.children.children.children',
  children___children___internal___content = 'children.children.internal.content',
  children___children___internal___contentDigest = 'children.children.internal.contentDigest',
  children___children___internal___description = 'children.children.internal.description',
  children___children___internal___fieldOwners = 'children.children.internal.fieldOwners',
  children___children___internal___ignoreType = 'children.children.internal.ignoreType',
  children___children___internal___mediaType = 'children.children.internal.mediaType',
  children___children___internal___owner = 'children.children.internal.owner',
  children___children___internal___type = 'children.children.internal.type',
  children___internal___content = 'children.internal.content',
  children___internal___contentDigest = 'children.internal.contentDigest',
  children___internal___description = 'children.internal.description',
  children___internal___fieldOwners = 'children.internal.fieldOwners',
  children___internal___ignoreType = 'children.internal.ignoreType',
  children___internal___mediaType = 'children.internal.mediaType',
  children___internal___owner = 'children.internal.owner',
  children___internal___type = 'children.internal.type',
  internal___content = 'internal.content',
  internal___contentDigest = 'internal.contentDigest',
  internal___description = 'internal.description',
  internal___fieldOwners = 'internal.fieldOwners',
  internal___ignoreType = 'internal.ignoreType',
  internal___mediaType = 'internal.mediaType',
  internal___owner = 'internal.owner',
  internal___type = 'internal.type'
}

type MdxFieldsFilterInput = {
  readonly slug: Maybe<StringQueryOperatorInput>;
  readonly sourceInstanceName: Maybe<StringQueryOperatorInput>;
  readonly title: Maybe<StringQueryOperatorInput>;
};

type MdxFilterInput = {
  readonly frontmatter: Maybe<MdxFrontmatterFilterInput>;
  readonly rawBody: Maybe<StringQueryOperatorInput>;
  readonly fileAbsolutePath: Maybe<StringQueryOperatorInput>;
  readonly slug: Maybe<StringQueryOperatorInput>;
  readonly body: Maybe<StringQueryOperatorInput>;
  readonly excerpt: Maybe<StringQueryOperatorInput>;
  readonly headings: Maybe<MdxHeadingMdxFilterListInput>;
  readonly html: Maybe<StringQueryOperatorInput>;
  readonly mdxAST: Maybe<JSONQueryOperatorInput>;
  readonly tableOfContents: Maybe<JSONQueryOperatorInput>;
  readonly timeToRead: Maybe<IntQueryOperatorInput>;
  readonly wordCount: Maybe<MdxWordCountFilterInput>;
  readonly fields: Maybe<MdxFieldsFilterInput>;
  readonly id: Maybe<StringQueryOperatorInput>;
  readonly parent: Maybe<NodeFilterInput>;
  readonly children: Maybe<NodeFilterListInput>;
  readonly internal: Maybe<InternalFilterInput>;
};

type MdxFrontmatter = {
  readonly title: Scalars['String'];
  readonly subheader: Maybe<Scalars['String']>;
  readonly order: Maybe<Scalars['Int']>;
  readonly embeddedImages: Maybe<ReadonlyArray<Maybe<File>>>;
};

type MdxFrontmatterFilterInput = {
  readonly title: Maybe<StringQueryOperatorInput>;
  readonly subheader: Maybe<StringQueryOperatorInput>;
  readonly order: Maybe<IntQueryOperatorInput>;
  readonly embeddedImages: Maybe<FileFilterListInput>;
};

type MdxGroupConnection = {
  readonly totalCount: Scalars['Int'];
  readonly edges: ReadonlyArray<MdxEdge>;
  readonly nodes: ReadonlyArray<Mdx>;
  readonly pageInfo: PageInfo;
  readonly field: Scalars['String'];
  readonly fieldValue: Maybe<Scalars['String']>;
};

type MdxHeadingMdx = {
  readonly value: Maybe<Scalars['String']>;
  readonly depth: Maybe<Scalars['Int']>;
};

type MdxHeadingMdxFilterInput = {
  readonly value: Maybe<StringQueryOperatorInput>;
  readonly depth: Maybe<IntQueryOperatorInput>;
};

type MdxHeadingMdxFilterListInput = {
  readonly elemMatch: Maybe<MdxHeadingMdxFilterInput>;
};

type MdxSortInput = {
  readonly fields: Maybe<ReadonlyArray<Maybe<MdxFieldsEnum>>>;
  readonly order: Maybe<ReadonlyArray<Maybe<SortOrderEnum>>>;
};

type MdxWordCount = {
  readonly paragraphs: Maybe<Scalars['Int']>;
  readonly sentences: Maybe<Scalars['Int']>;
  readonly words: Maybe<Scalars['Int']>;
};

type MdxWordCountFilterInput = {
  readonly paragraphs: Maybe<IntQueryOperatorInput>;
  readonly sentences: Maybe<IntQueryOperatorInput>;
  readonly words: Maybe<IntQueryOperatorInput>;
};

/** Node Interface */
type Node = {
  readonly id: Scalars['ID'];
  readonly parent: Maybe<Node>;
  readonly children: ReadonlyArray<Node>;
  readonly internal: Internal;
};

type NodeFilterInput = {
  readonly id: Maybe<StringQueryOperatorInput>;
  readonly parent: Maybe<NodeFilterInput>;
  readonly children: Maybe<NodeFilterListInput>;
  readonly internal: Maybe<InternalFilterInput>;
};

type NodeFilterListInput = {
  readonly elemMatch: Maybe<NodeFilterInput>;
};

type PageInfo = {
  readonly currentPage: Scalars['Int'];
  readonly hasPreviousPage: Scalars['Boolean'];
  readonly hasNextPage: Scalars['Boolean'];
  readonly itemCount: Scalars['Int'];
  readonly pageCount: Scalars['Int'];
  readonly perPage: Maybe<Scalars['Int']>;
  readonly totalCount: Scalars['Int'];
};

type Potrace = {
  readonly turnPolicy: Maybe<PotraceTurnPolicy>;
  readonly turdSize: Maybe<Scalars['Float']>;
  readonly alphaMax: Maybe<Scalars['Float']>;
  readonly optCurve: Maybe<Scalars['Boolean']>;
  readonly optTolerance: Maybe<Scalars['Float']>;
  readonly threshold: Maybe<Scalars['Int']>;
  readonly blackOnWhite: Maybe<Scalars['Boolean']>;
  readonly color: Maybe<Scalars['String']>;
  readonly background: Maybe<Scalars['String']>;
};

enum PotraceTurnPolicy {
  TURNPOLICY_BLACK = 'black',
  TURNPOLICY_WHITE = 'white',
  TURNPOLICY_LEFT = 'left',
  TURNPOLICY_RIGHT = 'right',
  TURNPOLICY_MINORITY = 'minority',
  TURNPOLICY_MAJORITY = 'majority'
}

type Query = {
  readonly file: Maybe<File>;
  readonly allFile: FileConnection;
  readonly directory: Maybe<Directory>;
  readonly allDirectory: DirectoryConnection;
  readonly site: Maybe<Site>;
  readonly allSite: SiteConnection;
  readonly sitePage: Maybe<SitePage>;
  readonly allSitePage: SitePageConnection;
  readonly imageSharp: Maybe<ImageSharp>;
  readonly allImageSharp: ImageSharpConnection;
  readonly mdx: Maybe<Mdx>;
  readonly allMdx: MdxConnection;
  readonly driverSicsserialGitJson: Maybe<DriverSicsserialGitJson>;
  readonly allDriverSicsserialGitJson: DriverSicsserialGitJsonConnection;
  readonly driverMtPhGitJson: Maybe<DriverMtPhGitJson>;
  readonly allDriverMtPhGitJson: DriverMtPhGitJsonConnection;
  readonly gitRemote: Maybe<GitRemote>;
  readonly allGitRemote: GitRemoteConnection;
  readonly siteBuildMetadata: Maybe<SiteBuildMetadata>;
  readonly allSiteBuildMetadata: SiteBuildMetadataConnection;
  readonly sitePlugin: Maybe<SitePlugin>;
  readonly allSitePlugin: SitePluginConnection;
};


type Query_fileArgs = {
  sourceInstanceName: Maybe<StringQueryOperatorInput>;
  absolutePath: Maybe<StringQueryOperatorInput>;
  relativePath: Maybe<StringQueryOperatorInput>;
  extension: Maybe<StringQueryOperatorInput>;
  size: Maybe<IntQueryOperatorInput>;
  prettySize: Maybe<StringQueryOperatorInput>;
  modifiedTime: Maybe<DateQueryOperatorInput>;
  accessTime: Maybe<DateQueryOperatorInput>;
  changeTime: Maybe<DateQueryOperatorInput>;
  birthTime: Maybe<DateQueryOperatorInput>;
  root: Maybe<StringQueryOperatorInput>;
  dir: Maybe<StringQueryOperatorInput>;
  base: Maybe<StringQueryOperatorInput>;
  ext: Maybe<StringQueryOperatorInput>;
  name: Maybe<StringQueryOperatorInput>;
  relativeDirectory: Maybe<StringQueryOperatorInput>;
  dev: Maybe<IntQueryOperatorInput>;
  mode: Maybe<IntQueryOperatorInput>;
  nlink: Maybe<IntQueryOperatorInput>;
  uid: Maybe<IntQueryOperatorInput>;
  gid: Maybe<IntQueryOperatorInput>;
  rdev: Maybe<IntQueryOperatorInput>;
  ino: Maybe<FloatQueryOperatorInput>;
  atimeMs: Maybe<FloatQueryOperatorInput>;
  mtimeMs: Maybe<FloatQueryOperatorInput>;
  ctimeMs: Maybe<FloatQueryOperatorInput>;
  atime: Maybe<DateQueryOperatorInput>;
  mtime: Maybe<DateQueryOperatorInput>;
  ctime: Maybe<DateQueryOperatorInput>;
  birthtime: Maybe<DateQueryOperatorInput>;
  birthtimeMs: Maybe<FloatQueryOperatorInput>;
  blksize: Maybe<IntQueryOperatorInput>;
  blocks: Maybe<IntQueryOperatorInput>;
  gitRemote: Maybe<GitRemoteFilterInput>;
  publicURL: Maybe<StringQueryOperatorInput>;
  childImageSharp: Maybe<ImageSharpFilterInput>;
  id: Maybe<StringQueryOperatorInput>;
  parent: Maybe<NodeFilterInput>;
  children: Maybe<NodeFilterListInput>;
  internal: Maybe<InternalFilterInput>;
  childMdx: Maybe<MdxFilterInput>;
  childDriverMtPhGitJson: Maybe<DriverMtPhGitJsonFilterInput>;
  childDriverSicsserialGitJson: Maybe<DriverSicsserialGitJsonFilterInput>;
};


type Query_allFileArgs = {
  filter: Maybe<FileFilterInput>;
  sort: Maybe<FileSortInput>;
  skip: Maybe<Scalars['Int']>;
  limit: Maybe<Scalars['Int']>;
};


type Query_directoryArgs = {
  sourceInstanceName: Maybe<StringQueryOperatorInput>;
  absolutePath: Maybe<StringQueryOperatorInput>;
  relativePath: Maybe<StringQueryOperatorInput>;
  extension: Maybe<StringQueryOperatorInput>;
  size: Maybe<IntQueryOperatorInput>;
  prettySize: Maybe<StringQueryOperatorInput>;
  modifiedTime: Maybe<DateQueryOperatorInput>;
  accessTime: Maybe<DateQueryOperatorInput>;
  changeTime: Maybe<DateQueryOperatorInput>;
  birthTime: Maybe<DateQueryOperatorInput>;
  root: Maybe<StringQueryOperatorInput>;
  dir: Maybe<StringQueryOperatorInput>;
  base: Maybe<StringQueryOperatorInput>;
  ext: Maybe<StringQueryOperatorInput>;
  name: Maybe<StringQueryOperatorInput>;
  relativeDirectory: Maybe<StringQueryOperatorInput>;
  dev: Maybe<IntQueryOperatorInput>;
  mode: Maybe<IntQueryOperatorInput>;
  nlink: Maybe<IntQueryOperatorInput>;
  uid: Maybe<IntQueryOperatorInput>;
  gid: Maybe<IntQueryOperatorInput>;
  rdev: Maybe<IntQueryOperatorInput>;
  ino: Maybe<FloatQueryOperatorInput>;
  atimeMs: Maybe<FloatQueryOperatorInput>;
  mtimeMs: Maybe<FloatQueryOperatorInput>;
  ctimeMs: Maybe<FloatQueryOperatorInput>;
  atime: Maybe<DateQueryOperatorInput>;
  mtime: Maybe<DateQueryOperatorInput>;
  ctime: Maybe<DateQueryOperatorInput>;
  birthtime: Maybe<DateQueryOperatorInput>;
  birthtimeMs: Maybe<FloatQueryOperatorInput>;
  blksize: Maybe<IntQueryOperatorInput>;
  blocks: Maybe<IntQueryOperatorInput>;
  id: Maybe<StringQueryOperatorInput>;
  parent: Maybe<NodeFilterInput>;
  children: Maybe<NodeFilterListInput>;
  internal: Maybe<InternalFilterInput>;
};


type Query_allDirectoryArgs = {
  filter: Maybe<DirectoryFilterInput>;
  sort: Maybe<DirectorySortInput>;
  skip: Maybe<Scalars['Int']>;
  limit: Maybe<Scalars['Int']>;
};


type Query_siteArgs = {
  buildTime: Maybe<DateQueryOperatorInput>;
  siteMetadata: Maybe<SiteSiteMetadataFilterInput>;
  port: Maybe<IntQueryOperatorInput>;
  host: Maybe<StringQueryOperatorInput>;
  polyfill: Maybe<BooleanQueryOperatorInput>;
  pathPrefix: Maybe<StringQueryOperatorInput>;
  id: Maybe<StringQueryOperatorInput>;
  parent: Maybe<NodeFilterInput>;
  children: Maybe<NodeFilterListInput>;
  internal: Maybe<InternalFilterInput>;
};


type Query_allSiteArgs = {
  filter: Maybe<SiteFilterInput>;
  sort: Maybe<SiteSortInput>;
  skip: Maybe<Scalars['Int']>;
  limit: Maybe<Scalars['Int']>;
};


type Query_sitePageArgs = {
  path: Maybe<StringQueryOperatorInput>;
  component: Maybe<StringQueryOperatorInput>;
  internalComponentName: Maybe<StringQueryOperatorInput>;
  componentChunkName: Maybe<StringQueryOperatorInput>;
  matchPath: Maybe<StringQueryOperatorInput>;
  id: Maybe<StringQueryOperatorInput>;
  parent: Maybe<NodeFilterInput>;
  children: Maybe<NodeFilterListInput>;
  internal: Maybe<InternalFilterInput>;
  isCreatedByStatefulCreatePages: Maybe<BooleanQueryOperatorInput>;
  context: Maybe<SitePageContextFilterInput>;
  pluginCreator: Maybe<SitePluginFilterInput>;
  pluginCreatorId: Maybe<StringQueryOperatorInput>;
  componentPath: Maybe<StringQueryOperatorInput>;
};


type Query_allSitePageArgs = {
  filter: Maybe<SitePageFilterInput>;
  sort: Maybe<SitePageSortInput>;
  skip: Maybe<Scalars['Int']>;
  limit: Maybe<Scalars['Int']>;
};


type Query_imageSharpArgs = {
  fixed: Maybe<ImageSharpFixedFilterInput>;
  resolutions: Maybe<ImageSharpResolutionsFilterInput>;
  fluid: Maybe<ImageSharpFluidFilterInput>;
  sizes: Maybe<ImageSharpSizesFilterInput>;
  original: Maybe<ImageSharpOriginalFilterInput>;
  resize: Maybe<ImageSharpResizeFilterInput>;
  id: Maybe<StringQueryOperatorInput>;
  parent: Maybe<NodeFilterInput>;
  children: Maybe<NodeFilterListInput>;
  internal: Maybe<InternalFilterInput>;
};


type Query_allImageSharpArgs = {
  filter: Maybe<ImageSharpFilterInput>;
  sort: Maybe<ImageSharpSortInput>;
  skip: Maybe<Scalars['Int']>;
  limit: Maybe<Scalars['Int']>;
};


type Query_mdxArgs = {
  frontmatter: Maybe<MdxFrontmatterFilterInput>;
  rawBody: Maybe<StringQueryOperatorInput>;
  fileAbsolutePath: Maybe<StringQueryOperatorInput>;
  slug: Maybe<StringQueryOperatorInput>;
  body: Maybe<StringQueryOperatorInput>;
  excerpt: Maybe<StringQueryOperatorInput>;
  headings: Maybe<MdxHeadingMdxFilterListInput>;
  html: Maybe<StringQueryOperatorInput>;
  mdxAST: Maybe<JSONQueryOperatorInput>;
  tableOfContents: Maybe<JSONQueryOperatorInput>;
  timeToRead: Maybe<IntQueryOperatorInput>;
  wordCount: Maybe<MdxWordCountFilterInput>;
  fields: Maybe<MdxFieldsFilterInput>;
  id: Maybe<StringQueryOperatorInput>;
  parent: Maybe<NodeFilterInput>;
  children: Maybe<NodeFilterListInput>;
  internal: Maybe<InternalFilterInput>;
};


type Query_allMdxArgs = {
  filter: Maybe<MdxFilterInput>;
  sort: Maybe<MdxSortInput>;
  skip: Maybe<Scalars['Int']>;
  limit: Maybe<Scalars['Int']>;
};


type Query_driverSicsserialGitJsonArgs = {
  id: Maybe<StringQueryOperatorInput>;
  parent: Maybe<NodeFilterInput>;
  children: Maybe<NodeFilterListInput>;
  internal: Maybe<InternalFilterInput>;
  _schema: Maybe<StringQueryOperatorInput>;
  title: Maybe<StringQueryOperatorInput>;
  type: Maybe<StringQueryOperatorInput>;
  required: Maybe<StringQueryOperatorInput>;
  properties: Maybe<DriverSicsserialGitJsonPropertiesFilterInput>;
};


type Query_allDriverSicsserialGitJsonArgs = {
  filter: Maybe<DriverSicsserialGitJsonFilterInput>;
  sort: Maybe<DriverSicsserialGitJsonSortInput>;
  skip: Maybe<Scalars['Int']>;
  limit: Maybe<Scalars['Int']>;
};


type Query_driverMtPhGitJsonArgs = {
  id: Maybe<StringQueryOperatorInput>;
  parent: Maybe<NodeFilterInput>;
  children: Maybe<NodeFilterListInput>;
  internal: Maybe<InternalFilterInput>;
  _schema: Maybe<StringQueryOperatorInput>;
  title: Maybe<StringQueryOperatorInput>;
  type: Maybe<StringQueryOperatorInput>;
  required: Maybe<StringQueryOperatorInput>;
  properties: Maybe<DriverMtPhGitJsonPropertiesFilterInput>;
};


type Query_allDriverMtPhGitJsonArgs = {
  filter: Maybe<DriverMtPhGitJsonFilterInput>;
  sort: Maybe<DriverMtPhGitJsonSortInput>;
  skip: Maybe<Scalars['Int']>;
  limit: Maybe<Scalars['Int']>;
};


type Query_gitRemoteArgs = {
  id: Maybe<StringQueryOperatorInput>;
  parent: Maybe<NodeFilterInput>;
  children: Maybe<NodeFilterListInput>;
  internal: Maybe<InternalFilterInput>;
  protocol: Maybe<StringQueryOperatorInput>;
  resource: Maybe<StringQueryOperatorInput>;
  user: Maybe<StringQueryOperatorInput>;
  pathname: Maybe<StringQueryOperatorInput>;
  hash: Maybe<StringQueryOperatorInput>;
  search: Maybe<StringQueryOperatorInput>;
  href: Maybe<StringQueryOperatorInput>;
  token: Maybe<StringQueryOperatorInput>;
  source: Maybe<StringQueryOperatorInput>;
  name: Maybe<StringQueryOperatorInput>;
  owner: Maybe<StringQueryOperatorInput>;
  ref: Maybe<StringQueryOperatorInput>;
  filepathtype: Maybe<StringQueryOperatorInput>;
  filepath: Maybe<StringQueryOperatorInput>;
  organization: Maybe<StringQueryOperatorInput>;
  full_name: Maybe<StringQueryOperatorInput>;
  webLink: Maybe<StringQueryOperatorInput>;
  sourceInstanceName: Maybe<StringQueryOperatorInput>;
  childrenFile: Maybe<FileFilterListInput>;
};


type Query_allGitRemoteArgs = {
  filter: Maybe<GitRemoteFilterInput>;
  sort: Maybe<GitRemoteSortInput>;
  skip: Maybe<Scalars['Int']>;
  limit: Maybe<Scalars['Int']>;
};


type Query_siteBuildMetadataArgs = {
  id: Maybe<StringQueryOperatorInput>;
  parent: Maybe<NodeFilterInput>;
  children: Maybe<NodeFilterListInput>;
  internal: Maybe<InternalFilterInput>;
  buildTime: Maybe<DateQueryOperatorInput>;
};


type Query_allSiteBuildMetadataArgs = {
  filter: Maybe<SiteBuildMetadataFilterInput>;
  sort: Maybe<SiteBuildMetadataSortInput>;
  skip: Maybe<Scalars['Int']>;
  limit: Maybe<Scalars['Int']>;
};


type Query_sitePluginArgs = {
  id: Maybe<StringQueryOperatorInput>;
  parent: Maybe<NodeFilterInput>;
  children: Maybe<NodeFilterListInput>;
  internal: Maybe<InternalFilterInput>;
  resolve: Maybe<StringQueryOperatorInput>;
  name: Maybe<StringQueryOperatorInput>;
  version: Maybe<StringQueryOperatorInput>;
  pluginOptions: Maybe<SitePluginPluginOptionsFilterInput>;
  nodeAPIs: Maybe<StringQueryOperatorInput>;
  browserAPIs: Maybe<StringQueryOperatorInput>;
  ssrAPIs: Maybe<StringQueryOperatorInput>;
  pluginFilepath: Maybe<StringQueryOperatorInput>;
  packageJson: Maybe<SitePluginPackageJsonFilterInput>;
};


type Query_allSitePluginArgs = {
  filter: Maybe<SitePluginFilterInput>;
  sort: Maybe<SitePluginSortInput>;
  skip: Maybe<Scalars['Int']>;
  limit: Maybe<Scalars['Int']>;
};

type Site = Node & {
  readonly buildTime: Maybe<Scalars['Date']>;
  readonly siteMetadata: Maybe<SiteSiteMetadata>;
  readonly port: Maybe<Scalars['Int']>;
  readonly host: Maybe<Scalars['String']>;
  readonly polyfill: Maybe<Scalars['Boolean']>;
  readonly pathPrefix: Maybe<Scalars['String']>;
  readonly id: Scalars['ID'];
  readonly parent: Maybe<Node>;
  readonly children: ReadonlyArray<Node>;
  readonly internal: Internal;
};


type Site_buildTimeArgs = {
  formatString: Maybe<Scalars['String']>;
  fromNow: Maybe<Scalars['Boolean']>;
  difference: Maybe<Scalars['String']>;
  locale: Maybe<Scalars['String']>;
};

type SiteBuildMetadata = Node & {
  readonly id: Scalars['ID'];
  readonly parent: Maybe<Node>;
  readonly children: ReadonlyArray<Node>;
  readonly internal: Internal;
  readonly buildTime: Maybe<Scalars['Date']>;
};


type SiteBuildMetadata_buildTimeArgs = {
  formatString: Maybe<Scalars['String']>;
  fromNow: Maybe<Scalars['Boolean']>;
  difference: Maybe<Scalars['String']>;
  locale: Maybe<Scalars['String']>;
};

type SiteBuildMetadataConnection = {
  readonly totalCount: Scalars['Int'];
  readonly edges: ReadonlyArray<SiteBuildMetadataEdge>;
  readonly nodes: ReadonlyArray<SiteBuildMetadata>;
  readonly pageInfo: PageInfo;
  readonly distinct: ReadonlyArray<Scalars['String']>;
  readonly group: ReadonlyArray<SiteBuildMetadataGroupConnection>;
};


type SiteBuildMetadataConnection_distinctArgs = {
  field: SiteBuildMetadataFieldsEnum;
};


type SiteBuildMetadataConnection_groupArgs = {
  skip: Maybe<Scalars['Int']>;
  limit: Maybe<Scalars['Int']>;
  field: SiteBuildMetadataFieldsEnum;
};

type SiteBuildMetadataEdge = {
  readonly next: Maybe<SiteBuildMetadata>;
  readonly node: SiteBuildMetadata;
  readonly previous: Maybe<SiteBuildMetadata>;
};

enum SiteBuildMetadataFieldsEnum {
  id = 'id',
  parent___id = 'parent.id',
  parent___parent___id = 'parent.parent.id',
  parent___parent___parent___id = 'parent.parent.parent.id',
  parent___parent___parent___children = 'parent.parent.parent.children',
  parent___parent___children = 'parent.parent.children',
  parent___parent___children___id = 'parent.parent.children.id',
  parent___parent___children___children = 'parent.parent.children.children',
  parent___parent___internal___content = 'parent.parent.internal.content',
  parent___parent___internal___contentDigest = 'parent.parent.internal.contentDigest',
  parent___parent___internal___description = 'parent.parent.internal.description',
  parent___parent___internal___fieldOwners = 'parent.parent.internal.fieldOwners',
  parent___parent___internal___ignoreType = 'parent.parent.internal.ignoreType',
  parent___parent___internal___mediaType = 'parent.parent.internal.mediaType',
  parent___parent___internal___owner = 'parent.parent.internal.owner',
  parent___parent___internal___type = 'parent.parent.internal.type',
  parent___children = 'parent.children',
  parent___children___id = 'parent.children.id',
  parent___children___parent___id = 'parent.children.parent.id',
  parent___children___parent___children = 'parent.children.parent.children',
  parent___children___children = 'parent.children.children',
  parent___children___children___id = 'parent.children.children.id',
  parent___children___children___children = 'parent.children.children.children',
  parent___children___internal___content = 'parent.children.internal.content',
  parent___children___internal___contentDigest = 'parent.children.internal.contentDigest',
  parent___children___internal___description = 'parent.children.internal.description',
  parent___children___internal___fieldOwners = 'parent.children.internal.fieldOwners',
  parent___children___internal___ignoreType = 'parent.children.internal.ignoreType',
  parent___children___internal___mediaType = 'parent.children.internal.mediaType',
  parent___children___internal___owner = 'parent.children.internal.owner',
  parent___children___internal___type = 'parent.children.internal.type',
  parent___internal___content = 'parent.internal.content',
  parent___internal___contentDigest = 'parent.internal.contentDigest',
  parent___internal___description = 'parent.internal.description',
  parent___internal___fieldOwners = 'parent.internal.fieldOwners',
  parent___internal___ignoreType = 'parent.internal.ignoreType',
  parent___internal___mediaType = 'parent.internal.mediaType',
  parent___internal___owner = 'parent.internal.owner',
  parent___internal___type = 'parent.internal.type',
  children = 'children',
  children___id = 'children.id',
  children___parent___id = 'children.parent.id',
  children___parent___parent___id = 'children.parent.parent.id',
  children___parent___parent___children = 'children.parent.parent.children',
  children___parent___children = 'children.parent.children',
  children___parent___children___id = 'children.parent.children.id',
  children___parent___children___children = 'children.parent.children.children',
  children___parent___internal___content = 'children.parent.internal.content',
  children___parent___internal___contentDigest = 'children.parent.internal.contentDigest',
  children___parent___internal___description = 'children.parent.internal.description',
  children___parent___internal___fieldOwners = 'children.parent.internal.fieldOwners',
  children___parent___internal___ignoreType = 'children.parent.internal.ignoreType',
  children___parent___internal___mediaType = 'children.parent.internal.mediaType',
  children___parent___internal___owner = 'children.parent.internal.owner',
  children___parent___internal___type = 'children.parent.internal.type',
  children___children = 'children.children',
  children___children___id = 'children.children.id',
  children___children___parent___id = 'children.children.parent.id',
  children___children___parent___children = 'children.children.parent.children',
  children___children___children = 'children.children.children',
  children___children___children___id = 'children.children.children.id',
  children___children___children___children = 'children.children.children.children',
  children___children___internal___content = 'children.children.internal.content',
  children___children___internal___contentDigest = 'children.children.internal.contentDigest',
  children___children___internal___description = 'children.children.internal.description',
  children___children___internal___fieldOwners = 'children.children.internal.fieldOwners',
  children___children___internal___ignoreType = 'children.children.internal.ignoreType',
  children___children___internal___mediaType = 'children.children.internal.mediaType',
  children___children___internal___owner = 'children.children.internal.owner',
  children___children___internal___type = 'children.children.internal.type',
  children___internal___content = 'children.internal.content',
  children___internal___contentDigest = 'children.internal.contentDigest',
  children___internal___description = 'children.internal.description',
  children___internal___fieldOwners = 'children.internal.fieldOwners',
  children___internal___ignoreType = 'children.internal.ignoreType',
  children___internal___mediaType = 'children.internal.mediaType',
  children___internal___owner = 'children.internal.owner',
  children___internal___type = 'children.internal.type',
  internal___content = 'internal.content',
  internal___contentDigest = 'internal.contentDigest',
  internal___description = 'internal.description',
  internal___fieldOwners = 'internal.fieldOwners',
  internal___ignoreType = 'internal.ignoreType',
  internal___mediaType = 'internal.mediaType',
  internal___owner = 'internal.owner',
  internal___type = 'internal.type',
  buildTime = 'buildTime'
}

type SiteBuildMetadataFilterInput = {
  readonly id: Maybe<StringQueryOperatorInput>;
  readonly parent: Maybe<NodeFilterInput>;
  readonly children: Maybe<NodeFilterListInput>;
  readonly internal: Maybe<InternalFilterInput>;
  readonly buildTime: Maybe<DateQueryOperatorInput>;
};

type SiteBuildMetadataGroupConnection = {
  readonly totalCount: Scalars['Int'];
  readonly edges: ReadonlyArray<SiteBuildMetadataEdge>;
  readonly nodes: ReadonlyArray<SiteBuildMetadata>;
  readonly pageInfo: PageInfo;
  readonly field: Scalars['String'];
  readonly fieldValue: Maybe<Scalars['String']>;
};

type SiteBuildMetadataSortInput = {
  readonly fields: Maybe<ReadonlyArray<Maybe<SiteBuildMetadataFieldsEnum>>>;
  readonly order: Maybe<ReadonlyArray<Maybe<SortOrderEnum>>>;
};

type SiteConnection = {
  readonly totalCount: Scalars['Int'];
  readonly edges: ReadonlyArray<SiteEdge>;
  readonly nodes: ReadonlyArray<Site>;
  readonly pageInfo: PageInfo;
  readonly distinct: ReadonlyArray<Scalars['String']>;
  readonly group: ReadonlyArray<SiteGroupConnection>;
};


type SiteConnection_distinctArgs = {
  field: SiteFieldsEnum;
};


type SiteConnection_groupArgs = {
  skip: Maybe<Scalars['Int']>;
  limit: Maybe<Scalars['Int']>;
  field: SiteFieldsEnum;
};

type SiteEdge = {
  readonly next: Maybe<Site>;
  readonly node: Site;
  readonly previous: Maybe<Site>;
};

enum SiteFieldsEnum {
  buildTime = 'buildTime',
  siteMetadata___title = 'siteMetadata.title',
  siteMetadata___description = 'siteMetadata.description',
  siteMetadata___siteUrl = 'siteMetadata.siteUrl',
  siteMetadata___githubUrl = 'siteMetadata.githubUrl',
  siteMetadata___robots = 'siteMetadata.robots',
  port = 'port',
  host = 'host',
  polyfill = 'polyfill',
  pathPrefix = 'pathPrefix',
  id = 'id',
  parent___id = 'parent.id',
  parent___parent___id = 'parent.parent.id',
  parent___parent___parent___id = 'parent.parent.parent.id',
  parent___parent___parent___children = 'parent.parent.parent.children',
  parent___parent___children = 'parent.parent.children',
  parent___parent___children___id = 'parent.parent.children.id',
  parent___parent___children___children = 'parent.parent.children.children',
  parent___parent___internal___content = 'parent.parent.internal.content',
  parent___parent___internal___contentDigest = 'parent.parent.internal.contentDigest',
  parent___parent___internal___description = 'parent.parent.internal.description',
  parent___parent___internal___fieldOwners = 'parent.parent.internal.fieldOwners',
  parent___parent___internal___ignoreType = 'parent.parent.internal.ignoreType',
  parent___parent___internal___mediaType = 'parent.parent.internal.mediaType',
  parent___parent___internal___owner = 'parent.parent.internal.owner',
  parent___parent___internal___type = 'parent.parent.internal.type',
  parent___children = 'parent.children',
  parent___children___id = 'parent.children.id',
  parent___children___parent___id = 'parent.children.parent.id',
  parent___children___parent___children = 'parent.children.parent.children',
  parent___children___children = 'parent.children.children',
  parent___children___children___id = 'parent.children.children.id',
  parent___children___children___children = 'parent.children.children.children',
  parent___children___internal___content = 'parent.children.internal.content',
  parent___children___internal___contentDigest = 'parent.children.internal.contentDigest',
  parent___children___internal___description = 'parent.children.internal.description',
  parent___children___internal___fieldOwners = 'parent.children.internal.fieldOwners',
  parent___children___internal___ignoreType = 'parent.children.internal.ignoreType',
  parent___children___internal___mediaType = 'parent.children.internal.mediaType',
  parent___children___internal___owner = 'parent.children.internal.owner',
  parent___children___internal___type = 'parent.children.internal.type',
  parent___internal___content = 'parent.internal.content',
  parent___internal___contentDigest = 'parent.internal.contentDigest',
  parent___internal___description = 'parent.internal.description',
  parent___internal___fieldOwners = 'parent.internal.fieldOwners',
  parent___internal___ignoreType = 'parent.internal.ignoreType',
  parent___internal___mediaType = 'parent.internal.mediaType',
  parent___internal___owner = 'parent.internal.owner',
  parent___internal___type = 'parent.internal.type',
  children = 'children',
  children___id = 'children.id',
  children___parent___id = 'children.parent.id',
  children___parent___parent___id = 'children.parent.parent.id',
  children___parent___parent___children = 'children.parent.parent.children',
  children___parent___children = 'children.parent.children',
  children___parent___children___id = 'children.parent.children.id',
  children___parent___children___children = 'children.parent.children.children',
  children___parent___internal___content = 'children.parent.internal.content',
  children___parent___internal___contentDigest = 'children.parent.internal.contentDigest',
  children___parent___internal___description = 'children.parent.internal.description',
  children___parent___internal___fieldOwners = 'children.parent.internal.fieldOwners',
  children___parent___internal___ignoreType = 'children.parent.internal.ignoreType',
  children___parent___internal___mediaType = 'children.parent.internal.mediaType',
  children___parent___internal___owner = 'children.parent.internal.owner',
  children___parent___internal___type = 'children.parent.internal.type',
  children___children = 'children.children',
  children___children___id = 'children.children.id',
  children___children___parent___id = 'children.children.parent.id',
  children___children___parent___children = 'children.children.parent.children',
  children___children___children = 'children.children.children',
  children___children___children___id = 'children.children.children.id',
  children___children___children___children = 'children.children.children.children',
  children___children___internal___content = 'children.children.internal.content',
  children___children___internal___contentDigest = 'children.children.internal.contentDigest',
  children___children___internal___description = 'children.children.internal.description',
  children___children___internal___fieldOwners = 'children.children.internal.fieldOwners',
  children___children___internal___ignoreType = 'children.children.internal.ignoreType',
  children___children___internal___mediaType = 'children.children.internal.mediaType',
  children___children___internal___owner = 'children.children.internal.owner',
  children___children___internal___type = 'children.children.internal.type',
  children___internal___content = 'children.internal.content',
  children___internal___contentDigest = 'children.internal.contentDigest',
  children___internal___description = 'children.internal.description',
  children___internal___fieldOwners = 'children.internal.fieldOwners',
  children___internal___ignoreType = 'children.internal.ignoreType',
  children___internal___mediaType = 'children.internal.mediaType',
  children___internal___owner = 'children.internal.owner',
  children___internal___type = 'children.internal.type',
  internal___content = 'internal.content',
  internal___contentDigest = 'internal.contentDigest',
  internal___description = 'internal.description',
  internal___fieldOwners = 'internal.fieldOwners',
  internal___ignoreType = 'internal.ignoreType',
  internal___mediaType = 'internal.mediaType',
  internal___owner = 'internal.owner',
  internal___type = 'internal.type'
}

type SiteFilterInput = {
  readonly buildTime: Maybe<DateQueryOperatorInput>;
  readonly siteMetadata: Maybe<SiteSiteMetadataFilterInput>;
  readonly port: Maybe<IntQueryOperatorInput>;
  readonly host: Maybe<StringQueryOperatorInput>;
  readonly polyfill: Maybe<BooleanQueryOperatorInput>;
  readonly pathPrefix: Maybe<StringQueryOperatorInput>;
  readonly id: Maybe<StringQueryOperatorInput>;
  readonly parent: Maybe<NodeFilterInput>;
  readonly children: Maybe<NodeFilterListInput>;
  readonly internal: Maybe<InternalFilterInput>;
};

type SiteGroupConnection = {
  readonly totalCount: Scalars['Int'];
  readonly edges: ReadonlyArray<SiteEdge>;
  readonly nodes: ReadonlyArray<Site>;
  readonly pageInfo: PageInfo;
  readonly field: Scalars['String'];
  readonly fieldValue: Maybe<Scalars['String']>;
};

type SitePage = Node & {
  readonly path: Scalars['String'];
  readonly component: Scalars['String'];
  readonly internalComponentName: Scalars['String'];
  readonly componentChunkName: Scalars['String'];
  readonly matchPath: Maybe<Scalars['String']>;
  readonly id: Scalars['ID'];
  readonly parent: Maybe<Node>;
  readonly children: ReadonlyArray<Node>;
  readonly internal: Internal;
  readonly isCreatedByStatefulCreatePages: Maybe<Scalars['Boolean']>;
  readonly context: Maybe<SitePageContext>;
  readonly pluginCreator: Maybe<SitePlugin>;
  readonly pluginCreatorId: Maybe<Scalars['String']>;
  readonly componentPath: Maybe<Scalars['String']>;
};

type SitePageConnection = {
  readonly totalCount: Scalars['Int'];
  readonly edges: ReadonlyArray<SitePageEdge>;
  readonly nodes: ReadonlyArray<SitePage>;
  readonly pageInfo: PageInfo;
  readonly distinct: ReadonlyArray<Scalars['String']>;
  readonly group: ReadonlyArray<SitePageGroupConnection>;
};


type SitePageConnection_distinctArgs = {
  field: SitePageFieldsEnum;
};


type SitePageConnection_groupArgs = {
  skip: Maybe<Scalars['Int']>;
  limit: Maybe<Scalars['Int']>;
  field: SitePageFieldsEnum;
};

type SitePageContext = {
  readonly frontmatter: Maybe<SitePageContextFrontmatter>;
  readonly slug: Maybe<Scalars['String']>;
};

type SitePageContextFilterInput = {
  readonly frontmatter: Maybe<SitePageContextFrontmatterFilterInput>;
  readonly slug: Maybe<StringQueryOperatorInput>;
};

type SitePageContextFrontmatter = {
  readonly title: Maybe<Scalars['String']>;
  readonly subheader: Maybe<Scalars['String']>;
  readonly order: Maybe<Scalars['Int']>;
};

type SitePageContextFrontmatterFilterInput = {
  readonly title: Maybe<StringQueryOperatorInput>;
  readonly subheader: Maybe<StringQueryOperatorInput>;
  readonly order: Maybe<IntQueryOperatorInput>;
};

type SitePageEdge = {
  readonly next: Maybe<SitePage>;
  readonly node: SitePage;
  readonly previous: Maybe<SitePage>;
};

enum SitePageFieldsEnum {
  path = 'path',
  component = 'component',
  internalComponentName = 'internalComponentName',
  componentChunkName = 'componentChunkName',
  matchPath = 'matchPath',
  id = 'id',
  parent___id = 'parent.id',
  parent___parent___id = 'parent.parent.id',
  parent___parent___parent___id = 'parent.parent.parent.id',
  parent___parent___parent___children = 'parent.parent.parent.children',
  parent___parent___children = 'parent.parent.children',
  parent___parent___children___id = 'parent.parent.children.id',
  parent___parent___children___children = 'parent.parent.children.children',
  parent___parent___internal___content = 'parent.parent.internal.content',
  parent___parent___internal___contentDigest = 'parent.parent.internal.contentDigest',
  parent___parent___internal___description = 'parent.parent.internal.description',
  parent___parent___internal___fieldOwners = 'parent.parent.internal.fieldOwners',
  parent___parent___internal___ignoreType = 'parent.parent.internal.ignoreType',
  parent___parent___internal___mediaType = 'parent.parent.internal.mediaType',
  parent___parent___internal___owner = 'parent.parent.internal.owner',
  parent___parent___internal___type = 'parent.parent.internal.type',
  parent___children = 'parent.children',
  parent___children___id = 'parent.children.id',
  parent___children___parent___id = 'parent.children.parent.id',
  parent___children___parent___children = 'parent.children.parent.children',
  parent___children___children = 'parent.children.children',
  parent___children___children___id = 'parent.children.children.id',
  parent___children___children___children = 'parent.children.children.children',
  parent___children___internal___content = 'parent.children.internal.content',
  parent___children___internal___contentDigest = 'parent.children.internal.contentDigest',
  parent___children___internal___description = 'parent.children.internal.description',
  parent___children___internal___fieldOwners = 'parent.children.internal.fieldOwners',
  parent___children___internal___ignoreType = 'parent.children.internal.ignoreType',
  parent___children___internal___mediaType = 'parent.children.internal.mediaType',
  parent___children___internal___owner = 'parent.children.internal.owner',
  parent___children___internal___type = 'parent.children.internal.type',
  parent___internal___content = 'parent.internal.content',
  parent___internal___contentDigest = 'parent.internal.contentDigest',
  parent___internal___description = 'parent.internal.description',
  parent___internal___fieldOwners = 'parent.internal.fieldOwners',
  parent___internal___ignoreType = 'parent.internal.ignoreType',
  parent___internal___mediaType = 'parent.internal.mediaType',
  parent___internal___owner = 'parent.internal.owner',
  parent___internal___type = 'parent.internal.type',
  children = 'children',
  children___id = 'children.id',
  children___parent___id = 'children.parent.id',
  children___parent___parent___id = 'children.parent.parent.id',
  children___parent___parent___children = 'children.parent.parent.children',
  children___parent___children = 'children.parent.children',
  children___parent___children___id = 'children.parent.children.id',
  children___parent___children___children = 'children.parent.children.children',
  children___parent___internal___content = 'children.parent.internal.content',
  children___parent___internal___contentDigest = 'children.parent.internal.contentDigest',
  children___parent___internal___description = 'children.parent.internal.description',
  children___parent___internal___fieldOwners = 'children.parent.internal.fieldOwners',
  children___parent___internal___ignoreType = 'children.parent.internal.ignoreType',
  children___parent___internal___mediaType = 'children.parent.internal.mediaType',
  children___parent___internal___owner = 'children.parent.internal.owner',
  children___parent___internal___type = 'children.parent.internal.type',
  children___children = 'children.children',
  children___children___id = 'children.children.id',
  children___children___parent___id = 'children.children.parent.id',
  children___children___parent___children = 'children.children.parent.children',
  children___children___children = 'children.children.children',
  children___children___children___id = 'children.children.children.id',
  children___children___children___children = 'children.children.children.children',
  children___children___internal___content = 'children.children.internal.content',
  children___children___internal___contentDigest = 'children.children.internal.contentDigest',
  children___children___internal___description = 'children.children.internal.description',
  children___children___internal___fieldOwners = 'children.children.internal.fieldOwners',
  children___children___internal___ignoreType = 'children.children.internal.ignoreType',
  children___children___internal___mediaType = 'children.children.internal.mediaType',
  children___children___internal___owner = 'children.children.internal.owner',
  children___children___internal___type = 'children.children.internal.type',
  children___internal___content = 'children.internal.content',
  children___internal___contentDigest = 'children.internal.contentDigest',
  children___internal___description = 'children.internal.description',
  children___internal___fieldOwners = 'children.internal.fieldOwners',
  children___internal___ignoreType = 'children.internal.ignoreType',
  children___internal___mediaType = 'children.internal.mediaType',
  children___internal___owner = 'children.internal.owner',
  children___internal___type = 'children.internal.type',
  internal___content = 'internal.content',
  internal___contentDigest = 'internal.contentDigest',
  internal___description = 'internal.description',
  internal___fieldOwners = 'internal.fieldOwners',
  internal___ignoreType = 'internal.ignoreType',
  internal___mediaType = 'internal.mediaType',
  internal___owner = 'internal.owner',
  internal___type = 'internal.type',
  isCreatedByStatefulCreatePages = 'isCreatedByStatefulCreatePages',
  context___frontmatter___title = 'context.frontmatter.title',
  context___frontmatter___subheader = 'context.frontmatter.subheader',
  context___frontmatter___order = 'context.frontmatter.order',
  context___slug = 'context.slug',
  pluginCreator___id = 'pluginCreator.id',
  pluginCreator___parent___id = 'pluginCreator.parent.id',
  pluginCreator___parent___parent___id = 'pluginCreator.parent.parent.id',
  pluginCreator___parent___parent___children = 'pluginCreator.parent.parent.children',
  pluginCreator___parent___children = 'pluginCreator.parent.children',
  pluginCreator___parent___children___id = 'pluginCreator.parent.children.id',
  pluginCreator___parent___children___children = 'pluginCreator.parent.children.children',
  pluginCreator___parent___internal___content = 'pluginCreator.parent.internal.content',
  pluginCreator___parent___internal___contentDigest = 'pluginCreator.parent.internal.contentDigest',
  pluginCreator___parent___internal___description = 'pluginCreator.parent.internal.description',
  pluginCreator___parent___internal___fieldOwners = 'pluginCreator.parent.internal.fieldOwners',
  pluginCreator___parent___internal___ignoreType = 'pluginCreator.parent.internal.ignoreType',
  pluginCreator___parent___internal___mediaType = 'pluginCreator.parent.internal.mediaType',
  pluginCreator___parent___internal___owner = 'pluginCreator.parent.internal.owner',
  pluginCreator___parent___internal___type = 'pluginCreator.parent.internal.type',
  pluginCreator___children = 'pluginCreator.children',
  pluginCreator___children___id = 'pluginCreator.children.id',
  pluginCreator___children___parent___id = 'pluginCreator.children.parent.id',
  pluginCreator___children___parent___children = 'pluginCreator.children.parent.children',
  pluginCreator___children___children = 'pluginCreator.children.children',
  pluginCreator___children___children___id = 'pluginCreator.children.children.id',
  pluginCreator___children___children___children = 'pluginCreator.children.children.children',
  pluginCreator___children___internal___content = 'pluginCreator.children.internal.content',
  pluginCreator___children___internal___contentDigest = 'pluginCreator.children.internal.contentDigest',
  pluginCreator___children___internal___description = 'pluginCreator.children.internal.description',
  pluginCreator___children___internal___fieldOwners = 'pluginCreator.children.internal.fieldOwners',
  pluginCreator___children___internal___ignoreType = 'pluginCreator.children.internal.ignoreType',
  pluginCreator___children___internal___mediaType = 'pluginCreator.children.internal.mediaType',
  pluginCreator___children___internal___owner = 'pluginCreator.children.internal.owner',
  pluginCreator___children___internal___type = 'pluginCreator.children.internal.type',
  pluginCreator___internal___content = 'pluginCreator.internal.content',
  pluginCreator___internal___contentDigest = 'pluginCreator.internal.contentDigest',
  pluginCreator___internal___description = 'pluginCreator.internal.description',
  pluginCreator___internal___fieldOwners = 'pluginCreator.internal.fieldOwners',
  pluginCreator___internal___ignoreType = 'pluginCreator.internal.ignoreType',
  pluginCreator___internal___mediaType = 'pluginCreator.internal.mediaType',
  pluginCreator___internal___owner = 'pluginCreator.internal.owner',
  pluginCreator___internal___type = 'pluginCreator.internal.type',
  pluginCreator___resolve = 'pluginCreator.resolve',
  pluginCreator___name = 'pluginCreator.name',
  pluginCreator___version = 'pluginCreator.version',
  pluginCreator___pluginOptions___extensions = 'pluginCreator.pluginOptions.extensions',
  pluginCreator___pluginOptions___name = 'pluginCreator.pluginOptions.name',
  pluginCreator___pluginOptions___path = 'pluginCreator.pluginOptions.path',
  pluginCreator___pluginOptions___remote = 'pluginCreator.pluginOptions.remote',
  pluginCreator___pluginOptions___branch = 'pluginCreator.pluginOptions.branch',
  pluginCreator___pluginOptions___patterns = 'pluginCreator.pluginOptions.patterns',
  pluginCreator___pluginOptions___stylesProvider___injectFirst = 'pluginCreator.pluginOptions.stylesProvider.injectFirst',
  pluginCreator___pluginOptions___short_name = 'pluginCreator.pluginOptions.short_name',
  pluginCreator___pluginOptions___start_url = 'pluginCreator.pluginOptions.start_url',
  pluginCreator___pluginOptions___background_color = 'pluginCreator.pluginOptions.background_color',
  pluginCreator___pluginOptions___theme_color = 'pluginCreator.pluginOptions.theme_color',
  pluginCreator___pluginOptions___display = 'pluginCreator.pluginOptions.display',
  pluginCreator___pluginOptions___icon = 'pluginCreator.pluginOptions.icon',
  pluginCreator___pluginOptions___cache_busting_mode = 'pluginCreator.pluginOptions.cache_busting_mode',
  pluginCreator___pluginOptions___include_favicon = 'pluginCreator.pluginOptions.include_favicon',
  pluginCreator___pluginOptions___legacy = 'pluginCreator.pluginOptions.legacy',
  pluginCreator___pluginOptions___theme_color_in_head = 'pluginCreator.pluginOptions.theme_color_in_head',
  pluginCreator___pluginOptions___cacheDigest = 'pluginCreator.pluginOptions.cacheDigest',
  pluginCreator___pluginOptions___gitSources = 'pluginCreator.pluginOptions.gitSources',
  pluginCreator___pluginOptions___gitSources___remote = 'pluginCreator.pluginOptions.gitSources.remote',
  pluginCreator___pluginOptions___gitSources___branch = 'pluginCreator.pluginOptions.gitSources.branch',
  pluginCreator___pluginOptions___gitSources___patterns = 'pluginCreator.pluginOptions.gitSources.patterns',
  pluginCreator___pluginOptions___pathCheck = 'pluginCreator.pluginOptions.pathCheck',
  pluginCreator___nodeAPIs = 'pluginCreator.nodeAPIs',
  pluginCreator___browserAPIs = 'pluginCreator.browserAPIs',
  pluginCreator___ssrAPIs = 'pluginCreator.ssrAPIs',
  pluginCreator___pluginFilepath = 'pluginCreator.pluginFilepath',
  pluginCreator___packageJson___name = 'pluginCreator.packageJson.name',
  pluginCreator___packageJson___description = 'pluginCreator.packageJson.description',
  pluginCreator___packageJson___version = 'pluginCreator.packageJson.version',
  pluginCreator___packageJson___main = 'pluginCreator.packageJson.main',
  pluginCreator___packageJson___license = 'pluginCreator.packageJson.license',
  pluginCreator___packageJson___dependencies = 'pluginCreator.packageJson.dependencies',
  pluginCreator___packageJson___dependencies___name = 'pluginCreator.packageJson.dependencies.name',
  pluginCreator___packageJson___dependencies___version = 'pluginCreator.packageJson.dependencies.version',
  pluginCreator___packageJson___devDependencies = 'pluginCreator.packageJson.devDependencies',
  pluginCreator___packageJson___devDependencies___name = 'pluginCreator.packageJson.devDependencies.name',
  pluginCreator___packageJson___devDependencies___version = 'pluginCreator.packageJson.devDependencies.version',
  pluginCreator___packageJson___peerDependencies = 'pluginCreator.packageJson.peerDependencies',
  pluginCreator___packageJson___peerDependencies___name = 'pluginCreator.packageJson.peerDependencies.name',
  pluginCreator___packageJson___peerDependencies___version = 'pluginCreator.packageJson.peerDependencies.version',
  pluginCreator___packageJson___keywords = 'pluginCreator.packageJson.keywords',
  pluginCreatorId = 'pluginCreatorId',
  componentPath = 'componentPath'
}

type SitePageFilterInput = {
  readonly path: Maybe<StringQueryOperatorInput>;
  readonly component: Maybe<StringQueryOperatorInput>;
  readonly internalComponentName: Maybe<StringQueryOperatorInput>;
  readonly componentChunkName: Maybe<StringQueryOperatorInput>;
  readonly matchPath: Maybe<StringQueryOperatorInput>;
  readonly id: Maybe<StringQueryOperatorInput>;
  readonly parent: Maybe<NodeFilterInput>;
  readonly children: Maybe<NodeFilterListInput>;
  readonly internal: Maybe<InternalFilterInput>;
  readonly isCreatedByStatefulCreatePages: Maybe<BooleanQueryOperatorInput>;
  readonly context: Maybe<SitePageContextFilterInput>;
  readonly pluginCreator: Maybe<SitePluginFilterInput>;
  readonly pluginCreatorId: Maybe<StringQueryOperatorInput>;
  readonly componentPath: Maybe<StringQueryOperatorInput>;
};

type SitePageGroupConnection = {
  readonly totalCount: Scalars['Int'];
  readonly edges: ReadonlyArray<SitePageEdge>;
  readonly nodes: ReadonlyArray<SitePage>;
  readonly pageInfo: PageInfo;
  readonly field: Scalars['String'];
  readonly fieldValue: Maybe<Scalars['String']>;
};

type SitePageSortInput = {
  readonly fields: Maybe<ReadonlyArray<Maybe<SitePageFieldsEnum>>>;
  readonly order: Maybe<ReadonlyArray<Maybe<SortOrderEnum>>>;
};

type SitePlugin = Node & {
  readonly id: Scalars['ID'];
  readonly parent: Maybe<Node>;
  readonly children: ReadonlyArray<Node>;
  readonly internal: Internal;
  readonly resolve: Maybe<Scalars['String']>;
  readonly name: Maybe<Scalars['String']>;
  readonly version: Maybe<Scalars['String']>;
  readonly pluginOptions: Maybe<SitePluginPluginOptions>;
  readonly nodeAPIs: Maybe<ReadonlyArray<Maybe<Scalars['String']>>>;
  readonly browserAPIs: Maybe<ReadonlyArray<Maybe<Scalars['String']>>>;
  readonly ssrAPIs: Maybe<ReadonlyArray<Maybe<Scalars['String']>>>;
  readonly pluginFilepath: Maybe<Scalars['String']>;
  readonly packageJson: Maybe<SitePluginPackageJson>;
};

type SitePluginConnection = {
  readonly totalCount: Scalars['Int'];
  readonly edges: ReadonlyArray<SitePluginEdge>;
  readonly nodes: ReadonlyArray<SitePlugin>;
  readonly pageInfo: PageInfo;
  readonly distinct: ReadonlyArray<Scalars['String']>;
  readonly group: ReadonlyArray<SitePluginGroupConnection>;
};


type SitePluginConnection_distinctArgs = {
  field: SitePluginFieldsEnum;
};


type SitePluginConnection_groupArgs = {
  skip: Maybe<Scalars['Int']>;
  limit: Maybe<Scalars['Int']>;
  field: SitePluginFieldsEnum;
};

type SitePluginEdge = {
  readonly next: Maybe<SitePlugin>;
  readonly node: SitePlugin;
  readonly previous: Maybe<SitePlugin>;
};

enum SitePluginFieldsEnum {
  id = 'id',
  parent___id = 'parent.id',
  parent___parent___id = 'parent.parent.id',
  parent___parent___parent___id = 'parent.parent.parent.id',
  parent___parent___parent___children = 'parent.parent.parent.children',
  parent___parent___children = 'parent.parent.children',
  parent___parent___children___id = 'parent.parent.children.id',
  parent___parent___children___children = 'parent.parent.children.children',
  parent___parent___internal___content = 'parent.parent.internal.content',
  parent___parent___internal___contentDigest = 'parent.parent.internal.contentDigest',
  parent___parent___internal___description = 'parent.parent.internal.description',
  parent___parent___internal___fieldOwners = 'parent.parent.internal.fieldOwners',
  parent___parent___internal___ignoreType = 'parent.parent.internal.ignoreType',
  parent___parent___internal___mediaType = 'parent.parent.internal.mediaType',
  parent___parent___internal___owner = 'parent.parent.internal.owner',
  parent___parent___internal___type = 'parent.parent.internal.type',
  parent___children = 'parent.children',
  parent___children___id = 'parent.children.id',
  parent___children___parent___id = 'parent.children.parent.id',
  parent___children___parent___children = 'parent.children.parent.children',
  parent___children___children = 'parent.children.children',
  parent___children___children___id = 'parent.children.children.id',
  parent___children___children___children = 'parent.children.children.children',
  parent___children___internal___content = 'parent.children.internal.content',
  parent___children___internal___contentDigest = 'parent.children.internal.contentDigest',
  parent___children___internal___description = 'parent.children.internal.description',
  parent___children___internal___fieldOwners = 'parent.children.internal.fieldOwners',
  parent___children___internal___ignoreType = 'parent.children.internal.ignoreType',
  parent___children___internal___mediaType = 'parent.children.internal.mediaType',
  parent___children___internal___owner = 'parent.children.internal.owner',
  parent___children___internal___type = 'parent.children.internal.type',
  parent___internal___content = 'parent.internal.content',
  parent___internal___contentDigest = 'parent.internal.contentDigest',
  parent___internal___description = 'parent.internal.description',
  parent___internal___fieldOwners = 'parent.internal.fieldOwners',
  parent___internal___ignoreType = 'parent.internal.ignoreType',
  parent___internal___mediaType = 'parent.internal.mediaType',
  parent___internal___owner = 'parent.internal.owner',
  parent___internal___type = 'parent.internal.type',
  children = 'children',
  children___id = 'children.id',
  children___parent___id = 'children.parent.id',
  children___parent___parent___id = 'children.parent.parent.id',
  children___parent___parent___children = 'children.parent.parent.children',
  children___parent___children = 'children.parent.children',
  children___parent___children___id = 'children.parent.children.id',
  children___parent___children___children = 'children.parent.children.children',
  children___parent___internal___content = 'children.parent.internal.content',
  children___parent___internal___contentDigest = 'children.parent.internal.contentDigest',
  children___parent___internal___description = 'children.parent.internal.description',
  children___parent___internal___fieldOwners = 'children.parent.internal.fieldOwners',
  children___parent___internal___ignoreType = 'children.parent.internal.ignoreType',
  children___parent___internal___mediaType = 'children.parent.internal.mediaType',
  children___parent___internal___owner = 'children.parent.internal.owner',
  children___parent___internal___type = 'children.parent.internal.type',
  children___children = 'children.children',
  children___children___id = 'children.children.id',
  children___children___parent___id = 'children.children.parent.id',
  children___children___parent___children = 'children.children.parent.children',
  children___children___children = 'children.children.children',
  children___children___children___id = 'children.children.children.id',
  children___children___children___children = 'children.children.children.children',
  children___children___internal___content = 'children.children.internal.content',
  children___children___internal___contentDigest = 'children.children.internal.contentDigest',
  children___children___internal___description = 'children.children.internal.description',
  children___children___internal___fieldOwners = 'children.children.internal.fieldOwners',
  children___children___internal___ignoreType = 'children.children.internal.ignoreType',
  children___children___internal___mediaType = 'children.children.internal.mediaType',
  children___children___internal___owner = 'children.children.internal.owner',
  children___children___internal___type = 'children.children.internal.type',
  children___internal___content = 'children.internal.content',
  children___internal___contentDigest = 'children.internal.contentDigest',
  children___internal___description = 'children.internal.description',
  children___internal___fieldOwners = 'children.internal.fieldOwners',
  children___internal___ignoreType = 'children.internal.ignoreType',
  children___internal___mediaType = 'children.internal.mediaType',
  children___internal___owner = 'children.internal.owner',
  children___internal___type = 'children.internal.type',
  internal___content = 'internal.content',
  internal___contentDigest = 'internal.contentDigest',
  internal___description = 'internal.description',
  internal___fieldOwners = 'internal.fieldOwners',
  internal___ignoreType = 'internal.ignoreType',
  internal___mediaType = 'internal.mediaType',
  internal___owner = 'internal.owner',
  internal___type = 'internal.type',
  resolve = 'resolve',
  name = 'name',
  version = 'version',
  pluginOptions___extensions = 'pluginOptions.extensions',
  pluginOptions___name = 'pluginOptions.name',
  pluginOptions___path = 'pluginOptions.path',
  pluginOptions___remote = 'pluginOptions.remote',
  pluginOptions___branch = 'pluginOptions.branch',
  pluginOptions___patterns = 'pluginOptions.patterns',
  pluginOptions___stylesProvider___injectFirst = 'pluginOptions.stylesProvider.injectFirst',
  pluginOptions___short_name = 'pluginOptions.short_name',
  pluginOptions___start_url = 'pluginOptions.start_url',
  pluginOptions___background_color = 'pluginOptions.background_color',
  pluginOptions___theme_color = 'pluginOptions.theme_color',
  pluginOptions___display = 'pluginOptions.display',
  pluginOptions___icon = 'pluginOptions.icon',
  pluginOptions___cache_busting_mode = 'pluginOptions.cache_busting_mode',
  pluginOptions___include_favicon = 'pluginOptions.include_favicon',
  pluginOptions___legacy = 'pluginOptions.legacy',
  pluginOptions___theme_color_in_head = 'pluginOptions.theme_color_in_head',
  pluginOptions___cacheDigest = 'pluginOptions.cacheDigest',
  pluginOptions___gitSources = 'pluginOptions.gitSources',
  pluginOptions___gitSources___remote = 'pluginOptions.gitSources.remote',
  pluginOptions___gitSources___branch = 'pluginOptions.gitSources.branch',
  pluginOptions___gitSources___patterns = 'pluginOptions.gitSources.patterns',
  pluginOptions___pathCheck = 'pluginOptions.pathCheck',
  nodeAPIs = 'nodeAPIs',
  browserAPIs = 'browserAPIs',
  ssrAPIs = 'ssrAPIs',
  pluginFilepath = 'pluginFilepath',
  packageJson___name = 'packageJson.name',
  packageJson___description = 'packageJson.description',
  packageJson___version = 'packageJson.version',
  packageJson___main = 'packageJson.main',
  packageJson___license = 'packageJson.license',
  packageJson___dependencies = 'packageJson.dependencies',
  packageJson___dependencies___name = 'packageJson.dependencies.name',
  packageJson___dependencies___version = 'packageJson.dependencies.version',
  packageJson___devDependencies = 'packageJson.devDependencies',
  packageJson___devDependencies___name = 'packageJson.devDependencies.name',
  packageJson___devDependencies___version = 'packageJson.devDependencies.version',
  packageJson___peerDependencies = 'packageJson.peerDependencies',
  packageJson___peerDependencies___name = 'packageJson.peerDependencies.name',
  packageJson___peerDependencies___version = 'packageJson.peerDependencies.version',
  packageJson___keywords = 'packageJson.keywords'
}

type SitePluginFilterInput = {
  readonly id: Maybe<StringQueryOperatorInput>;
  readonly parent: Maybe<NodeFilterInput>;
  readonly children: Maybe<NodeFilterListInput>;
  readonly internal: Maybe<InternalFilterInput>;
  readonly resolve: Maybe<StringQueryOperatorInput>;
  readonly name: Maybe<StringQueryOperatorInput>;
  readonly version: Maybe<StringQueryOperatorInput>;
  readonly pluginOptions: Maybe<SitePluginPluginOptionsFilterInput>;
  readonly nodeAPIs: Maybe<StringQueryOperatorInput>;
  readonly browserAPIs: Maybe<StringQueryOperatorInput>;
  readonly ssrAPIs: Maybe<StringQueryOperatorInput>;
  readonly pluginFilepath: Maybe<StringQueryOperatorInput>;
  readonly packageJson: Maybe<SitePluginPackageJsonFilterInput>;
};

type SitePluginGroupConnection = {
  readonly totalCount: Scalars['Int'];
  readonly edges: ReadonlyArray<SitePluginEdge>;
  readonly nodes: ReadonlyArray<SitePlugin>;
  readonly pageInfo: PageInfo;
  readonly field: Scalars['String'];
  readonly fieldValue: Maybe<Scalars['String']>;
};

type SitePluginPackageJson = {
  readonly name: Maybe<Scalars['String']>;
  readonly description: Maybe<Scalars['String']>;
  readonly version: Maybe<Scalars['String']>;
  readonly main: Maybe<Scalars['String']>;
  readonly license: Maybe<Scalars['String']>;
  readonly dependencies: Maybe<ReadonlyArray<Maybe<SitePluginPackageJsonDependencies>>>;
  readonly devDependencies: Maybe<ReadonlyArray<Maybe<SitePluginPackageJsonDevDependencies>>>;
  readonly peerDependencies: Maybe<ReadonlyArray<Maybe<SitePluginPackageJsonPeerDependencies>>>;
  readonly keywords: Maybe<ReadonlyArray<Maybe<Scalars['String']>>>;
};

type SitePluginPackageJsonDependencies = {
  readonly name: Maybe<Scalars['String']>;
  readonly version: Maybe<Scalars['String']>;
};

type SitePluginPackageJsonDependenciesFilterInput = {
  readonly name: Maybe<StringQueryOperatorInput>;
  readonly version: Maybe<StringQueryOperatorInput>;
};

type SitePluginPackageJsonDependenciesFilterListInput = {
  readonly elemMatch: Maybe<SitePluginPackageJsonDependenciesFilterInput>;
};

type SitePluginPackageJsonDevDependencies = {
  readonly name: Maybe<Scalars['String']>;
  readonly version: Maybe<Scalars['String']>;
};

type SitePluginPackageJsonDevDependenciesFilterInput = {
  readonly name: Maybe<StringQueryOperatorInput>;
  readonly version: Maybe<StringQueryOperatorInput>;
};

type SitePluginPackageJsonDevDependenciesFilterListInput = {
  readonly elemMatch: Maybe<SitePluginPackageJsonDevDependenciesFilterInput>;
};

type SitePluginPackageJsonFilterInput = {
  readonly name: Maybe<StringQueryOperatorInput>;
  readonly description: Maybe<StringQueryOperatorInput>;
  readonly version: Maybe<StringQueryOperatorInput>;
  readonly main: Maybe<StringQueryOperatorInput>;
  readonly license: Maybe<StringQueryOperatorInput>;
  readonly dependencies: Maybe<SitePluginPackageJsonDependenciesFilterListInput>;
  readonly devDependencies: Maybe<SitePluginPackageJsonDevDependenciesFilterListInput>;
  readonly peerDependencies: Maybe<SitePluginPackageJsonPeerDependenciesFilterListInput>;
  readonly keywords: Maybe<StringQueryOperatorInput>;
};

type SitePluginPackageJsonPeerDependencies = {
  readonly name: Maybe<Scalars['String']>;
  readonly version: Maybe<Scalars['String']>;
};

type SitePluginPackageJsonPeerDependenciesFilterInput = {
  readonly name: Maybe<StringQueryOperatorInput>;
  readonly version: Maybe<StringQueryOperatorInput>;
};

type SitePluginPackageJsonPeerDependenciesFilterListInput = {
  readonly elemMatch: Maybe<SitePluginPackageJsonPeerDependenciesFilterInput>;
};

type SitePluginPluginOptions = {
  readonly extensions: Maybe<ReadonlyArray<Maybe<Scalars['String']>>>;
  readonly name: Maybe<Scalars['String']>;
  readonly path: Maybe<Scalars['String']>;
  readonly remote: Maybe<Scalars['String']>;
  readonly branch: Maybe<Scalars['String']>;
  readonly patterns: Maybe<ReadonlyArray<Maybe<Scalars['String']>>>;
  readonly stylesProvider: Maybe<SitePluginPluginOptionsStylesProvider>;
  readonly short_name: Maybe<Scalars['String']>;
  readonly start_url: Maybe<Scalars['String']>;
  readonly background_color: Maybe<Scalars['String']>;
  readonly theme_color: Maybe<Scalars['String']>;
  readonly display: Maybe<Scalars['String']>;
  readonly icon: Maybe<Scalars['String']>;
  readonly cache_busting_mode: Maybe<Scalars['String']>;
  readonly include_favicon: Maybe<Scalars['Boolean']>;
  readonly legacy: Maybe<Scalars['Boolean']>;
  readonly theme_color_in_head: Maybe<Scalars['Boolean']>;
  readonly cacheDigest: Maybe<Scalars['String']>;
  readonly gitSources: Maybe<ReadonlyArray<Maybe<SitePluginPluginOptionsGitSources>>>;
  readonly pathCheck: Maybe<Scalars['Boolean']>;
};

type SitePluginPluginOptionsFilterInput = {
  readonly extensions: Maybe<StringQueryOperatorInput>;
  readonly name: Maybe<StringQueryOperatorInput>;
  readonly path: Maybe<StringQueryOperatorInput>;
  readonly remote: Maybe<StringQueryOperatorInput>;
  readonly branch: Maybe<StringQueryOperatorInput>;
  readonly patterns: Maybe<StringQueryOperatorInput>;
  readonly stylesProvider: Maybe<SitePluginPluginOptionsStylesProviderFilterInput>;
  readonly short_name: Maybe<StringQueryOperatorInput>;
  readonly start_url: Maybe<StringQueryOperatorInput>;
  readonly background_color: Maybe<StringQueryOperatorInput>;
  readonly theme_color: Maybe<StringQueryOperatorInput>;
  readonly display: Maybe<StringQueryOperatorInput>;
  readonly icon: Maybe<StringQueryOperatorInput>;
  readonly cache_busting_mode: Maybe<StringQueryOperatorInput>;
  readonly include_favicon: Maybe<BooleanQueryOperatorInput>;
  readonly legacy: Maybe<BooleanQueryOperatorInput>;
  readonly theme_color_in_head: Maybe<BooleanQueryOperatorInput>;
  readonly cacheDigest: Maybe<StringQueryOperatorInput>;
  readonly gitSources: Maybe<SitePluginPluginOptionsGitSourcesFilterListInput>;
  readonly pathCheck: Maybe<BooleanQueryOperatorInput>;
};

type SitePluginPluginOptionsGitSources = {
  readonly remote: Maybe<Scalars['String']>;
  readonly branch: Maybe<Scalars['String']>;
  readonly patterns: Maybe<ReadonlyArray<Maybe<Scalars['String']>>>;
};

type SitePluginPluginOptionsGitSourcesFilterInput = {
  readonly remote: Maybe<StringQueryOperatorInput>;
  readonly branch: Maybe<StringQueryOperatorInput>;
  readonly patterns: Maybe<StringQueryOperatorInput>;
};

type SitePluginPluginOptionsGitSourcesFilterListInput = {
  readonly elemMatch: Maybe<SitePluginPluginOptionsGitSourcesFilterInput>;
};

type SitePluginPluginOptionsStylesProvider = {
  readonly injectFirst: Maybe<Scalars['Boolean']>;
};

type SitePluginPluginOptionsStylesProviderFilterInput = {
  readonly injectFirst: Maybe<BooleanQueryOperatorInput>;
};

type SitePluginSortInput = {
  readonly fields: Maybe<ReadonlyArray<Maybe<SitePluginFieldsEnum>>>;
  readonly order: Maybe<ReadonlyArray<Maybe<SortOrderEnum>>>;
};

type SiteSiteMetadata = {
  readonly title: Maybe<Scalars['String']>;
  readonly description: Maybe<Scalars['String']>;
  readonly siteUrl: Maybe<Scalars['String']>;
  readonly githubUrl: Maybe<Scalars['String']>;
  readonly robots: Maybe<Scalars['Boolean']>;
};

type SiteSiteMetadataFilterInput = {
  readonly title: Maybe<StringQueryOperatorInput>;
  readonly description: Maybe<StringQueryOperatorInput>;
  readonly siteUrl: Maybe<StringQueryOperatorInput>;
  readonly githubUrl: Maybe<StringQueryOperatorInput>;
  readonly robots: Maybe<BooleanQueryOperatorInput>;
};

type SiteSortInput = {
  readonly fields: Maybe<ReadonlyArray<Maybe<SiteFieldsEnum>>>;
  readonly order: Maybe<ReadonlyArray<Maybe<SortOrderEnum>>>;
};

enum SortOrderEnum {
  ASC = 'ASC',
  DESC = 'DESC'
}

type StringQueryOperatorInput = {
  readonly eq: Maybe<Scalars['String']>;
  readonly ne: Maybe<Scalars['String']>;
  readonly in: Maybe<ReadonlyArray<Maybe<Scalars['String']>>>;
  readonly nin: Maybe<ReadonlyArray<Maybe<Scalars['String']>>>;
  readonly regex: Maybe<Scalars['String']>;
  readonly glob: Maybe<Scalars['String']>;
};

type PagesQueryQueryVariables = Exact<{ [key: string]: never; }>;


type PagesQueryQuery = { readonly allSitePage: { readonly nodes: ReadonlyArray<Pick<SitePage, 'path'>> } };

type GatsbyImageSharpFluidFragment = Pick<ImageSharpFluid, 'base64' | 'aspectRatio' | 'src' | 'srcSet' | 'sizes'>;

type PageTemplateQueryVariables = Exact<{
  slug: Scalars['String'];
}>;


type PageTemplateQuery = { readonly mdx: Maybe<(
    Pick<Mdx, 'body' | 'tableOfContents' | 'timeToRead'>
    & { readonly frontmatter: Maybe<(
      Pick<MdxFrontmatter, 'title'>
      & { readonly embeddedImages: Maybe<ReadonlyArray<Maybe<{ readonly childImageSharp: Maybe<{ readonly original: Maybe<Pick<ImageSharpOriginal, 'width' | 'height' | 'src'>>, readonly fluid: Maybe<GatsbyImageSharpFluidFragment> }> }>>> }
    )>, readonly fields: Maybe<Pick<MdxFields, 'slug'>> }
  )> };

type SiteTitleQueryVariables = Exact<{ [key: string]: never; }>;


type SiteTitleQuery = { readonly site: Maybe<{ readonly siteMetadata: Maybe<Pick<SiteSiteMetadata, 'title' | 'githubUrl'>> }> };

type SiteMetaDataQueryVariables = Exact<{ [key: string]: never; }>;


type SiteMetaDataQuery = { readonly site: Maybe<{ readonly siteMetadata: Maybe<Pick<SiteSiteMetadata, 'title' | 'robots' | 'description'>> }> };

type Unnamed_1_QueryVariables = Exact<{ [key: string]: never; }>;


type Unnamed_1_Query = { readonly allMdx: { readonly nodes: ReadonlyArray<{ readonly fields: Maybe<Pick<MdxFields, 'sourceInstanceName' | 'slug'>>, readonly frontmatter: Maybe<Pick<MdxFrontmatter, 'title' | 'subheader' | 'order'>> }> } };

type GatsbyImageSharpFixedFragment = Pick<ImageSharpFixed, 'base64' | 'width' | 'height' | 'src' | 'srcSet'>;

type GatsbyImageSharpFixed_tracedSVGFragment = Pick<ImageSharpFixed, 'tracedSVG' | 'width' | 'height' | 'src' | 'srcSet'>;

type GatsbyImageSharpFixed_withWebpFragment = Pick<ImageSharpFixed, 'base64' | 'width' | 'height' | 'src' | 'srcSet' | 'srcWebp' | 'srcSetWebp'>;

type GatsbyImageSharpFixed_withWebp_tracedSVGFragment = Pick<ImageSharpFixed, 'tracedSVG' | 'width' | 'height' | 'src' | 'srcSet' | 'srcWebp' | 'srcSetWebp'>;

type GatsbyImageSharpFixed_noBase64Fragment = Pick<ImageSharpFixed, 'width' | 'height' | 'src' | 'srcSet'>;

type GatsbyImageSharpFixed_withWebp_noBase64Fragment = Pick<ImageSharpFixed, 'width' | 'height' | 'src' | 'srcSet' | 'srcWebp' | 'srcSetWebp'>;

type GatsbyImageSharpFluidLimitPresentationSizeFragment = { maxHeight: ImageSharpFluid['presentationHeight'], maxWidth: ImageSharpFluid['presentationWidth'] };

type GatsbyImageSharpFluid_tracedSVGFragment = Pick<ImageSharpFluid, 'tracedSVG' | 'aspectRatio' | 'src' | 'srcSet' | 'sizes'>;

type GatsbyImageSharpFluid_withWebpFragment = Pick<ImageSharpFluid, 'base64' | 'aspectRatio' | 'src' | 'srcSet' | 'srcWebp' | 'srcSetWebp' | 'sizes'>;

type GatsbyImageSharpFluid_withWebp_tracedSVGFragment = Pick<ImageSharpFluid, 'tracedSVG' | 'aspectRatio' | 'src' | 'srcSet' | 'srcWebp' | 'srcSetWebp' | 'sizes'>;

type GatsbyImageSharpFluid_noBase64Fragment = Pick<ImageSharpFluid, 'aspectRatio' | 'src' | 'srcSet' | 'sizes'>;

type GatsbyImageSharpFluid_withWebp_noBase64Fragment = Pick<ImageSharpFluid, 'aspectRatio' | 'src' | 'srcSet' | 'srcWebp' | 'srcSetWebp' | 'sizes'>;

type GatsbyImageSharpResolutionsFragment = Pick<ImageSharpResolutions, 'base64' | 'width' | 'height' | 'src' | 'srcSet'>;

type GatsbyImageSharpResolutions_tracedSVGFragment = Pick<ImageSharpResolutions, 'tracedSVG' | 'width' | 'height' | 'src' | 'srcSet'>;

type GatsbyImageSharpResolutions_withWebpFragment = Pick<ImageSharpResolutions, 'base64' | 'width' | 'height' | 'src' | 'srcSet' | 'srcWebp' | 'srcSetWebp'>;

type GatsbyImageSharpResolutions_withWebp_tracedSVGFragment = Pick<ImageSharpResolutions, 'tracedSVG' | 'width' | 'height' | 'src' | 'srcSet' | 'srcWebp' | 'srcSetWebp'>;

type GatsbyImageSharpResolutions_noBase64Fragment = Pick<ImageSharpResolutions, 'width' | 'height' | 'src' | 'srcSet'>;

type GatsbyImageSharpResolutions_withWebp_noBase64Fragment = Pick<ImageSharpResolutions, 'width' | 'height' | 'src' | 'srcSet' | 'srcWebp' | 'srcSetWebp'>;

type GatsbyImageSharpSizesFragment = Pick<ImageSharpSizes, 'base64' | 'aspectRatio' | 'src' | 'srcSet' | 'sizes'>;

type GatsbyImageSharpSizes_tracedSVGFragment = Pick<ImageSharpSizes, 'tracedSVG' | 'aspectRatio' | 'src' | 'srcSet' | 'sizes'>;

type GatsbyImageSharpSizes_withWebpFragment = Pick<ImageSharpSizes, 'base64' | 'aspectRatio' | 'src' | 'srcSet' | 'srcWebp' | 'srcSetWebp' | 'sizes'>;

type GatsbyImageSharpSizes_withWebp_tracedSVGFragment = Pick<ImageSharpSizes, 'tracedSVG' | 'aspectRatio' | 'src' | 'srcSet' | 'srcWebp' | 'srcSetWebp' | 'sizes'>;

type GatsbyImageSharpSizes_noBase64Fragment = Pick<ImageSharpSizes, 'aspectRatio' | 'src' | 'srcSet' | 'sizes'>;

type GatsbyImageSharpSizes_withWebp_noBase64Fragment = Pick<ImageSharpSizes, 'aspectRatio' | 'src' | 'srcSet' | 'srcWebp' | 'srcSetWebp' | 'sizes'>;

}

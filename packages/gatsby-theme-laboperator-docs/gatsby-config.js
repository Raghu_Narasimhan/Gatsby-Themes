module.exports = ({ gitSources = [] }) => ({
  siteMetadata: {
    title: 'Laboperator Docs',
    siteUrl: 'https://www.laboperator.com',
    githubUrl: 'https://www.github.com/labforward',
    description: 'A documentation site for Laboperator.',
    robots: false,
  },
  plugins: [
    {
      resolve: 'gatsby-plugin-mdx',
      options: {
        extensions: ['.mdx', '.md'],
        gatsbyRemarkPlugins: [
          {
            resolve: 'gatsby-remark-autolink-headers',
            options: {
              offsetY: '100',
              elements: ['h2', 'h3'],
            },
          },
          'gatsby-remark-prismjs',
        ],
      },
    },
    {
      resolve: 'gatsby-source-filesystem',
      options: {
        name: 'pages',
        path: 'src/pages/',
      },
    },
    {
      resolve: 'gatsby-source-filesystem',
      options: {
        name: 'images',
        path: 'src/images/',
      },
    },
    ...gitSources.map((options) => ({
      resolve: 'gatsby-source-git',
      options: {
        name: options.remote,
        ...options,
      },
    })),
    {
      resolve: 'gatsby-plugin-material-ui',
      options: {
        stylesProvider: {
          injectFirst: true,
        },
      },
    },
    'gatsby-plugin-react-helmet',
    'gatsby-plugin-catch-links',
    'gatsby-plugin-styled-components',
    'gatsby-plugin-sharp',
    'gatsby-plugin-typescript',
    'gatsby-plugin-typegen',
    'gatsby-transformer-sharp',
    {
      resolve: 'gatsby-plugin-manifest',
      options: {
        name: 'Laboperator Docs',
        short_name: 'Laboperator',
        start_url: '/',
        background_color: '#fff',
        theme_color: '#ffad00',
        display: 'standalone',
        icon: 'src/images/laboperator_favicon_32x32.png',
      },
    },
  ],
});

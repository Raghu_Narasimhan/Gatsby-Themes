const { attachFields } = require('gatsby-plugin-node-fields');
const { createFilePath } = require('gatsby-source-filesystem');

function isMdx(node) {
  return node.internal.type === 'Mdx';
}

function isGitRemoteFile(node) {
  return node.internal.type === 'File' && node.gitRemote___NODE;
}

const descriptors = [
  {
    predicate: isMdx,
    fields: [
      {
        name: 'slug',
        getter: (node, _actions, _context, getNode) =>
          createFilePath({ node, getNode }),
      },
      {
        name: 'sourceInstanceName',
        getter: (node, _actions, _context, getNode) =>
          getNode(node.parent).sourceInstanceName,
      },
      {
        name: 'title',
        getter: (node) => node.frontmatter.title,
        defaultValue: '',
      },
    ],
  },
  {
    predicate: isGitRemoteFile,
    fields: [
      {
        setter: (value, node, _, actions) => {
          actions.createParentChildLink({ parent: value, child: node });

          // eslint-disable-next-line no-param-reassign
          node.parent = node.gitRemote___NODE;
        },
        getter: (node, _actions, _context, getNode) =>
          getNode(node.gitRemote___NODE),
      },
    ],
  },
];

module.exports = async (
  {
    node,
    // loadNodeContent,
    actions,
    // createNodeId,
    getNode,
    // getNodes,
    // reporter,
    // cache,
    // pathPrefix,
    // ..._helpers
  },
  _pluginOptions
) => {
  attachFields(node, actions, getNode, descriptors);
};

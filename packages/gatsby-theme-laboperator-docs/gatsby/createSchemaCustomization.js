module.exports = ({ actions, _schema }) => {
  const { createTypes } = actions;
  const typeDefs = [
    'type Mdx implements Node { frontmatter: MdxFrontmatter }',
    `type MdxFrontmatter {
      title: String
      subheader: String
      order: Int
    }`,
  ];

  createTypes(typeDefs);
};

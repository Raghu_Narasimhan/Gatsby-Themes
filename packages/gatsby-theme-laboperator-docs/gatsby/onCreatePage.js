const isInPages = /.*pages.*/;

module.exports = async ({ page, actions }, _pluginOptions) => {
  const { createPage, deletePage } = actions;

  if (!isInPages.test(page.componentPath)) return;

  const newPage = {
    ...page,
    component: require.resolve('../src/components/PageTemplate.tsx'),
    context: {
      ...page.context,
      slug: page.path,
    },
  };

  deletePage(page);
  createPage(newPage);
};
